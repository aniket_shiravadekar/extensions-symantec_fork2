//+---------------------------------------------------------------------------
//  File:        symantec.cxx
//
// Copyright 2017 Synchronoss , All Rights Reserved.
//
// The copyright to the computer software herein is the property of
// Synchronoss, Inc. The software may be used and/or copied only with
// The written permission of Synchronoss, Inc. or in accordance with the
// terms and conditions stipulated in the agreement/contract under which
// the software has been supplied.
//----------------------------------------------------------------------------

#include <stdio.h>
#include <iostream>
#include <extapi.hxx>

#include "syutils.hxx"
#include "symantechandler.hxx"

using namespace std;

// Re-declared APIUserMsg because APIUserMsgs.hxx lives in mercury/omu
// and we can't include omu header files here.
#define APIUserMsg      0x1e0001

// This object is used to initialize Symantec logging and tracing
SymantecLog symantecLog(MODULE_NAME);

SymantecApplication* symantecApp = NULL; 

extern SymantecCode ProcessMail(IMxMailDelivery*, SymantecHookType);

//+---------------------------------------------------------------------
// Initialize global application object
//+---------------------------------------------------------------------
SVCAPI void Initialize( void )
{
    symantecLog.initSymantecLogging();
    symantecLog.setMainLogEvent("APIUserMsg", APIUserMsg);
    LOG(Notification, "Initialize", "Enter");
 
    try {
        DIAGC("symantecscan", 1, "Entering Initialize");
        symantecApp = new SymantecApplication(EXTN_SERVICE);

        // Initialize symantec scan application
        symantecApp->Initialize();

    } catch(exception &ex) {

        if (symantecApp->isMsgcatLoggingEnabled()) {
            REPORT_SymnAVInitFailed(SLOGHANDLE, Fatal, 
                                "SymnAVExtn extension service initialization failed.");
        } else {
        
            LOG(Fatal, "AVSymInitFailed", 
                "SymnAVExtn extension service initialization failed.");
            LOG(Fatal, "Initialize exception", "%s", ex.what());
        }
        return;

    } catch(...) {
        if (symantecApp->isMsgcatLoggingEnabled()) {
            REPORT_SymnAVInitFailed(SLOGHANDLE, Fatal, "SymnAVExtn extension service initialization failed.");
        } else {
            LOG(Fatal, "AVSymInitFailed", 
                "SymnAVExtn extension service initialization failed.");
        }    
        return;	
    }

    // Log the version info and initialization
    if (symantecApp->isMsgcatLoggingEnabled()) {
        REPORT_SymnAVInit(SLOGHANDLE, Notification, "Successfully initialized SymnAVExtn extension service.");
    } else {
        LOG(Notification, "AVSymExtSvc", "Successfully initialized SymnAVExtn extensions service.");
    }
}

//+---------------------------------------------------------------------
// Clean up and shutdown the application.
//+---------------------------------------------------------------------
SVCAPI void Finalize( void )
{
    //Free Symantec Logging
    symantecLog.freeSymantecLogging();

    DIAGC("symantec", 1, "Calling Finalize");
    symantecApp->Finalize();
    delete symantecApp;	
}


SVCAPI const AdvertiseInfo* Advertise( void )
{
    static EventInfo SymantecLocalEvents[] =
    {
        { "SymnAVScanLocal", MTA_LOCAL_DELIVER },
        0
    };

    static EventInfo SymantecRemoteEvents[] =
    {
        { "SymnAVScanRemote", MTA_REMOTE_DELIVER },
        0
    };

    static EventInfo SymantecPostEvents[] =
    {
        { "SymnAVScanPostValidate", MTA_POST_VALIDATE },
        0
    };

    static EventInfo SymantecEvents[] =
    {
        { "SymnAVScanLocal", MTA_LOCAL_DELIVER },
        { "SymnAVScanRemote", MTA_REMOTE_DELIVER },
        { "SymnAVScanPostValidate", MTA_POST_VALIDATE },
        0
    };

    static ServiceInfo Service[] =
    {
        { "SymnAVScan", SymantecEvents },
        { "SymnAVScanLocal", SymantecLocalEvents },
        { "SymnAVScanRemote", SymantecRemoteEvents },
        { "SymnAVScanPostValidate", SymantecPostEvents },
        0
    };

    static AdvertiseInfo info =
    {
        AdvertiseVersion1,
        (const ServiceInfo*) Service
    };

    return &info;
}

extern "C" SVCAPI void SymnAVScanLocal (IMxMailDelivery* mailDelivery);
extern "C" SVCAPI void SymnAVScanRemote (IMxMailDelivery* mailDelivery);
extern "C" SVCAPI void SymnAVScanPostValidate (IMxMailDelivery* mailDelivery);

//+---------------------------------------------------------------------------
// Synopsis: Extension server call this function for every inbound mail. 
//			This function is executed in a separate thread.
//+---------------------------------------------------------------------------
void SymnAVScanLocal( IMxMailDelivery* mailDelivery )
{  
    DIAGC("symantec", 5, "Received on SymnAVScanLocal");
    ProcessMail(mailDelivery, SymantecInbound);	
    DIAGC("symantec", 5, "DONE processing SymnAVScanLocal mail");
} 

//+---------------------------------------------------------------------------
// Synopsis: Extension server call this function for every outbound mail.
//			This function is executed in a separate thread.
//+--------------------------------------------------------------------------- 
void SymnAVScanRemote ( IMxMailDelivery* mailDelivery )
{
    DIAGC("symantec", 5, "Received on SymnAVScanRemote");
    ProcessMail(mailDelivery, SymantecOutbound);	
    DIAGC("symantec", 5, "DONE processing SymantecOutbound mail");
}

//+---------------------------------------------------------------------------
// Synopsis:  Extension server calles this function for every mail
//            it received through PostValidate hook.
//            This function is executed in an seperate thread.
//----------------------------------------------------------------------------
void SymnAVScanPostValidate ( IMxMailDelivery* mailDelivery )
{
    DIAGC("symantec", 5, "Received on SymnAVScanPostValidate");
    ProcessMail(mailDelivery, SymantecPostValidate);
    DIAGC("symantec", 5, "DONE processing SymnAVScanPostValidate mail");
}

SymantecCode ProcessMail(IMxMailDelivery* mailDelivery, SymantecHookType hookType)
{
    SymantecCode retCode;

    // check if Symantec Anti-Virus scanner initialized
    if(!symantecApp->isAppInitialized()) {
        if (symantecApp->isMsgcatLoggingEnabled()){
            REPORT_SymnAVScanNotAvail(SLOGHANDLE, Urgent,
                "No Valid SymnAVExtn Scanner; ");
        }else{
            LOG(Urgent, "AVSymScanNotAvail", "SymnAVExtn AV App failed to initialized !");
        }
        return SymantecFailure;
    }

    // Read the data from extension server and scan mail for virus 
    // If mail suspect then handle it as per action specified
    Handler handler(mailDelivery);

    try {
        if(handler.Initialize(hookType) != SymantecSuccess) {
            if (symantecApp->isMsgcatLoggingEnabled()) {
                REPORT_SymnAVMsgDeferred(SLOGHANDLE, Error, "HandlerInitializationFailed",
                    handler.getMsgInfo()->getTotalTime(),
                    handler.getMsgInfo()->getMailContext());
            }else{
                LOG(Error, "AVExtServerDataReadFailed", "HandlerInitializationFailed; %s; %s",
                    handler.getMsgInfo()->getTotalTime(),
                    handler.getMsgInfo()->getMailContext());
            }
            return SymantecFailure;
        }

        retCode = handler.PreProcess();

        switch (retCode) {
            case SymantecNoAction:
                if (symantecApp->isMsgcatLoggingEnabled()) {
                    REPORT_SymnAVMsgNoAction(SLOGHANDLE, Notification, 
                                           handler.getMsgInfo()->getTotalTime(),
                                           handler.getMsgInfo()->getMessageID().c_str());
                } else {
                    LOG(Notification, "MsgTrace", "NoAction; %s; msgid=%s",
                        handler.getMsgInfo()->getTotalTime(),
                        handler.getMsgInfo()->getMessageID().c_str());
                }
                handler.commitXHeader();
                return retCode;
            case SymantecNotScanned:
                if (symantecApp->isMsgcatLoggingEnabled()) {
                    REPORT_SymnAVMsgNotScanned(SLOGHANDLE, Notification, 
                                             handler.getMsgInfo()->getTotalTime(),
                                             handler.getMsgInfo()->getMessageID().c_str());
                } else {
                    LOG(Notification, "MsgTrace", "NotScanned; %s; msgid=%s",
                        handler.getMsgInfo()->getTotalTime(),
                        handler.getMsgInfo()->getMessageID().c_str());
                }
                handler.commitNoChange();
                return retCode;
            case SymantecFailure:
                if (symantecApp->isMsgcatLoggingEnabled()) {
                    REPORT_SymnAVMsgDeferred(SLOGHANDLE, Error, "InvalidSymnAVAction",
                                           handler.getMsgInfo()->getTotalTime(),
                                           handler.getMsgInfo()->getMailContext());
                } else {
                    LOG(Error, "AVSymInvalidSymnAVAction",
                        "Deferred; %s; %s",
                        handler.getMsgInfo()->getTotalTime(),
                        handler.getMsgInfo()->getMailContext());
                }
                handler.deferMessage();
                return retCode;
            default:
                break;
        }
    
        retCode = handler.Process();
        switch (retCode) {
            case SymantecCleanMail:
                handler.commitNoChange();
                if (symantecApp->isMsgcatLoggingEnabled()) {
                    REPORT_SymnAVMsgNoVirus(SLOGHANDLE, Notification,
                                          handler.getMsgInfo()->getTotalTime(),
                                          handler.getMsgInfo()->getMessageID().c_str());
                } else {

                    LOG(Notification, "MsgTrace", "NoVirus; %s; msgid=%s",
                        handler.getMsgInfo()->getTotalTime(),
                        handler.getMsgInfo()->getMessageID().c_str());
                }
                
                return retCode;
            case SymantecTimeout:
                handler.commitTimeout();
                return retCode;
            case SymantecNotCleaned:
                handler.takeBadMsgAction();
                return retCode;
            case SymantecFailure:
                 
                if (symantecApp->isMsgcatLoggingEnabled()) {
                    REPORT_SymnAVMsgDeferred(SLOGHANDLE, Error, "ProcessFailed",
                                           handler.getMsgInfo()->getTotalTime(),
                                           handler.getMsgInfo()->getMailContext());  
                } else {
                    LOG(Error, "AVExtSvcProcessingFailed", "ProcessFailed Deferred; %s; %s",
                        handler.getMsgInfo()->getTotalTime(),
                        handler.getMsgInfo()->getMailContext());
                }   
                handler.deferMessage();
                return retCode;
            case SymantecScanFailed:

                // take action as per configuration key 
                handler.takeScanFailedAction();
                return retCode;  
            default:
                break;
        }

        if (handler.Commit() != SymantecSuccess) {
            if (symantecApp->isMsgcatLoggingEnabled()) {
                REPORT_SymnAVMsgDeferred(SLOGHANDLE, Error, "CommitFailed",
                                               handler.getMsgInfo()->getTotalTime(),
                                               handler.getMsgInfo()->getMailContext());  
            } else {
                LOG(Error, "AVExtSvcProcessingFailed", "CommitFailed Deferred; %s; %s",
                        handler.getMsgInfo()->getTotalTime(),
                        handler.getMsgInfo()->getMailContext());
            }   
            handler.deferMessage();
            return retCode;
        }
    }
    catch(exception& ex) {

        if (symantecApp->isMsgcatLoggingEnabled()) {
            REPORT_SymnAVExceptionThrown(SLOGHANDLE, Urgent, ex.what());
        } else {
            LOG(Urgent, "AVExtSvcProcessingFailed", "Exception: %s", ex.what());
        }
        handler.deferMessage();
        return SymantecFailure;
    }
    catch(...) {
        if (symantecApp->isMsgcatLoggingEnabled()) {
            REPORT_SymnAVExceptionThrown(SLOGHANDLE, Urgent, "Unknown error, mail will be deferred");
        } else {
            LOG(Urgent, "AVExtSvcProcessingFailed", "Exception thrown, mail will be deferred");
        }
        handler.deferMessage();
        return SymantecFailure;
    }

    return SymantecSuccess;
}


//+---------------------------------------------------------------------------
// Synopsis:  This function is defined for the purpose to use with
//            imgetversion tool to get the shared library version, similar to
//            Intermail servers.
//            Example:
//            mta -version
//            imgetversion symnavscan.so
//----------------------------------------------------------------------------
const char* getSharedLibraryVersion()
{
    SymantecApplication app(EXTN_SERVICE);
    app.Initialize();
    string str(app.getSymAVVersion());
    app.Finalize();
    return str.c_str();
}
