//----------------------------------------------------------------------------
//
// File Name:    syutils.hxx
//
// Synopsis:    This file contains some wrapper class for CAPI and
//              std::string
//----------------------------------------------------------------------------
// Copyright 2017 Synchronoss, Inc. All Rights Reserved.
//
// The copyright to the computer software herein is the property of
// Synchronoss, Inc. The software may be used and/or copied only with
// The written permission of Synchronoss, Inc. or in accordance with the
// terms and conditions stipulated in the agreement/contract under which
// the software has been supplied.
//----------------------------------------------------------------------------

#ifndef __SYMUTILS_HXX__
#define __SYMUTILS_HXX__

//system includes 
#include <iostream>
#include <string.h>
#include <stdarg.h>
#include <vector>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/time.h>
#include <string>
#include <algorithm>

typedef long long long64;
typedef unsigned long long ulong64;

typedef char MsgNumBuf[32];

//C-API includes
#include <im_config2.h>
#include <im_log2.h>

#include "symconstants.hxx"
#include <extapi.hxx>

using namespace std; 

namespace symantecscan {


//+---------------------------------------------------------------------------
// Synopsis: This class is used as a wrapper class for Log and Trace apis 
//+---------------------------------------------------------------------------

class SymantecLog
{
  public:
    // Constructor and Destructor
    SymantecLog(const char* name): _moduleName(name) {};
    ~SymantecLog() {};

    void initSymantecLogging();
    void freeSymantecLogging();
    void setMainLogEvent(const char* logEvent, IM_MsgId msgId);

    // This function add log entry in the extension server log file
    static inline int writeLog(IM_LogSeverity severity, const char* logEvent,
                               const char* logText="", ...)
    {
        if (symantecLogHandle == NULL)
       	    return -1;

        va_list       ap;
        va_start (ap, logText);
        char          message[4096];
        vsnprintf (message, sizeof(message), logText, ap);
        return IM_WriteLogArgs (symantecLogHandle, NULL, severity, logEvent, 1, message);
    }

    // This function add trace data in a log file if trace enabled
    static inline int writeTrace(const char* tag, short level, const char* format...)
    {
        if (!IM_IsTraceEnabled())
        return 0;	//return when trace not enabled

        va_list ap;
        va_start(ap, format);
        // 1024 is the maximum chars allowed for trace message
        char message[1024];
        vsnprintf(message, sizeof(message), format, ap);
        return IM_WriteTrace(symantecTraceHandle, NULL, tag, level, message);	 
    }

  public:
    // log and trace handle
    static IM_FileInfo* symantecLogHandle;
    static IM_FileInfo* symantecTraceHandle;
	
  private:
    // module name of the extension service
    string _moduleName;
};


class MxSConfig;

typedef struct tagCfgContext
{
    int         min;
    int         max;
    IM_IMPACT   impact;
    SConfigType type;
    MxSConfig   *parent;
} CfgContext;

// When error occured write the error message and error code to
// imextserv.trace.
bool HandleError(IM_Error& imError, const char* name="");

ImpactInfo* ConfigCallback(const char *host, const char* module, const char* key, IM_ACTION action,
                           const char* value, void* callbackContext);

#ifdef MAX_PATH_NAME
#undef MAX_PATH_NAME
#endif

#define MAX_PATH_NAME 4096

class MxSConfig
{
  public:
    // constructors
    MxSConfig(){
        _registration = NULL;
    }
   
    MxSConfig(const char* name, const char* defValue, 
             const char* host, const char* prog, SConfigType val, IM_IMPACT impact);

    //value_type=boolean
    MxSConfig(const char* name, bool defValue,
             const char* host, const char* prog, SConfigType val, IM_IMPACT impact);

    //value_type=long
    MxSConfig(const char* name, long defValue,
             const char* host, const char* prog, SConfigType val, IM_IMPACT impact);
    
    // destructor
    ~MxSConfig(){
        if (_impactInfo != NULL) {
            delete _impactInfo;
            _impactInfo = NULL;
        }
    }

    inline const char*  getString() {return getString(key.c_str(), defaultval.c_str()); }
    const char*         getString(const char* key, const char* defVal);

    long  getLong(const char* key, long defVal);
    bool  getBool(const char* key, bool defVal);
    bool  getBool() {return getBool(key.c_str(), defaultval == "false" ? false : true); }
    int   getInt() {return atoi(getString()); }
    void  addConfigKey(const char* program, const char* keyName, const char* value, 
                       SConfigType type, IM_IMPACT impact, int min=0, int max=0);
    inline ImpactInfo* getImpactInfo() { return _impactInfo; }
    char* getPath(char * buf, int bufsize);

  private:
    void  _setCfgSymantecInboundAction();

  private:
    void*        _registration;
    CfgContext   _callbackContext;	
    ImpactInfo*  _impactInfo;

    string  _name;
    string  key;
    string  defaultval;
};


typedef vector<std::string> SymStringArray;

class SymString : public std::string
{
  private:
    SymStringArray  _split(const SymString& delimiter);

  public:
    SymString(){};
    SymString(const char* str) : std::string(str) {};
    SymString(const char* str, long len) : std::string(str, len) {};
    SymString(const std::string& str) : std::string(str) {};
    SymString(char ch, int repeat);

    ~SymString(){};

    SymString& operator= (const std::string& str);
    SymString& operator= (const SymString& str);
    SymString& operator= (const char* str);
    SymString& operator= (const char& c);

    SymString& operator+= (const std::string& str);
    SymString& operator+= (const SymString& str);
    SymString& operator+= (const char* str);
    SymString& operator+= (const char& c); 

    bool operator== (const SymString& str);
    bool operator== (const std::string& str);
    bool operator== (const char* str);

    bool operator!= (const SymString& str);
    bool operator!= (const std::string& str);
    bool operator!= (const char* str);
    char operator[] (int index);

    SymString&   Trim();
    SymString    Clone(void) const;
    inline void  Clear(void) { erase(); };
    int          CompareNoCase(const char* str);
    int          Index(const char* str, int start=0) const;
    int          RIndex(const char* str, int start=0) const;
    bool         HasPrefix(const char* str) const;
    SymString&   UpperCase();
    SymString    Segment(int start, int length) const;
    SymStringArray  Split(const char* delimiter);
    SymString     S(const char* macro, const SymString& replace, const char* opts = "");
    SymString     S(const char* macro, const char* replace, const char* opts = "");
    char*        GetBuf() const;  // return the copy of the string buffer

    inline const char* AsPtr() const { return c_str(); }
    inline operator const char * () const { return (c_str() ? c_str() : ""); }

    inline friend int Len ( const SymString&);
};

int Len(const SymString& str) {
    return str.size();
}

#ifdef _BIG_ENDIAN
static int const msb_index = 0;
static int const lsb_index = 1;
#else // _LITTLE_ENDIAN
static int const msb_index = 1;
static int const lsb_index = 0;
#endif


inline int int_cast(long n) { 
    return n;
}


// Mime Header Parse tokens
const int k_MHT_EOF    = -1;
const int k_MHT_String = 1;
const int k_MHT_Error  = 2;

class MxMimeHeaderParser
{
    // input line
    SymString _str_Input;
    // position in input
    unsigned long _n_InputPos;
    // text of current token
    SymString _str_TokenText;
    // type of current token
    int _n_Token;
    // true if we're parsing RFC2045 parameters
    boolean _f_ParsingParameters;
    // true if we had a scanner error
    boolean _f_Error;
    // get a token
    int GetToken(SymString &s);

public:
    // parse string x; f_Param is true if we should use RFC2045
    inline MxMimeHeaderParser(const SymString &s, boolean f_Param) {
        _str_Input = s;
        _n_InputPos = 0;
        _f_ParsingParameters = f_Param;
        _n_Token = GetToken(_str_TokenText);
        _f_Error = false;
    }
    inline boolean GetErrorFlag() { return _f_Error; }
    // skip a token if it's "n", otherwise do nothing
    inline boolean SkipToken(int n) {
        if (_n_Token != n)
            return false;
        _n_Token = GetToken(_str_TokenText);
        return true;
    }
    // get a string token
    SymString GetString();
    // skip over a token, and flag an error if we didn't find it
    inline void Expect(int n) {
        if (!SkipToken(n))
            _f_Error = true;
    }
    // true if we're at the end of the string
    inline boolean AtEOF() { return _f_Error || _n_Token == k_MHT_EOF; }
    // make sure we're at the end of the string, and flag an
    // error if there's still stuff to parse
    inline void Finished() { Expect(k_MHT_EOF); }
};

SymString MxDecodeBase64 (const SymString & str_EncodedString);
SymString MxEncodeBase64 (const SymString & str_UnencodedString, bool oneline=false);

typedef long long RealTimeClockSample_t;
//----------------------------------------------------------------------------
//
// Class Name       MethodTimingSnapShot
//
// Synopsis:        An instance of this class can be used to record and
//                  report the duration of execution of a method or block of
//                  code. It is implemented as a Guard class, such that
//                  creation of the instance records the start of the timing
//                  window, and invocation of the destructor completes the
//                  timing snapshot and logs the methods duration.  An
//                  instance may be placed just after a code blocks opening
//                  '{'. Logging to the .trace file can be disarmed by
//                  calling the disarm() method, and the text message
//                  my be retrieved and reported else in the application.
//----------------------------------------------------------------------------
class MethodTimingSnapShot
{
protected:
   bool                     enabled;
   std::string*             methodName;
   RealTimeClockSample_t    startingTime;
   char                     textBuffer[256];
   float                    duration;

   MethodTimingSnapShot();
   MethodTimingSnapShot( const MethodTimingSnapShot&);
   const MethodTimingSnapShot& operator=( const MethodTimingSnapShot&);

   // Allocates the std::string
   void init(char const * const name);

public:
   MethodTimingSnapShot( char const * const argMethodName);
  ~MethodTimingSnapShot();

  void recordSnapShot();

  inline void disable() { enabled = false;} // Disables logging.

  inline char const * const getTextBuffer() {return textBuffer;}
  // return the time snap shot in milliseconds.
  inline float getDuration() { return duration; }

  // Samples the system's real time clock if available.
  // Declared static to allow other modules to sample
  // the clock without creating an instance of this class.
  static RealTimeClockSample_t getRealTimeClock();

};
} 
#endif
