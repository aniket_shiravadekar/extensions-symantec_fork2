//----------------------------------------------------------------------------
//
// File Name:    msginfo.hxx
//
// Synopsis:     MsgInfo and Recipient class declaration
//----------------------------------------------------------------------------
// Copyright  2017 Synchronoss, Inc. All Rights Reserved.
//
// The copyright to the computer software herein is the property of
// Synchronoss, Inc. The software may be used and/or copied only with
// The written permission of Synchronoss, Inc. or in accordance with the
// terms and conditions stipulated in the agreement/contract under which
// the software has been supplied.
//----------------------------------------------------------------------------

#ifndef __MSGINFO_H__
#define __MSGINFO_H__

#include <sys/time.h>
#include <time.h>

#include <iostream>
#include <string>
#include <sys/types.h>

#include "syutils.hxx"
#include "symconstants.hxx"
#include "mmapfile.hxx"

#include <extapi.hxx>

using std::string;

#define MAX_PATH_NAME 4096

namespace symantecscan {

class MailUser
{
  public:
    MailUser(IMxMailgram* Mailgram);
    virtual ~MailUser();	
	
    const StringInfo*         getAddress();
    const StringInfo*         getAddrStrInfo();
    virtual SymantecAction    getSymantecAction() = 0;
    inline SymantecActionBase getSymantecActionBase() { return _actionType; }
    SymString getCosAttr(const char* cosAttrs,
                         const char* cosName, bool isString = false);
 
    inline void setSymantecActionBase(SymantecActionBase symAction) { 
        _actionType = symAction; 
    }
    void setSymantecAction(SymantecAction symAction);	

  protected:
    IMxMailgram*       _mailgram;
    IMxEnvelope*       _envelope;
    int	               _result;
    SymantecActionBase _actionType;
    SymantecAction     _action;
    string             _noticeStr;
    SymString          _actionStr;
};


class Recipient : public MailUser
{
  public:
    Recipient(IMxMailgram* Mailgram, int id);
    ~Recipient();

    const StringInfo* getAddress();
    const StringInfo* getAddrStrInfo();
    SymantecAction    getSymantecAction();

    const char* getRcptToDisp();
    const char* getRcptToAttr();

    int  getDisposition(StringInfo** infoRcptToDisp);
    bool isRemoteAddress();
    void setRemoteAddress(bool bRemote) { _bRemote = bRemote; }

  private:
    int  _index;
    bool _bAddress;
    bool _bRemote;
};


class Sender : public MailUser
{
  public:
    Sender(IMxMailgram* Mailgram);
    ~Sender();

    const StringInfo* getAddress();
    const StringInfo* getAddrStrInfo();
    SymantecAction    getSymantecAction();

    char const* getMailFromAttr();
};

typedef long long hrtime_t;

class MsgInfo
{
  public:
    MsgInfo(IMxMailDelivery* MailDelivery);
    ~MsgInfo(); 

    // getters and setters
    const StringInfo* getBody();
    const StringInfo* getHeader();
    const StringInfo* getMailFrom();

    string getSubject();
    string getMessageID();
    const char* getTotalTime(); 
    RealTimeClockSample_t getInitTime(){ return _initTime; }
    
    inline const StringInfo* getNewBody() {
        return (_newBody != NULL ? _newBody : getBody()); 
    }
    inline const StringInfo* getNewHeader() {
        return (_newHeader != NULL ? _newHeader : getHeader()); 
    }
    inline const char* getPeerIP() {
        return  _peerIP.AsPtr();
    }

    inline void setNewBody(StringInfo* newBody) {
        _newBody = newBody;
    }
    inline void setNewHeader(StringInfo* newHeader) {
        _newHeader = newHeader;
    }
    inline void setFileName(const char* name) { 
        if (_fileName.size()) _fileName.append("; ");
        _fileName.append(name); 
    }
    void clearSuspectMsgPartFlag();
    inline const char* getFileName() { 
        return _fileName.c_str(); 
    }
    inline void clearFileName() {
        _fileName.clear();
    }
    inline void clearVirusName() {
        _virusname.clear();
    } 
    inline void clearProblemDetails() {
        _problemDetails.clear();    
    }

    void setMessageID();	
    inline int getRecipCount() { return _rcptToCount; }
	
    SymString readFromHeader(const char*, const char*, int len=0);
    SymString readFromHeader(const char*, bool decode=true, bool perl=false);
    bool isMsgWithAttachment(const StringInfo* header, const StringInfo* body);
    bool findInfo(Entity* childEntity, int& type);

    inline bool  isRepaired() { return _bRepaired; }
    inline void  setRepaired(bool flag) { _bRepaired = flag; }
    inline void  setRepairFailed(bool flag) { _repairFailed = flag; }
    inline bool  isRepairFailed() {return _repairFailed; }
    inline StringInfo* getRepairedBody() { return _pRepairedBody; }
    
    inline void  setRepairedBody(StringInfo* pMsgBody) { 
       _pRepairedBody = pMsgBody; 
    }
    inline void  setRepairedHeader(StringInfo* pMsgHeader) {
       _pRepairedHeader = pMsgHeader; 
    }
    inline StringInfo* getRepairedHeader() { return _pRepairedHeader; }

    // Get mail attribute
    inline bool         isOutboundMail() { return _outboundMail; }
    inline const char*  getVirusName() { return _virusname.c_str(); }

    inline const char*  getMailContext()   { return _mailContext.AsPtr(); }
    SymString           readFromBody(const char*, const char*, int);   
    void                deleteSymantecExtSvcXHeader(bool);

    float getTimeConsumed() { 
       getTotalTime();
       return _fTimeConsume; 
    } 
    inline bool isMsgPartSuspect() { return _bMsgPartSuspect;}


    void   setVirusName(string& name);
    void   setSymantecActionType(SymantecActionBase symActionType);
    void   setSymantecVerdict(SymantecVerdict verdict, bool scanSet=false);
    void   setProblemDetails(string virusName, string fileName);
    void   setCurrentVirusDetails(string virusName, string fileName) {
        _currVirusName.assign(virusName.c_str());
        _currInfcFileName.assign(fileName.c_str());
    }
    

    inline SymantecVerdict    getSymantecVerdict() { return _virusVerdict; }
    inline const char*        getDateSubmitted() { return _dateSubmitted.c_str(); }
    inline SymantecActionBase getSymantecActionType() { return _symantecActionType; }
    inline const char*        getProblemDetails() { return _problemDetails.c_str(); }
    inline const char*        getCurrentVirusName() { return _currVirusName.c_str(); }
    inline const char*        getCurrentInfecFileName() { return _currInfcFileName.c_str(); }
	
    // Get Sender and Recipients
    Recipient*  getRecipient(int index);
    const char* getRecipientList();
    Sender*     getSender() { return _MailSender; }

    SMappedFileWrite* getSMappedHeader();
    SMappedFileWrite* getSMappedBody();

    SymantecCode _createSMappedFileWrite(SMappedFileWrite*& sfw,
                                         const char* fileExtn,
                                         const StringInfo* strInfo);
    bool isHeaderRewritten() {return _headerRewritten;}
    void headerRewritten() {_headerRewritten = true;}
    void addSymantecExtSvcXHeader(const char* value = "Processed");

  private:	
    SymantecCode _storeSender(void);
    SymantecCode _storeRecipients(void);
    void         _cleanRecipArray(void);
    SymString    _searchSString(const SymString&, const char*, const char*);
    void         _buildMailContext();

  public:
    typedef vector<Recipient *> RecipArray;
  
  private:
    StringInfo*	 _newHeader;
    StringInfo*  _newBody;
    Sender*      _MailSender;
    StringInfo*  _pRepairedBody;
    StringInfo*  _pRepairedHeader;

    IMxMailDelivery* _mailDelivery;
    IMxMailgram*     _mailgram;
    IMxEnvelope*     _envelope;
    IMxMailMessage*  _mailMessage;

    SMappedFileWrite* _sfwHeader;
    SMappedFileWrite* _sfwBody;

    SMappedFileWrite* cleanHeader;
    SMappedFileWrite* cleanBody;

    float        _fTimeConsume;
    hrtime_t     _initTime;
    int          _result;
    unsigned int _rcptToCount;
    RecipArray   _recipArray;

    char  _TimeBuffer[20];
    bool  _bRepaired;
    bool  _repairFailed; 
    bool  _headerRewritten;
    bool  _bSymantecXHeaderAdded;
    bool  _outboundMail;
    bool  _bMsgPartSuspect;

    SymantecActionBase _symantecActionType;
    SymantecAction     _symantecAction;
    SymantecVerdict    _virusVerdict;
    SymantecVerdict    _scanVirusVerdict;

    string  _msgID;
    string  _subject;
    string  _virusname;
    string  _fileName;
    string  _dateSubmitted;
    string  _problemDetails;
    string  _currVirusName;
    string  _currInfcFileName;

    SymString  _recipientList;
    SymString  _peerIP;
    SymString  _mailContext;
};

SymString escape(const SymString& line, bool perl = true);
char* getSymantecTmpDir(char * buf, int bufsize);
char* mkTempName(const char *ext, char *pathBuffer, bool useRunDir);

}
#endif

