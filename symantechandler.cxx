//----------------------------------------------------------------------------
//
// File Name:    symantechandler.cxx
//
// Synopsis:     Handler class implementation
//----------------------------------------------------------------------------
// Copyright 2017 Synchronoss, Inc. All Rights Reserved.
//
// The copyright to the computer software herein is the property of
// Synchronoss, Inc. The software may be used and/or copied only with
// The written permission of Synchronoss, Inc. or in accordance with the
// terms and conditions stipulated in the agreement/contract under which
// the software has been supplied.
//----------------------------------------------------------------------------

#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <stdio.h>

#include "symantechandler.hxx"

using namespace std;

//+---------------------------------------------------------------------------
//
// Method:     Handler::Handler
//
// Synopsis:   Constructor of class Handler, initialize  mail info and action 
//             array.
//
//----------------------------------------------------------------------------
Handler::Handler(IMxMailDelivery* MailDelivery) 
		: _recipCount(0),
                  _virusNoticeCount(0),
                  _msgInfo(NULL),
                  _mailDelivery(MailDelivery),
                  _wHeader(NULL),
                  _wBody(NULL),
                  _rHeader(NULL),
                  _rBody(NULL),
                  _cleanMail(false),
                  _RepairMsg(false),
                  _CleanVirus(false),
                  _ScanVirus(false),
                  _strVNoticeRecipients()
{
    DIAGC("symantecHandler", 5, "Handler Constructor");

    _mailActionArray.clear();
    _virusNoticeRecipArray.clear();
    _cleanMainInfoArray();
}

//+---------------------------------------------------------------------------
//
// Method:     Handler::~Handler
//
// Synopsis:   Destructor of class Handler, clean mail info and action array.
//
//----------------------------------------------------------------------------
Handler::~Handler()
{
    if (_msgInfo)
        delete _msgInfo;

    if (_wHeader) {
        if (!_wHeader->IsCached()) {
            _wHeader->Close();
            unlink(_wHeader->getFilename());
        }
        delete _wHeader;
    }

    if (_rHeader) delete _rHeader;

    if (_wBody) {
        if (!_wBody->IsCached()) {
            _wBody->Close();
            unlink(_wBody->getFilename());
        }
        delete _wBody;
    }

    if (_rBody) delete _rBody;

    _mailActionArray.clear();
    _cleanMainInfoArray();
}

//+---------------------------------------------------------------------------
//
// Method:	Handler::_cleanMainInfoArray
//
// Synopsis:	Loop through each element in MailInfoArray and
//		delete the objects.
//
//----------------------------------------------------------------------------
void Handler::_cleanMainInfoArray()
{
    for (MailInfoArray::iterator pAction = _mailActionArray.begin();
		pAction != _mailActionArray.end(); pAction++) {
        RecipInfoArray pInfo = pAction->second.recipInfoArray;
        for (int index = 0; index < int_cast(pInfo.size()); index++) {
            RecipInfo* recipInfo = pInfo[index];
            if (recipInfo == NULL)
                continue;
			
            delete recipInfo;
            recipInfo = NULL;
        }
    }
}

//+---------------------------------------------------------------------------
//
// Method: Handler::Initialize
// 
// Synopsis: Create MsgInfo object, read and store all the mail information
//			 from extension server.
//
//----------------------------------------------------------------------------
SymantecCode Handler::Initialize(SymantecHookType hookType)
{
    DIAGC("syamntecHandler", 5, "Entering Handler::Initialize");

    _hookType = hookType;
    try {
        _msgInfo = new MsgInfo(_mailDelivery);
        _recipCount = _msgInfo->getRecipCount();

    } catch(...) {
        if (symantecApp->isMsgcatLoggingEnabled())
            REPORT_SymnAVDataReadFailed(SLOGHANDLE, Error, "Creating MsgInfo object");
        else
            LOG(Error, "AVExtServerDataReadFailed", "Creating MsgInfo object");
        throw;
    }

    if (symantecApp->isMsgcatLoggingEnabled()) {
        REPORT_SymnAVMsgReceived(SLOGHANDLE, Notification, "Handler initialized", 
                                   _msgInfo->getMessageID().c_str());
    }else {
        LOG(Notification, "MsgTrace", "MsgReceived; msgid=%s", 
            _msgInfo->getMessageID().c_str());
    }

    return SymantecSuccess;
}

//+---------------------------------------------------------------------------
//
// Method:	Handler::PreProcess
//
// Synopsis:	Determine mail action as per hook type and CoS autorative value 
//
//----------------------------------------------------------------------------
SymantecCode Handler::PreProcess()
{
    DIAGC("symantecHandler", 5, "Entering PreProcess");
    //+---------------------------------------------------------------------------
    // When SymantecAction is "Reject" (disposition=bounce) "X-Opwv-SymantecExtSvc"
    // XHeader will be added to header and message will be sent back to MTA with
    // "bounce" disposition. MTA creates a new message. It generates a new
    // header and prepends old header to old body and creates new body.
    // So when we recieve a message, its body is scanned for
    // "X-Opwv-SymantecExtSvc" to determine if the message was processed once.
    // If X-Opwv-SymantecExtSvc Header tag found remove it from body and send
    // back the message without processing
    //+---------------------------------------------------------------------------

    //+---------------------------------------------------------------------------
    // If it is the bounce message, the X-Opwv-SymantecExtsvc header should be
    // searched within the length defined below. So we don't spend extra time
    // searching a big message body.
    //+---------------------------------------------------------------------------
    int iSearchLen = _msgInfo->getBody()->len;
    if (Len(_msgInfo->readFromBody(symantecApp->getXOpwvHeader(), "\r",  iSearchLen)) > 0)
    {
        _msgInfo->deleteSymantecExtSvcXHeader(true);
        DIAGC("symantecHandler", 5, "Exit PreProcess, deleteExtSvcXHeader");
        return SymantecNoAction;
    }

    // determine input message valid for anti-virus scaning
    if (strcasecmp("AllMsg", symantecApp->getSymAVScanByMsgType())==0) {
        // All type of messages allowed to av scan
        
    } else {
        // check if message has attachment
        if (_msgInfo->isMsgWithAttachment(_msgInfo->getHeader(), _msgInfo->getBody()));
        else {
            // message not allowed for scanning, will deliver as NO action
            LOG(Notification, 
            "A message without an attachment ! Please check Config key symnAVScanByMsgType.");

            return SymantecNotScanned;
        }
    }
 
    // determine symantec action base from CoS and hook type 
    SymantecActionBase symantecActionType;
    SymantecCode retCode;
    SymantecAction symAction;

    switch (_hookType) {
        case SymantecOutbound:
            if (symantecApp->isCOSAuthoritative())
                symantecActionType = SymantecOutboundCos;
            else symantecActionType = SymantecOutboundConfig;
            break;
        case SymantecInbound:
            if (symantecApp->isCOSAuthoritative())
                symantecActionType = SymantecInboundCos;
            else symantecActionType = SymantecInboundConfig;
            break;
        case SymantecPostValidate:
            if (symantecApp->isCOSAuthoritative())
                symantecActionType = SymantecPostValidateCos;
            else symantecActionType = SymantecPostValidateConfig;
            break;
        default:
            symantecActionType = SymantecUnknown;
    }

    _msgInfo->setSymantecActionType(symantecActionType);
    // Symantec action determined following order per hook type
    if (_hookType == SymantecInbound) {

        // retrieve CoS action of all recipients
        for( int i =0; i < _msgInfo->getRecipCount(); i++) {

            Recipient* recip = _msgInfo->getRecipient(i);
            SymantecAction sym = recip->getSymantecAction();
            retCode = setSymantecActionPre(sym);
        } 
    } else if (_hookType == SymantecOutbound) {

        // Check sender CoS
        Sender* sender = _msgInfo->getSender();
        symAction = sender->getSymantecAction();
        retCode = setSymantecActionPre(symAction);

    } else { //PostValidate hook

        // check first sender CoS
        Sender* sender = _msgInfo->getSender();
        symAction = sender->getSymantecAction();
        retCode = setSymantecActionPre(symAction);

        // if sender's action invalid then check recipients CoS
        if ((!_CleanVirus) && (!_ScanVirus) && symAction != SymantecActionAllow) {

            // retrieve CoS action of all recipients
            for( int i =0; i < _msgInfo->getRecipCount(); i++) {

                Recipient* recip = _msgInfo->getRecipient(i);
                SymantecAction sym = recip->getSymantecAction();
                retCode = setSymantecActionPre(sym);
            } 
        }
    }
 
    // check overall message av scan action flags to determine action
    if ((!_CleanVirus) && (!_ScanVirus)) {

       DIAGC("symantecHandler", 5, "Exit NotScanned");
       return SymantecNotScanned;
    }

    DIAGC("symantecHandler", 5, "Exit PreProcess");
    return retCode;
}

//+---------------------------------------------------------------------------
//
// Method:    Handler::setSymantecActionPre
//
// Synopsis:  Determine resultant action prior to scan a message i.e. scan only
//            / repair / clean
//
//----------------------------------------------------------------------------
SymantecCode Handler::setSymantecActionPre(SymantecAction symAction)
{
    DIAGC("symantecHandler", 5, "Entering setSymantecActionPre");
    switch (symAction) {
        case SymantecActionRepair:
            _RepairMsg = true;
        case SymantecActionClean:
            _CleanVirus = true;
            break;
        case SymantecActionInvalid:
            DIAGC("symantecHandler", 5, "InvalidAction, return");
            return SymantecFailure;
            break;
        case SymantecActionNull:
        case SymantecActionAllow:
            break;
        case SymantecActionReject:
        case SymantecActionSideline:
        case SymantecActionDiscard:
        default:
            _ScanVirus  = true;
            break;
    } // switch

   DIAGC("symantecHandler", 5, "Exit, setSymantecActionPre");
   return SymantecSuccess;
}

//+---------------------------------------------------------------------------
//
// Method:	Handler::Process
//
// Synopsis:	Determine scan verdict of the message and mark clean flag 
//              accordingly.
//
//----------------------------------------------------------------------------
SymantecCode Handler::Process()
{
    DIAGC("symantecHandler", 5, "Entering Handler::Process");
    SymantecVerdict retCode = SymantecVerdictFailed;
    string virusName;
    SymantecScanner* scanner = symantecApp->getSymantecScanner();
    bool scanAndClean = symantecApp->isScanAndClean();

    if (_wHeader) delete _wHeader;
    if (_wBody) delete _wBody;

    try {

        if (_CleanVirus || _RepairMsg) { 
    
            DIAGC("symantecHandler", 5, "Cleaning a message"); 
            _wHeader = new SMappedFileWrite();
            _wBody = new SMappedFileWrite();
            bool repairFlg = _RepairMsg;
           
            // clean previous scan result from msg object
            _msgInfo->clearFileName();
            _msgInfo->clearVirusName(); 
            _msgInfo->setRepaired(false);
            _msgInfo->setSymantecVerdict(SymantecVerdictOkay); 

            // check if scan and clean flag ON
            if (scanAndClean) {

                // scan a message
                retCode = scanner->scanMessage(_msgInfo->getHeader(),
                                               _msgInfo->getBody(), _msgInfo, false);

                // take action if message suspect otherwise return
                if (retCode == SymantecVerdictSuspectNoRepaired ||
                    retCode == SymantecVerdictSuspectRepaired) {

                    _msgInfo->clearFileName();
                    _msgInfo->clearVirusName(); 
                    _msgInfo->clearProblemDetails();
                    _msgInfo->clearSuspectMsgPartFlag();

                    // clean a suspected message
                    retCode = scanner->cleanMessage(_msgInfo, _wHeader, _wBody, _rHeader, _rBody, repairFlg);
                }
            } else { 

                retCode = scanner->cleanMessage(_msgInfo, _wHeader, _wBody, _rHeader, _rBody, repairFlg);
            }

        } else if (_ScanVirus) {
       
            int n=0;
            DIAGC("symantecHandler", 5, "Scanning a message");

            // Scan body only if mail sent from telnet
            if ((n=strncasecmp(MIME_FORM, _msgInfo->getBody()->str, MIME_LEN)) == 0) {
            
                // mime format message - scan with header and body
                retCode = scanner->scanMessage(_msgInfo->getHeader(),
                                               _msgInfo->getBody(), _msgInfo, false);
            } else {

                // Smtp message not in well mime format and SPE does not recognize it.
                // smtp message has potential unsafe contain in body, sent header empty to scanner
                retCode = scanner->scanMessage(NULL, _msgInfo->getBody(), _msgInfo, false);
            }
        }

    } catch(exception& ex) {

        LOG(Notification, "Exception occured in process a message;", "%s", ex.what());
    } catch(...) {

        LOG(Notification, "Unknown exception in process a message");
    }

    switch (retCode) {
        case SymantecVerdictOkay:
            DIAGC("symantecHandler", 5, "Exit, VerdictOkey Process");
            _cleanMail = true;
            return SymantecCleanMail;
        case SymantecVerdictSuspectNoRepaired:
        case SymantecVerdictSuspectRepaired:
           
            _postProcess();
            DIAGC("symantecHandler", 5, "Exit, VerdictSuspect Process");
            return SymantecSuccess;
        case SymantecVerdictTimeout:
            return SymantecTimeout;
        case SymantecVerdictBadMsg:
            return SymantecNotCleaned;
        case SymantecVerdictScanFailed:
            return SymantecScanFailed;
        case SymantecVerdictUnknown:
        case SymantecVerdictFailed:
        default:
            return SymantecFailure;
    }

    return SymantecSuccess;
}


//+---------------------------------------------------------------------------
//
// Method:    Handler::commitTimeout
//
// Synopsis:  commit mail with defer disposition for all recipients
//
//----------------------------------------------------------------------------
void Handler::commitTimeout()
{
    DIAGC("symantecHandler", 5, "Entering commitTimeout");

    string strAction(symantecApp->getSymTimeoutAction());
    if (symantecApp->isMsgcatLoggingEnabled()) {
        REPORT_SymnAVMsgTimeout(SLOGHANDLE, Error, getMsgInfo()->getTotalTime(), 
                                 strAction.c_str(), getMsgInfo()->getMailContext());
    } else {
        LOG(Error, "MsgTrace", "SymTimeout; %s; TimeoutAction=%s; %s",
           getMsgInfo()->getTotalTime(), strAction.c_str(), 
           getMsgInfo()->getMailContext()); 
    }

    SymantecAction symAction = symantecApp->getSymantecAction(strAction);
    _addMailInfo(symAction);
    commitResponse();

    DIAGC("symantecHandler", 5, "Exit commitTimeout"); 
}


//+---------------------------------------------------------------------------
//
// Method:   Handler::deferMessage
// 
// Synopsis: commit mail with defer disposition for all recipient.
//
//---------------------------------------------------------------------------
void Handler::deferMessage()
{
    DIAGC("symantecHandler", 5, "Entering deferMessage");
    _addMailInfo(SymantecActionDefer);
    commitResponse();
    DIAGC("symantecHandler", 5, "Exit deferMessage");
}


//+---------------------------------------------------------------------------
//
// Method:   Handler::takeBadMsgAction
// 
// Synopsis: commit mail with defer disposition for all recipient.
//
//---------------------------------------------------------------------------
void Handler::takeBadMsgAction()
{
    DIAGC("symantecHandler", 5,  "Entering takeBadMsgAction");
    SymantecAction symAction = SymantecActionReject;
    _addMailInfo(symAction);
    commitResponse();
}


//+---------------------------------------------------------------------------
//
// Method:   Handler::takeScanFailedAction()
//
// Synopsis: commit action on a mail as per config on scan process failed event.
//
//---------------------------------------------------------------------------
void Handler::takeScanFailedAction()
{
    DIAGC("symantecHandler", 5,  "Entering takeScanFailedAction");

    string action = symantecApp->getSymAVScanFailedAction();
    SymantecAction symAction = symantecApp->getSymantecAction(action);
    bool discard = false;
    string msgAction("Allow");

    if ((_CleanVirus || _RepairMsg) && symantecApp->isScanAndClean()) {

        // hard set to action Discard
        discard = true;
        msgAction.assign("Discard");
    } else {

        if (strcasecmp("Discard", action.c_str()) ==0) {
            discard = true;
            msgAction.assign("Discard");
        }
    }


    if (discard) {


        bool noticeSent = generateNotice(symantecApp->getSymAVScanFailNoticeHeader(),
                              symantecApp->getSymAVScanFailNoticeBody());

        if (noticeSent) {

            // sender address
            SymString strFrom(_msgInfo->getMailFrom()->str, _msgInfo->getMailFrom()->len);
            SymString strNoAngleSender = strFrom.Segment(1, int_cast(strFrom.size())-2);

            if (symantecApp->isMsgcatLoggingEnabled()){
                REPORT_SymnAVScanFailNoticeGenerated(SLOGHANDLE, Notification, 
                    "Sender", strNoAngleSender.AsPtr());
            } else {
                LOG(Notification, "MsgTrace", "%s Scan failed notice generated for %s",
                    "Sender", strNoAngleSender.AsPtr());
            }
        } else {
            LOG(Notification, "Scan failed notice not generated !"); 
        }

        _addMailInfo(symAction);
    } else {

        _addMailInfo(SymantecActionAllow);
    }

    commitResponse();

    if (symantecApp->isMsgcatLoggingEnabled()) {

        REPORT_SymnAVScanFailed(SLOGHANDLE, Notification, "ScanFailed, action=",
                                msgAction.c_str(),
                                getMsgInfo()->getTotalTime(),
                                getMsgInfo()->getMailContext());
    } else {
        LOG(Notification, "MsgTrace", "ScanFailed, action=%s; %s; %s;",
            msgAction.c_str(),
            getMsgInfo()->getTotalTime(),
            getMsgInfo()->getMailContext());
    }

    DIAGC("symantecHandler", 5,  "Exit takeScanFailedAction");
}

//+---------------------------------------------------------------------------
//
// Method:    Handler::commitWithNewHeader
//
// Synopsis:  commit mail with new message body in case of bounce mail.
//
//----------------------------------------------------------------------------
void Handler::commitXHeader()
{
    DIAGC("symantecHandler", 5,  "Entering commitXHeader");
    _mailDelivery->commitResponse(0,0,0,0,
                                  _msgInfo->getNewHeader(),
                                  _msgInfo->getNewBody());
}

//+---------------------------------------------------------------------------
//
// Method:    Handler::Commit
//
// Synopsis:  Using MailInfoArray data, create recipient, disposition,
//            header objects and commitResponse().  When we commitReponse the
//            mail will be sent back to extension server and depending on the
//            disposition extension server  sets the actionCode and sends the
//            message back to MTA.
//
//----------------------------------------------------------------------------
SymantecCode Handler::Commit()
{
    DIAGC("symantecHandler", 5,  "Entering Commit");
    bool bNotice = false;
    string strMsg;

    // commit overall response
    commitResponse();
    
    //string symAction(symantecApp->getSymantecInboundAction());

    if (!_cleanMail) {

        // preparing the array of recipient for virus notice generation
        if (symantecApp->isSenderNoticeEnabled()) {

            // generate notice
            bNotice = generateNotice(symantecApp->getSymAVNoticeHeaderSender(), 
		                             symantecApp->getSymAVNoticeBodySender(), true);
            
        } else if (symantecApp->isSenderOptInNotice()) {


            SymantecAction senderAction = _msgInfo->getSender()->getSymantecAction();

            if ((senderAction != SymantecActionNull) || (senderAction != SymantecActionAllow)) {
                // then send the virus notification to them
                bNotice = generateNotice(symantecApp->getSymAVNoticeHeaderSender(),
                                     symantecApp->getSymAVNoticeBodySender(), true);
            }
        } 

        if (bNotice == true) {

            // sender address
            SymString strFrom(_msgInfo->getMailFrom()->str, _msgInfo->getMailFrom()->len);
            SymString strNoAngleSender = strFrom.Segment(1, int_cast(strFrom.size())-2);

            if (symantecApp->isMsgcatLoggingEnabled()){
                REPORT_SymnAVVirusNoticeGenerated(SLOGHANDLE, Notification, 
                    "Sender", strNoAngleSender.AsPtr());
            } else {
                LOG(Notification, "MsgTrace", "%s virus notice generated for %s",
                    "Sender", strNoAngleSender.AsPtr());
            }
        } else {
            LOG(Notification, "Virus notice not generated for Sender !");
        }

        if (symantecApp->isRecipientNoticeEnabled() || 
            symantecApp->isRemoteRecipientNoticeEnabled()) {

            bNotice =  generateNotice(symantecApp->getSymVirusNoticeHeaderRecip(),
                                    symantecApp->getSymVirusNoticeBodyRecip(), false); 

            if (bNotice == true) {

                // check virus notice flag to set message
                const char* choice = symantecApp->getSymVirusNotice();
                if (strcasecmp("Recipient", choice) == 0 ||
                    strcasecmp("LocalRecipient", choice) == 0) {
                    strMsg.assign("Local Recipients");

                } else if (strcasecmp("RemoteRecipient", choice) == 0) {
                    strMsg.assign("Remote Recipients");

                } else if (strcasecmp("AllRecipient", choice) == 0) {
                    strMsg.assign("Remote and local Recipients");

                } else {
                    strMsg.assign("Local Recipients");
                }


                if (symantecApp->isMsgcatLoggingEnabled()){
                    REPORT_SymnAVVirusNoticeGenerated(SLOGHANDLE, Notification, 
                        strMsg.c_str(), _strVNoticeRecipients.c_str());
                } else {
                    LOG(Notification, "MsgTrace", "%s virus notice generated for %s",
                        strMsg.c_str(), _strVNoticeRecipients.c_str());
                }
     
            } else {
                LOG(Notification, "Virus notice not generated for ", _strVNoticeRecipients.c_str());
            }
        }
    }

    if (symantecApp->isMsgcatLoggingEnabled()){
        REPORT_SymnAVMsgProcessed(SLOGHANDLE, Notification, getMsgInfo()->getTotalTime(),
                                  _strRcptDisp.c_str(),  getMsgInfo()->getMailContext());
    } else {
        LOG(Notification, "MsgTrace", "Processed; %s; SymAction=%s; %s",
            getMsgInfo()->getTotalTime(),
            _strRcptDisp.c_str(),
            getMsgInfo()->getMailContext());
    }
    return SymantecSuccess;
}

//+---------------------------------------------------------------------------
//
// Synopsis:	Add a record for each for 1. Clean, 2. Reject, 3. Sideline
//		4. Discard, Invalid and Allow.
//			
//		If the recip is NULL then all the recipient are scanned and
//		RcptTo and RcptToDisp will be created.
//
//----------------------------------------------------------------------------
void Handler::_addMailInfo(SymantecAction symAction, Recipient* recp)
{
    DIAGC("symantecHandler", 5, "Entering addMailInfo");
	
    StringInfo* newHeader = NULL;	
    StringInfo* newBody = NULL;
    ActionKey key;

    if (!_cleanMail) {

        switch (symAction) {
            case SymantecActionReject:
                // New Header and old body
                // The cleaned msg body is stored in Handler
                _msgInfo->addSymantecExtSvcXHeader();
                
                newHeader = (StringInfo*) _msgInfo->getNewHeader();
                key = ActionNewHeader;
                break;
            case SymantecActionClean:
                {
                    key = ActionNewBody;
                    // suspect mail, delete part of infected mail body
                    if(_msgInfo->isHeaderRewritten()) {
                        newHeader = (StringInfo*)_msgInfo->getNewHeader();
                    } else {
                        newHeader = (StringInfo*)_msgInfo->getHeader();
                    }
                    newBody = (StringInfo*)_msgInfo->getNewBody();
                }
                break;
           case SymantecActionRepair:
                // Old Header and New Body
                // The cleaned msg body is stored in Handler
                DIAGC("symantecHandler", 5, "ActionRepair");
                newBody = (StringInfo*)_msgInfo->getNewBody();
                key = ActionNewBody;
                break;
           case SymantecActionDiscard:
                // If mail suspect and action set to discard
                // extension service send virus notice to sender
                
           case SymantecActionSideline:
           case SymantecActionNull:
           case SymantecActionInvalid:
           case SymantecActionAllow:
           default:
               // Old header and old body
               key = ActionDefault;
               break;
        }
    } else {
    
        switch (symAction) {
            case SymantecActionReject:
            case SymantecActionRepair:
            case SymantecActionClean:
            case SymantecActionSideline:
            case SymantecActionDiscard:
                key = ActionNewXScannedHdr;
                break;
            case SymantecActionNull:
            case SymantecActionInvalid:
            case SymantecActionAllow:
                default:
                // old header and old body
                key = ActionDefault;
                break;
        }
    }		

    // check if record exist
    if (_mailActionArray.count(key) == 0) {
        // Create new record for action
        MailInfo mailInfo;
        mailInfo.header = NULL;
        mailInfo.body = NULL;
        std::pair<MailInfoArray::iterator, bool> ret;
        ret = _mailActionArray.insert(make_pair( key, mailInfo));
		
        if (ret.second == false) 
	    LOG(Error, "Value not inserted in mailactionarray !");
    }
	
    _mailActionArray[key].header = newHeader;
    _mailActionArray[key].body = newBody;

    StringInfo* rcptToDisp; 
    string dispStr;
    if (!_cleanMail && symAction != SymantecActionInvalid 
        && symAction != SymantecActionNull) {

        rcptToDisp = symantecApp->getDisposition(symAction);
    } else {
        // This is clean mail or invalid action of recipient 
        // "Allow - keep disposition"
        rcptToDisp = symantecApp->getDisposition(SymantecActionAllow);
    }

    if (recp) {
        RecipInfo* recipInfo = _createRecipInfo(rcptToDisp, recp); 
        _mailActionArray[key].recipInfoArray.push_back(recipInfo);

        // create disposition list
        _strRcptDisp.append( rcptToDisp->str, rcptToDisp->len);
        _strRcptDisp.append( ";");

        if (!_cleanMail) {
            // add record for virus notice
            _addVirusNoticeRecipInfo(recipInfo, recp, symAction);
        }
        DIAGC("symantecHandler", 5, "Exit, per recp addMailInfo");
        return;	
    }

    for (int index=0; index < _recipCount; index++) {

        Recipient* recip = _msgInfo->getRecipient(index);
        RecipInfo* recipInfo = _createRecipInfo(rcptToDisp, recip);

        _mailActionArray[key].recipInfoArray.push_back(recipInfo);

        // create disposition list
        _strRcptDisp.append( rcptToDisp->str, rcptToDisp->len);
        _strRcptDisp.append( ";");

        if (!_cleanMail) {
            // Add a record for virus notices
            _addVirusNoticeRecipInfo(recipInfo, recip, symAction);
        }
    }

    _virusNoticeCount = _recipCount;
    DIAGC("symantecHandler", 5, "Exit addMailInfo");
}

//+---------------------------------------------------------------------------
//
// Method:    Handler::_createRecipInfo
//
// Synopsis:  Create RecipInfo object
//
//----------------------------------------------------------------------------
Handler::RecipInfo* Handler::_createRecipInfo(StringInfo* disp, Recipient* recip)
{
    DIAGC("symantecHandler", 5, "Entering createRecipInfo");
    RecipInfo* recipInfo = new RecipInfo;
    recipInfo->rcptTo = (StringInfo*) recip->getAddrStrInfo();
    recipInfo->rcptToDisp  = disp;

    return recipInfo;
}

//+---------------------------------------------------------------------------
//
// Method:    Handler::_addVirusNoticeRecipInfo
//
// Synopsis:  Add a record to _virusNoticeRecipArray if needed
//
//----------------------------------------------------------------------------
void Handler::_addVirusNoticeRecipInfo(RecipInfo* recipInfo, Recipient* recip, 
                                       SymantecAction symAction)
{
    DIAGC("symantecHandler", 5, "Entering addVirusNoticeRecipInfo");
    
    if (symAction == SymantecActionAllow || symAction == SymantecActionNull)
        return;	

    // VirusNotice is disabled for both LOCAL and
    // REMOTE recipients.
    if (!symantecApp->isRemoteRecipientNoticeEnabled() && 
        !symantecApp->isRecipientNoticeEnabled())
        return;

    // check for remote recipient
    if (recip->isRemoteAddress() && 
        (!symantecApp->isRemoteRecipientNoticeEnabled()))
        return;

    // check for local recipient
    if (!recip->isRemoteAddress() && 
         (!symantecApp->isRecipientNoticeEnabled()))
        return; 

    // add recipient info 
    _virusNoticeRecipArray.push_back(recipInfo);

    if (Len(_strVNoticeRecipients))
        _strVNoticeRecipients += ",";

    _strVNoticeRecipients.append( recipInfo->rcptTo->str, recipInfo->rcptTo->len);
    DIAGC("symantecHandler", 5, "Exit addVirusNoticeRecipInfo");
}

//+---------------------------------------------------------------------------
//
// Method:    Handler::_postProcess
//
// Synopsis:  When the mail is virus suspect then add MailInfo records
//
//----------------------------------------------------------------------------
void Handler::_postProcess()
{
    DIAGC("symantecHandler", 5, "Entering postProcess"); 
    SymantecActionBase symActBase = _msgInfo->getSymantecActionType(); 

    switch (symActBase) {
        case SymantecOutboundCos:
        case SymantecOutboundConfig:
            _addMailInfo(_msgInfo->getSender()->getSymantecAction());
            break;
        case SymantecInboundCos:
        case SymantecInboundConfig:
            {
                if (!_cleanMail) {
                
                    for (int i=0; i < _recipCount; i++) {

                        Recipient* recip = _msgInfo->getRecipient(i);
                        _addMailInfo(recip->getSymantecAction(), recip);
                    }
                } 
            }
            break;
        case SymantecPostValidateCos:
        case SymantecPostValidateConfig:
            {
                SymantecAction action = _msgInfo->getSender()->getSymantecAction();
                if (action != SymantecActionInvalid && action != SymantecActionNull) {

                    _addMailInfo(_msgInfo->getSender()->getSymantecAction());
                 
                } else {

                    for (int i=0; i < _recipCount; i++) {

                        Recipient* recip = _msgInfo->getRecipient(i);
                        _addMailInfo(recip->getSymantecAction(), recip);
                    }
                }
            } 
            break;
        case SymantecVarious:
            break;
        case SymantecUnknown:
        default:
            return;
    }

    DIAGC("symantecHandler", 5, "Exit postProcess"); 
} 

void Handler::commitResponse()
{
    DIAGC("symantecHandler", 5, "Entering commitResponse");
    
    for(MailInfoArray::iterator pAction = _mailActionArray.begin();
        pAction != _mailActionArray.end(); pAction++) {

        RecipInfoArray pInfo = pAction->second.recipInfoArray;

        if (pInfo.empty())
            continue;

        unsigned int count = pInfo.size();
        StringInfo** infoRcptTo	= NULL;
        StringInfo** infoRcptToDisp = NULL;
		
        infoRcptTo = new StringInfo*[count];
        infoRcptToDisp = new StringInfo*[count];

        for(int index=0; index < int_cast(pInfo.size()); index++) {
            infoRcptTo[index] = pInfo[index]->rcptTo;
            infoRcptToDisp[index] = pInfo[index]->rcptToDisp;
        }

        // Group all the recipients and disposition and do one commit
        _mailDelivery->commitResponse(0,
                                      count,
                                      (const StringInfo**) infoRcptTo,
                                      (const StringInfo**) infoRcptToDisp,
                                      (const StringInfo*) pAction->second.header,
                                      (const StringInfo*) pAction->second.body);
        delete[] infoRcptTo;
        delete[] infoRcptToDisp;
    }
    DIAGC("symantecHandler", 5, "Exit commitResponse");
}

void Handler::commitNoChange()
{
    DIAGC("symantecHandler", 5, "Entering commitNoChange");
    _mailDelivery->commitResponse(0, 0, 0, 0, 0, 0);
    DIAGC("symantecHandler", 5, "Exit commitNoChange");
}


bool Handler::generateNotice(const char* header, const char* body, bool bSender)
{
    DIAGC("symantecHandler", 5, "Entering generateNotice");

    SymString strHeader(header);
    SymString strBody(body);
    SymString strFrom(_msgInfo->getMailFrom()->str, _msgInfo->getMailFrom()->len);
    SymString strNoAngleSender = strFrom.Segment(1, int_cast(strFrom.size())-2);
    SymString strFileName(_msgInfo->getFileName());
    SymString strVirusName(_msgInfo->getVirusName());
    SymString strDateSubm(_msgInfo->getDateSubmitted());
    SymString strVirusNoticeFrm(symantecApp->getVirusNoticeFrom());
    SymString strMsgID(_msgInfo->getMessageID().c_str());
    SymString strMsgSub(_msgInfo->getSubject().c_str());
    SymString strRecpList(_msgInfo->getRecipientList());
    SymString strProblemStr(_msgInfo->getProblemDetails());
 
    if (!strFileName.size()) strFileName.assign("Your message");
 
    strHeader.S("<Sender>",    strNoAngleSender, "g");
    strHeader.S("<Virusname>", strVirusName.AsPtr(), "g");
    strHeader.S("<Filename>",  strFileName.AsPtr(), "g");
    strHeader.S("<Date>",      strDateSubm.AsPtr(), "g");
    strHeader.S("<Postmaster>",strVirusNoticeFrm.AsPtr(), "g");
    strHeader.S("<Message-ID>",strMsgID.AsPtr(), "g");
    strHeader.S("<Subject>",   strMsgSub.AsPtr(), "g");
    strHeader.S("<Problems>",  strProblemStr.AsPtr(), "g");

    if (bSender)
        strHeader.S("<Recipients>", strRecpList.AsPtr(), "g");

    strHeader.S("\n", "\r\n", "g");
    strHeader += "\r\n";
	
    strBody.S("<Sender>",    strNoAngleSender, "g");
    strBody.S("<Virusname>", strVirusName.AsPtr(), "g");
    strBody.S("<Filename>",  strFileName.AsPtr(), "g");
    strBody.S("<Date>",      strDateSubm.AsPtr(), "g");
    strBody.S("<Postmaster>",strVirusNoticeFrm.AsPtr(), "g");
    strBody.S("<Message-ID>",strMsgID.AsPtr(), "g");
    strBody.S("<Subject>",   strMsgSub.AsPtr(), "g");
    strBody.S("<Problems>",  strProblemStr.AsPtr(), "g");

    if (bSender)
        strBody.S("<Recipients>", strRecpList.AsPtr(), "g");

    strBody.S("\n", "\r\n", "g");
    strBody += "\r\n";

    StringInfo* newMailFrom = new StringInfo;
    StringInfo* newHeader = new StringInfo;
    StringInfo* newBody = new StringInfo;
  
    if (newMailFrom != NULL) { 
        newMailFrom->str = new char[strVirusNoticeFrm.size()];
        if (newMailFrom->str != NULL) {
            memcpy((void*) newMailFrom->str, strVirusNoticeFrm.AsPtr(), strVirusNoticeFrm.size());
            newMailFrom->len = strVirusNoticeFrm.size();
        }
    }
   
    if (newHeader != NULL) { 
        newHeader->str = new char[strHeader.size()];
        if (newHeader->str != NULL) {
            memcpy((void*)newHeader->str, strHeader.AsPtr(), strHeader.size());
            newHeader->len = strHeader.size();
        }
    }

    if (newBody != NULL) {
        newBody->str = new char[strBody.size()];
        if (newBody->str != NULL) {
            memcpy((void*)newBody->str, strBody.AsPtr(), strBody.size());
            newBody->len = strBody.size();
        }
    }

    StringInfo** strDispositions = NULL;
    StringInfo** strRecipients = NULL;
    int count = 0;

    StringInfo* pInfo;
    string strMsg;
    if (bSender) {

        count =1;
        strDispositions = new StringInfo* [count];
        strRecipients 	= new StringInfo* [count];
        pInfo = symantecApp->getDisposition(SymantecActionAllow);
        strDispositions[0] = pInfo;
        strRecipients[0] = (StringInfo*) _msgInfo->getMailFrom(); 
        strMsg.assign("Sender");

    } else {
        // Get the count of recipients we should send notices.
        int virusNRecipCount = _virusNoticeRecipArray.size();

        count = virusNRecipCount;
        strDispositions = new StringInfo* [count];
        strRecipients   = new StringInfo* [count];
        
        for (int iCount=0; iCount < virusNRecipCount; iCount++) {
            strDispositions[iCount] = symantecApp->getDisposition(SymantecActionAllow);
            strRecipients[iCount]   = _virusNoticeRecipArray[iCount]->rcptTo;
        }

    }
    
    // flag notice send
    bool flag = true;
    if (count > 0) {    
        _mailDelivery->commitResponse((const StringInfo*) newMailFrom,
                                     count,
                                     (const StringInfo**) strRecipients,
                                     (const StringInfo**) strDispositions,
                                     newHeader,
                                     newBody);
    } else {
        flag = false;  
    }   

    // release memory
    if (strDispositions != NULL)
        delete[] strDispositions;

    if (strRecipients != NULL)
        delete[] strRecipients;

    if (newMailFrom != NULL) {
        if (newMailFrom->str != NULL)
            delete[] newMailFrom->str;
        delete newMailFrom;
    }

    
    if (newHeader != NULL) {
        if (newHeader->str != NULL)
            delete[] newHeader->str;
        delete newHeader;
    }

    if (newBody != NULL) {
        if (newBody->str != NULL)
            delete[] newBody->str;
        delete newBody;
    }

    DIAGC("symantecHandler", 5, "Exit generateNotice");   
    return flag;
}

