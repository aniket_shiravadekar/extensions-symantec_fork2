################################################################################
# Copyright 2003 Openwave Systems, Inc. All Rights Reserved.
#
# The copyright to the computer software herein is the property of 
# OpenWave Systems, Inc.  The software may be used and/or copied only with
# The written permission of OpenWave Systems, Inc. or in accordance with the
# terms and conditions stipulated in the agreement/contract under which
# the software has been supplied.
################################################################################

PROJECT_ROOT	:=	../../mercury
include		$(PROJECT_ROOT)/mtools/project.mk

SYMANTEC_EXT_ROOT	:= .

include		$(SYMANTEC_EXT_ROOT)/mtools/project.mk
include		$(SYMANTEC_EXT_ROOT)/mtools/Version.mk



SYMANTEC_NAME	= symnavscan

ifeq ($(MPLATFORM), Linux)
    ifeq ($(BUILDING_64), 1)
        PLATFORM_DEFINES += -D_64BIT_ -DUNIX -DLINUX
    endif
endif
DEFINES	    	= $(PLATFORM_DEFINES) $(THREAD_DEFINES) -DUNIX  $(SYMANTEC_DEFINES)
INCLUDES	= -I. -I$(SYMANTEC_INCDIR) $(MX_INCLUDES) $(MSGCAT_INCLUDES)

MPLATFORM := $(shell uname -s)

ifeq ($(MPLATFORM), SunOS)
    ifeq ($(BUILDING_64), 1)
        all::
	@echo "Note: 64 bit Symantec engine not available for Solaris."
	@exit 1
    endif
endif

# Complete list of libraries for linking

LIBS            = $(MXSDK_LIB_STATIC) $(SSL_LIB_STATIC) $(SYMANTEC_LIB)
DEPS        	= $(LIB_IM) $(LIB_ES)

ARCH_LIBS   	= $(THREAD_LIBS) $(PLATFORM_LIBS)

#####################
vpath %m $(MSGCAT_ROOT)
LOGMSGCAT		:=	$(SYMANTEC_NAME).cat

LOGMFILES		=	Symantec.m \
				$(END_LIST)

LOGMHXXFILES		:=	$(LOGMFILES:.m=.hxx)

$(LOGMHXXFILES) : %.hxx : %.m
	$(BUILD_LOGAPI_MHXXFILES)

DEPEND_FILES		+=	$(LOGMHXXFILES)
TARGETS			+=	$(LOGMHXXFILES) $(LOGMSGCAT)

$(LOGMSGCAT) : $(LOGMFILES)
	$(BUILD_LOGMSGCAT)



#####################

END_OF_FILES = 

HFILES = \
	symantecapp.hxx \
	symantecscan.hxx \
	symantechandler.hxx \
	msginfo.hxx \
	syutils.hxx \
	mmapfile.hxx \
	$(END_OF_FILES)

CFILES = \
	symantecapp.cxx \
	symantecscan.cxx \
	symantechandler.cxx \
	msginfo.cxx \
	symantec.cxx \
	syutils.cxx \
	mmapfile.cxx \
	$(END_OF_FILES)

MCA_SHAREDLIB_OBJS = \
	symantec.o \
	symantecapp.o \
	symantecscan.o \
	symantechandler.o \
	msginfo.o \
	syutils.o \
	mmapfile.o \
	$(END_OF_FILES)

SYMANTEC_VER_SHAREDLIB	= $(SYMANTEC_NAME).$(EXTSVC_VER).$(SHAREDLIB_SUFFIX)
MCA_SHAREDLIB		= $(SYMANTEC_NAME).$(SHAREDLIB_SUFFIX)


DEPEND_FILES		+=	$(HFILES)
CLEAN_FILES 		+=	$(MCA_SHAREDLIB_OBJS) 
TARGETS			+=	$(MCA_SHAREDLIB) $(SYMANTEC_VER_SHAREDLIB)

############################################################################
MASTER_STAMPED = $(MCA_SHAREDLIB)
$(SYMANTEC_VER_SHAREDLIB): SHARED_LIB_FLAGS += $(LIB_REAL_NAME_FLAG)
$(SYMANTEC_VER_SHAREDLIB): $(MCA_SHAREDLIB_OBJS)
	$(RM) $@
	$(LD) $(LDFLAGS) $(SHARED_LIB_FLAGS) -o $@ $(MCA_SHAREDLIB_OBJS) $(LIBS)


$(MCA_SHAREDLIB): $(SYMANTEC_VER_SHAREDLIB)
	$(RM) $@
	$(LN) $(SYMANTEC_VER_SHAREDLIB) $@
	$(SYMLINK_LI_LIB)



############################################################################


include                 $(SIMPLE_MK)
