//----------------------------------------------------------------------------
//
// File Name:    symantecscan.cxx
//
// Synopsis:     SymantecScanner class implementation
//----------------------------------------------------------------------------
// Copyright 2017 Synchronoss, Inc. All Rights Reserved.
//
// The copyright to the computer software herein is the property of
// Synchronoss, Inc. The software may be used and/or copied only with
// The written permission of Synchronoss, Inc. or in accordance with the
// terms and conditions stipulated in the agreement/contract under which
// the software has been supplied.
//----------------------------------------------------------------------------

#include <iostream>
#include <string>
#include <fstream>

#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>

#include "xmimecontent.hxx"
#include "symantecscan.hxx"
#include "symantecapp.hxx"
#include "mmapfile.hxx"

using namespace std;

//+---------------------------------------------------------------------------
//
// Method:    SymantecScanner::SymantecScanner
//
// Synopsis:  Constructor for SymantecScanner class
//
//----------------------------------------------------------------------------
SymantecScanner::SymantecScanner()
{
    DIAGC("SymantecScanner", 5, "Entering symnAV Scanner ctor");
    SC_ERROR retCode = 0;
    
    // build spe connection string
    _strConnConfig = _getSymantecClientConfig(); 

    // build scan config string
    _scanClient = NULL;
    _initialized = false;

    try {
        // Initialize symantec client
        retCode = ScanClientStartUp(&_scanClient, (char*)_strConnConfig.c_str());
        if (retCode == SC_OK) {
            _initialized = true;
            LOG(Notification, "AVSymScanner", "Client start successfully !");
        }
        else {
            LOG(Error, "ScanClientStartUp Return :", "%d", retCode);
            throw "ScanClientStartUp Failed !";
        }

    } catch(exception &ex) {
        LOG(Fatal, "AVSymScanner, exception =", "%s", ex.what());
    } catch(...) {
        LOG(Fatal, "AVSymScanner", "generic exception");
    }

    DIAGC("symantecScanner", 5, "Exit symnAV Scanner ctor"); 
}


//+---------------------------------------------------------------------------
//
// Method:    SymantecScanner::_getSymantecClientConfig
//
// Synopsis:  returns symantec server IP, port and conn pool size
//
//----------------------------------------------------------------------------
string SymantecScanner::_getSymantecClientConfig()
{
    string strClientConfg;

    // get symantec server IP, port and conn pool size
    strClientConfg.append(symantecApp->getSymantecServerConnection());
    
    // get fail retry time in seconds
    strClientConfg.append("ReadWriteTime:");
    strClientConfg.append(symantecApp->getSymTimeoutSecs());
    strClientConfg.append(";;;");

    // get spe retry interval
    strClientConfg.append("FailRetryTime:");
    strClientConfg.append(symantecApp->getSymSpeRetryInterval());
    strClientConfg.append(";;;");
    
    return strClientConfg;	
}

//+---------------------------------------------------------------------------
//
// Method:    SymantecScanner::~SymantecScanner
//
// Synopsis:  destructor for class SymantecScanner
//
//----------------------------------------------------------------------------
SymantecScanner::~SymantecScanner()
{
    DIAGC("symantecScanner", 5, "Entering dtor SymantecScanner");
    ScanClientShutDown(_scanClient);
}

//+---------------------------------------------------------------------------
//
// Method:    SymantecScanner::scanMessage
//
// Synopsis:  Scan a messege
//
//----------------------------------------------------------------------------
SymantecVerdict SymantecScanner::scanMessage(const StringInfo* strHeader, 
                                             const StringInfo* strBody,
                                             MsgInfo* msg, bool repair)
{
    DIAGC("symantecScanner", 5, "Entering scanMessage");
    SymantecVerdict retCode;
    string virusName;

    try {

        // Scan message
        retCode = _doScanning(strHeader, strBody, msg, virusName, repair);    

        DIAGC("symantecScanner", 5, "Return doScanning; %d", retCode);

        msg->setSymantecVerdict(retCode, true);
 
        SymString actionTaken;
        if (repair) actionTaken = "Repair";
        else actionTaken = "Scan";

        if (retCode == SymantecVerdictSuspectNoRepaired || 
            retCode == SymantecVerdictSuspectRepaired) {

            if (symantecApp->isMsgcatLoggingEnabled())
                REPORT_SymnAVVirusDetected(SLOGHANDLE, Notification, actionTaken,
                             getVirusContext(virusName, "this message", msg));
            else
                LOG(Notification, "AVVirusDetected", "%s %s",
                             actionTaken.AsPtr(),
                             getVirusContext(virusName, "this message", msg).AsPtr());
        }
    } catch(exception& ex) {

        LOG(Notification, "Exception occured in scan a message; ", "%s", ex.what());  
    } catch(...) {

        LOG(Notification, "Unknown exception in scan a message");
    }
   
    return retCode;
}

//+---------------------------------------------------------------------------
//
// Method:    SymantecScanner::_doScanning
//
// Synopsis:  Scans input string
//
//----------------------------------------------------------------------------
SymantecVerdict SymantecScanner::_doScanning(const StringInfo* strHeader,
        const StringInfo* strBody,
        MsgInfo* msg,
        string& virusName,
        bool repair)
{
    SC_ERROR retCode;
    HSCANSTREAM hScanStream = NULL;
    char repairFile[512]={0};
    string optionStr;
    HSCANRESULTS results = NULL;
    int numProblems = 0;
    SymantecVerdict verdictScan;
    long numRetry = -1;

    // Start scan client
    if (repair)
         optionStr.append("RepairOnly:1");
    else 
        optionStr.append("ScanOnly:1");

    if (symantecApp->isICAPHttpClientIPEnabled()) {
        optionStr.append(";;;");
        optionStr.append("ICAPClientIP:");
        optionStr.append(symantecApp->getHostIP()); 
    }

    do {
        retCode = ScanClientStreamStart(_scanClient, (char*)"message", (char*)optionStr.c_str(), &hScanStream);
        if (retCode != SC_OK) {

            if (symantecApp->isMsgcatLoggingEnabled())
                REPORT_SymnAVScanFailedRetry(SLOGHANDLE, Notification, "ScanClientStreamStart; RetCode=",
                                             retCode, "RetryCnt=", numRetry+1);
            else 
                LOG(Notification, "ScanFailedRetry", "API=%s; RetCode=%d; RetryCnt=%d",
                    "ScanClientStreamStart", retCode, numRetry+1);  
        }

        switch(retCode) {
            case SC_OK:
                // No action  
                break;  
            case SC_CONNECT_FAILURE: 
            case SC_SOCKET_FAILURE:
            case SC_CONNPOOLSIZE_EXCEEDED:
            case SC_SERVER_TOO_BUSY:
                if(symantecApp->nSymConnRetry > 0 && numRetry < (symantecApp->nSymConnRetry-1)) {
                    numRetry++;
                    sleep(symantecApp->getSymSpeRetryIntervalSecs());
 
                    continue;   //recall API ScanClientStreamStart 
                }
                else {
                    return SymantecVerdictScanFailed;
                }
                break;
            case SC_MEMORY_ERROR:
                return SymantecVerdictFailed;
                break;
            case SC_INVALID_PARAMETER:
            default:
                return SymantecVerdictScanFailed;
                break;
        }

        //Connection successful, a message consider for further processing
        //Symantec AV Service tries to send stream of data to SPE via C API 
        if(strHeader != NULL && strHeader->len) {
            // Send message header for scanning
            retCode = ScanClientStreamSendBytes(hScanStream, (LPBYTE)strHeader->str, strHeader->len);
            DIAGC("symantecScanner", 5, "API ScanClientStreamSendBytes(Header); Return =%d", retCode);
        }

        if(retCode == SC_OK && strBody != NULL && strBody->len) {
            // Send message body for scanning
            retCode = ScanClientStreamSendBytes(hScanStream, (LPBYTE)strBody->str, strBody->len);
            DIAGC("symantecScanner", 5, "API ScanClientStreamSendBytes(Body); Return =%d", retCode);
        }


        if (retCode != SC_OK) {

            if (symantecApp->isMsgcatLoggingEnabled())
                REPORT_SymnAVScanFailedRetry(SLOGHANDLE, Notification, "ScanClientStreamSendBytes; RetCode=",
                                             retCode, "RetryCnt=", numRetry+1);
            else 
                LOG(Notification, "ScanFailedRetry", "API=%s; RetCode=%d; RetryCnt=%d",
                    "ScanClientStreamStart", retCode, numRetry+1);  
        }

        switch (retCode) 
        {
            case SC_OK:
                // No action  
                break;  
            case SC_SOCKET_FAILURE:
                if(symantecApp->nSymConnRetry > 0 && numRetry < (symantecApp->nSymConnRetry-1)) {
                    numRetry++;
                    sleep(symantecApp->getSymSpeRetryIntervalSecs());
                    continue;   //recall API ScanClientStreamStart
                }
                else {
                    return SymantecVerdictScanFailed;
                }
                break;
            case SC_INVALID_PARAMETER:
            default:
                return SymantecVerdictScanFailed;
                break;
        }

        //Service retrieve final result of the scan process via C API ScanClientStreamFinish
        //Fetch scan result of a message
        //call API to retrieve scan result

        retCode = ScanClientStreamFinish(hScanStream, repairFile, &results);
        if (retCode != SCSCANFILE_CLEAN && retCode != SCSCANFILE_INF_NO_REP 
            && retCode != SCSCANFILE_INF_REPAIRED) {

            if (symantecApp->isMsgcatLoggingEnabled())
                REPORT_SymnAVScanFailedRetry(SLOGHANDLE, Notification, "ScanClientStreamFinish; RetCode=",
                                             retCode, "RetryCnt=", numRetry+1);
            else 
                LOG(Notification, "ScanFailedRetry", "API=%s; RetCode=%d; RetryCnt=%d",
                    "ScanClientStreamFinish", retCode, numRetry+1);  
        }

        long timeLimit = symantecApp->getSymTimeoutLimitSecs();
        if (timeLimit > 0) {

            float timeConsumed = (msg->getTimeConsumed()/ 1000); // convert ms to secs
            if (timeConsumed > timeLimit) {
                msg->setSymantecVerdict(SymantecVerdictTimeout, true);
       
                if (results)
                    ScanResultsFree(results);

                return SymantecVerdictTimeout;
            }
        }

        switch(retCode){
            case SCSCANFILE_CLEAN:
                verdictScan = SymantecVerdictOkay;
                if (results)
                    ScanResultsFree(results);
                return verdictScan;
                break;  
            case SCSCANFILE_INF_NO_REP:
                {
                    verdictScan = SymantecVerdictSuspectNoRepaired;
                    msg->setRepaired(false);
                    // delete generated file
                    int ret = remove(repairFile);
                    DIAGC("symantecScanner", 5, "Remove generated repaired file, retCode= %d", ret);
                }
                break;
            case SCSCANFILE_INF_REPAIRED:
                {
                    // Suspect message has repaired
                    // read repaired message body from generated file
                    DIAGC("symantecScanner", 5, "Generated repaired a message file name= %s",
                            repairFile);
                    verdictScan = SymantecVerdictSuspectRepaired;
                    msg->setRepaired(true);
                    ifstream in(repairFile, std::ios::ate | std::ios::binary);

                    if (in.is_open()) {

                        // get size of file
                        int size = in.tellg();

                        // set position input to start of file
                        in.seekg(0, in.beg);

                        // read repaired file
                        char *buffer = new char[size+1];
                        in.read(buffer, size);
                        buffer[size] = '\0';  

                        StringInfo* pRepairedBody = new StringInfo();
                        pRepairedBody->str = buffer;
                        pRepairedBody->len = size+1;
                        msg->setRepairedBody(pRepairedBody);

                        DIAGC("symantecScanner", 5, "Read repaired message successfully !");

                    } else {

                        DIAGC("symantecScanner", 5, "Unable to open repaired a message file !");
                    }

                    in.close();
                    // delete generated file
                    int ret = remove(repairFile);
                    DIAGC("symantecScanner", 5, "Remove generated repaired file, retCode= %d", ret);
                }
                break;
            case SCSCANFILE_FAIL_MEMORY:
                if (results)
                    ScanResultsFree(results);
                return SymantecVerdictFailed;
                break;
            case SCSCANFILE_FAIL_ABORTED:
                if(symantecApp->nSymConnRetry > 0 && numRetry < (symantecApp->nSymConnRetry-1)) {
                    numRetry++;
                    sleep(symantecApp->getSymSpeRetryIntervalSecs());
                    continue;   //recall API ScanClientStreamStart
                }
                else {

                    if (results)
                        ScanResultsFree(results);
                    return SymantecVerdictScanFailed;
                }
                break;
            case SCSCANFILE_INVALID_PARAM:
            case SCSCANFILE_FAIL_RECV_FILE:
            case SCSCANFILE_FAIL_FILE_ACCESS:
            case SCSCANFILE_ERROR_SCANNINGFILE:
            case SCSCANFILE_ABORT_NO_AV_SCANNING_LICENSE:
            default:
                if (results)
                    ScanResultsFree(results);
                return SymantecVerdictScanFailed;
                break;
        }
        break;
    }while(numRetry < symantecApp->nSymConnRetry);

    // set verdict 
    msg->setSymantecVerdict(verdictScan,true);

    if (results) {
        if (ScanResultGetNumProblems(results, &numProblems) != SC_OK) {

            // free scan result structure
            if (results)
                ScanResultsFree(results);

            return verdictScan;
        }
    }

    DIAGC("symantecScanner", 5, "ScanResultNumProblem; %d", numProblems); 
    // consider clean part
    if (numProblems == 0) {

        LOG(Notification, "SymnAVScan found 0 infection.");

        if (results)
            ScanResultsFree(results); 

        msg->setSymantecVerdict(SymantecVerdictOkay, true);
        return SymantecVerdictOkay;
    }

    
    for(int i=0; i < numProblems; i++) {

        string fileName;
        int violationID = getScanProblemInfo(results, i, virusName, fileName, msg);
        
        // set virus name and file name
        msg->setProblemDetails(virusName, fileName);

        // set current virus details
        msg->setCurrentVirusDetails(virusName, fileName);

        DIAGC("symantecScanner", 5, "ProblemNum=%d, virusName=%s", i, virusName.c_str()); 

        if (violationID < 0) {

            // set policy violation happen
            // consider scan failed event here
            LOG(Notification, "MsgTrace", "Policy violation ; MsgID=%s; PolicyViolationID=%d",
                msg->getMessageID().c_str(),
                violationID);

            msg->setSymantecVerdict(SymantecVerdictScanFailed, true);
            return SymantecVerdictScanFailed;
        }
    }

    // free scan result structure
    if (results)
        ScanResultsFree(results);

    return verdictScan;
}


//+---------------------------------------------------------------------------
//
// Method:    SymantecScanner::getScanProblemInfo
//
// Synopsis:  Gets scanning problem, virus name, virus catagory(threat, spam etc.)
//
//----------------------------------------------------------------------------
int SymantecScanner::getScanProblemInfo(HSCANRESULTS hResults, 
                                        int iWhichProblem, string& virusName, 
                                        string& fileName, MsgInfo* msg)
{
    char attrib[512];
    int attrib_size = 512;
    string virusDefDt;
    string virusDefRevNo;
    int violationID = 1;

    // get virus contain file name 
    ScanResultGetProblem( hResults,
                        iWhichProblem,
                        SC_PROBLEM_FILENAME,
                        attrib,
                        &attrib_size );
    fileName.assign(attrib);

    attrib_size = 512;
    ScanResultGetProblem( hResults,
                        iWhichProblem,
                        SC_PROBLEM_VIRUSNAME,
                        attrib,
                        &attrib_size );
    // set virus name
    virusName.assign(attrib);

    // check of addition details of virus
    if (symantecApp->isReportDefInfoEnabled()) {

        // get virus defination date
        attrib_size = 512;
        ScanResultGetProblem( hResults,
                             iWhichProblem,
                             SC_PROBLEM_DEFINITION_DATE,
                             attrib,
                             &attrib_size );
        virusDefDt.assign(attrib);

        // get virus defination revision no.
        attrib_size = 512;
        ScanResultGetProblem( hResults,
                             iWhichProblem,
                             SC_PROBLEM_DEFINITION_REV,
                             attrib,
                             &attrib_size );
        virusDefRevNo.assign(attrib);

        LOG(Notification, "ReportDefInfo", 
            "MsgID=%s; filename=%s; virusname=%s; VirusDefDt=%s; VirusDefRev=%s",
            msg->getMessageID().c_str(), fileName.c_str(), virusName.c_str(),
            virusDefDt.c_str(), virusDefRevNo.c_str());
    }
     
    // check for policy violation
    attrib_size = 512;
    ScanResultGetProblem( hResults,
                         iWhichProblem,
                         SC_PROBLEM_VIRUSID,
                         attrib,
                         &attrib_size);
   
    violationID = atol(attrib);
    
    return violationID; 
}

//+---------------------------------------------------------------------------
//
// Method:    SymantecScanner::createMxMessage
//
// Synopsis:  Create Message object using Header and Body
//
//----------------------------------------------------------------------------
MxMessage* SymantecScanner::createMxMessage(MsgInfo* msg)
{
    MxMessage* message = NULL;

    try {

        if (msg->getSMappedHeader() == NULL ||
            msg->getSMappedBody() == NULL)
            return message;

        if (msg->getSMappedHeader()->IsCached() &&
            msg->getSMappedBody()->IsCached()) {

            // Header and Body are both in memory
            DIAGC("symantecScanner", 8, "Reading header and body from memory");

            message = new MxMessage(msg->getSMappedHeader(),
                                    msg->getSMappedBody());
        } else {
            // Either or both of Header and/or Body is on the disk
            if (msg->getSMappedHeader()->IsCached()) {
                msg->getSMappedHeader()->FlushCache();
            }
            msg->getSMappedHeader()->Flush(true);

            if (msg->getSMappedBody()->IsCached()) {
                msg->getSMappedBody()->FlushCache();
            }
            msg->getSMappedBody()->Flush(true);

            message = new MxMessage(msg->getSMappedHeader()->getFilename(),
                                    msg->getSMappedBody()->getFilename());

        }
    } catch(...) {
        delete message;
        message = NULL;
    }

    return message;
}

SymantecVerdict SymantecScanner::scanUUencodePart(MsgInfo* msg, const char* data, int length,
                                                  string& virus,
                                                  const char* filename,
                                                  bool bRepair,
                                                  SymString& str_NewBody)
{
    SymantecVerdict verdict = SymantecVerdictOkay;
    const char* pdata = data;
    SymString str_FileName(filename);
    DIAGC("symantecScanner", 5, "Entering UUencode scanning");
   
    if (length < 6) {
        return verdict; 
    }

    do {
        // Parse the message body for uuencoded attachment.
        // This process can subdivide to 4 step.
        // For example, when the message which incoming is as follows..
        //
        // -----------------------------------------
        // This is virus message.
        //
        // begin 666 eicar.com
        // <encoded data>
        //
        // end
        //
        // begin 666 ATT00002.htm
        // <encoded data>
        //
        // end
        //
        // -----------------------------------------
        //
        // Step 1) Find end mark
        //         Looking for the end of the data which uuencoded.
        //         In the case of the above example, end of eicar.com is finded first.
        //         If the message body have no "end" mark, we just break this loop.
        //
        // Step 2) Find "begin" mark
        //         Looking for the head portion corresponding to end mark which found in Step 1.
        //         We can know "begin 666 eicar.com" position now.
        //
        // Step 3) Scan
        //         Triming and scanning.
        //         We have the chunk of the following data.
        //         "begin 666 eicar.com
        //         <encoded data>
        //
        //         end"
        //
        //         And, pass this data directly to ScanBodyPart as a parameter.
        //
        // Step 4) proceed to the next section.
        //         If message body has other uuencoded part,
        //         we need to keep going.
        //         Start point is shifted for next loop,
        //         and go back to Step 1.
        //         In next loop, "ATT00002.htm" is processed.
        //
        
        // Step 1) Find end mark
        const char* end = strstr(pdata, "\nend");
        if (end == NULL) {
            // means we have no more UUencoded data or the data is broken.
            // we can now finish scanning.
            break;
        }

        if (end - pdata > length) {
            // Scan only within the range that we are asked for.
            break;
        }

        // Step 2) Find begin mark in backward from the end mark.
        // The idxBegin is the offset to the begin mark
        //
        SymString toScan(pdata, end - pdata);
        int idxBegin = toScan.RIndex("\nbegin");
        if (idxBegin < 0) {
            if (toScan.HasPrefix("begin"))
                idxBegin = 0;
            else {
                // proceed to the next section.
                pdata = end + 5;
                continue;
            }
        }

        DIAGC("symantecScanner", 3, "Rescann uuencode file" );       
        
        // Step 3) scan
        //
        SymString toScanStr = toScan.Segment(idxBegin, Len(toScan) - idxBegin); // trimming the scanning section

        StringInfo* strBody = new StringInfo();
        if (strBody != NULL) {
            strBody->str = toScanStr.AsPtr();
            strBody->len = Len(toScanStr);
        }

        StringInfo* strHeader = new StringInfo();
        strHeader->str = "";
        strHeader->len = 0;

        string virus;
        SymantecVerdict result = _doScanning(strHeader, strBody, msg, virus, bRepair);
       
        // set verdict with message
        msg->setSymantecVerdict(result);
 
        delete strBody;
        delete strHeader; 

        // check result
        if (result == SymantecVerdictSuspectRepaired ||
            result == SymantecVerdictSuspectNoRepaired) {

            verdict = result;
            if (bRepair && result == SymantecVerdictSuspectRepaired) {
                // repair ok
                str_NewBody += SymString(pdata, idxBegin);
                
                str_NewBody += SymString(msg->getRepairedBody()->str, msg->getRepairedBody()->len);
            } else {
                // if clean or repair fails, then don't proceed as this part should be
                // removed anyway.
                if (bRepair)
                    str_NewBody.Clear();

                break;
            }
        }

        // Step 4) proceed to the next section.
        //
        pdata = end + 5;
    } while (pdata && *pdata); 

    if (bRepair && str_NewBody.size())
        str_NewBody += SymString(pdata, data + length - pdata);

    DIAGC("symantecScanner", 5, "Exiting UUencode scanning", "%d", verdict);
    return verdict;
}

inline SymString GetHeaderValue(Entity* mEntity, const char* field)
{
    HeaderField *hdrField = mEntity->findHeader(field);
    if (hdrField != NULL)
        return SymString(hdrField->buffer() + hdrField->valueOffset() + 1,
                       hdrField->valueLen() - 1 );

    return "";
}

//+---------------------------------------------------------------------------
//
// Method:    SymantecScanner::getVirusContext
//
// Synopsis:  Create a string containing virus, filename and other mail info.
//
//----------------------------------------------------------------------------
SymString SymantecScanner::getVirusContext(SymString vname, SymString fname, MsgInfo* msg)
{

    SymString virusContext;
    virusContext  =   "Virus(es)="     + vname;
    virusContext += "; FileName="      + fname;
    if (msg != NULL) {
        virusContext += "; Sender="        + SymString(msg->getMailFrom()->str, msg->getMailFrom()->len);
        virusContext += "; RecipientList=" + SymString(msg->getRecipientList());
        virusContext += "; HostFromIP="    + SymString(msg->getPeerIP());
        virusContext += "; msgid="         + msg->getMessageID();
    }

    return virusContext;
}

//+---------------------------------------------------------------------------
//
// Method:    SymantecScanner::parseBodyPart
//
// Synopsis:  Read each SMime part, scan and clean virus
//
//----------------------------------------------------------------------------
SymantecVerdict SymantecScanner::parseBodyPart(MsgInfo*          msg,
                                               Entity*           mimeEntity,
                                               SMappedFileWrite* mfwHeader,
                                               SMappedFileWrite* mfwBody,
                                               bool              bRepair)
{

    SymantecVerdict result = SymantecVerdictOkay;

    // Do not miss the preamble part which usually is ignored by mail client.
    // Also in the case that an attachment itself is a RFC822 message, the header
    // in the attachment needs to be preserved.
    if (mimeEntity->bodyBuffer() && (mimeEntity->preambleOffset() || mimeEntity->preambleLen() )){
        // This dumps out the header in the attachment + preamble.
        mfwBody->Write(mimeEntity->bodyBuffer(), (mimeEntity->preambleOffset() + mimeEntity->preambleLen()));
        mfwBody->Write("\r\n");
    }

    // Dumps anything between the boundary of multi-part Mime
    // and the boundary of the first Mime part in multi-part Mime.
    // And this usually is only a header line like this:
    // Content-Type: multipart/alternative;
    // boundary="----=_NextPart_001_0048_01C3ECAE.5F4E2F90"

    // The root mime entity doesn't contain Mime header like above.
    // So We only write header for child mime entity i.e. mime entity which has parent.
    if (mimeEntity->parent() && mimeEntity->bodyBuffer() == NULL && mimeEntity->bodyOffset() == 0
             && mimeEntity->bodyLen() == 0 && mimeEntity->header() != NULL)
    {
        HeaderField *Field = mimeEntity->header();

        // Dump out all of headers in a MIME
        while (Field){

            mfwBody->Write(Field->buffer() + Field->nameOffset(), (Field->nameLen() + Field->valueLen()-1));
            mfwBody->Write("\r\n");
            Field = Field->next();
        }
    }

    if (mimeEntity->type() == ENTITY_TYPE_MULTIPART)
        mfwBody->Write("\r\n");

    SymString boundary  = mimeEntity->boundary();
    Entity *childEntity = mimeEntity->child();
    if (childEntity != NULL) {

        int count = 0;
        while (childEntity != NULL) {

            if (count > 0) {
                mfwBody->Write("\r\n");
            }
            SymString tmpBoundary = "--" + boundary + "\r\n";
            if (mimeEntity->boundary() && strcmp(mimeEntity->boundary(), "") != 0 ) {
                mfwBody->Write(tmpBoundary);
            }
            else
                mfwBody->Write("\r\n");
           
            SymantecVerdict tmp;
            tmp = parseBodyPart(msg, childEntity, mfwHeader, mfwBody, bRepair);

            // attach notice for repaired case only 
            if (tmp != SymantecVerdictOkay && bRepair && !msg->isRepairFailed()) {

                // Add a notify part: the attached file was repaired.
                // Get type of entity and accordingly attach notice to the mime message
                SymString disp = GetHeaderValue(childEntity, "Content-Disposition");
                SymString newPart;
                SymantecVerdict verdict = msg->getSymantecVerdict();
                SymString startBoundary;
                SymString endBoundary;

                if (Len(disp)) {
                    if (verdict == SymantecVerdictSuspectNoRepaired) {
                        newPart.assign(symantecApp->getSymDelAttachNotice());
                    } else if (verdict == SymantecVerdictSuspectRepaired){
                        startBoundary.append("\r\n--");
                        startBoundary.append(boundary.AsPtr());
                        startBoundary.append("\r\n");
                        newPart.assign(symantecApp->getSymRepairedAttachNotice());
                    }
                } else {

                    if (verdict == SymantecVerdictSuspectNoRepaired) {
                        
                        startBoundary.append("\r\n--");
                        startBoundary.append(boundary.AsPtr());
                        startBoundary.append("\r\n");
                        endBoundary.append("\r\n--");
                        endBoundary.append(boundary.AsPtr());
                        endBoundary.append("--\r\n");
                        
                        newPart.assign(symantecApp->getSymDelPlainTextNotice());
                    } else if (verdict == SymantecVerdictSuspectRepaired) {
                        startBoundary.append("\r\n--");
                        startBoundary.append(boundary.AsPtr());
                        startBoundary.append("\r\n");
                        newPart.assign(symantecApp->getSymRepairedPlainTextNotice());
                    }
                } 
                SymString strFrom(msg->getMailFrom()->str, msg->getMailFrom()->len);
                SymString str_NoAngleSender = strFrom.Segment(1, int_cast(strFrom.size()) - 2);

                newPart.S("<Virusname>",  msg->getCurrentVirusName(), "g");
                newPart.S("<Filename>",   msg->getCurrentInfecFileName(), "g");
                newPart.S("<Sender>",     str_NoAngleSender, "g");
                newPart.S("<Date>",       msg->getDateSubmitted(),   "g");
                newPart.S("<Postmaster>", symantecApp->getVirusNoticeFrom(), "g");
                newPart.S("\n",           "\r\n", "g");
                newPart += "\r\n";

                mfwBody->Write(startBoundary);
                mfwBody->Write(newPart);
                mfwBody->Write(endBoundary);
            } 
             
            if (tmp != SymantecVerdictOkay) result = tmp;
            
            childEntity = childEntity->next();
            ++count;
        }
        SymString tmpBoundary = "\r\n--" + boundary + "--\r\n";
        if (mimeEntity->boundary() && strcmp(mimeEntity->boundary(), "") != 0 )
            mfwBody->Write(tmpBoundary);
        else
            mfwBody->Write("\r\n\r\n");

    } else { // if (entity == NULL)

        // Once timedout or scan failed, unwind the stack
        if (msg->getSymantecVerdict() == SymantecVerdictTimeout ||
            msg->getSymantecVerdict() == SymantecVerdictFailed ||
            msg->getSymantecVerdict() == SymantecVerdictScanFailed)
            return msg->getSymantecVerdict();

        // If the header and buffer is NULL, then no text to scan,
        // return Okay verdict
        if (mimeEntity->header() == NULL && mimeEntity->bodyBuffer() == NULL)
            return SymantecVerdictOkay;

        SymString strEncoding = GetHeaderValue(mimeEntity, "Content-Transfer-Encoding");
        SymString charset     = GetHeaderValue(mimeEntity, "charset");
        SymString disp        = GetHeaderValue(mimeEntity, "Content-Disposition");
        SymString contentType = GetHeaderValue(mimeEntity, "Content-Type");
        SymString encoding    = strEncoding.Segment(0,6);

        // This part of filename extraction code is tested in unit test
        // "test/t_mxutils.cxx:runExtractFileName()".
        SymString strFileName = "";
        if (Len(disp))
        {
             MxMimeHeaderParser parser(disp, true) ;
             parser.GetString(); // Skip first token.

             // parameters
             while (parser.SkipToken(';')) {
                 SymString str_Name(parser.GetString());
                 str_Name = str_Name.UpperCase();
                 if (str_Name=="FILENAME") {
                     parser.Expect('=');
                     SymString str_Value;
                     while(1) {
                         str_Value=parser.GetString();
                         strFileName+=str_Value;
                         if (parser.AtEOF()) {
                             break;
                         }
                     }
                 }
             }
        }

        int headerOffset, mimeDatasize;
        const char* entityBuffer;
        if (mimeEntity->bodyLen() != 0 && mimeEntity->bodyBuffer() != NULL) {

            entityBuffer = mimeEntity->bodyBuffer();

            headerOffset = (mimeEntity->header() != NULL) ?
                            mimeEntity->header()->nameOffset() : mimeEntity->bodyOffset();
            mimeDatasize = mimeEntity->bodyOffset()  - headerOffset  + mimeEntity->bodyLen();

        } else if (mimeEntity->header() == NULL) {

            // bodyLength is 0,  bodyBuffer is NULL and header is also NULL.
            // no text to scan. Return Okay verdict.
            return SymantecVerdictOkay;
        } else { // bodyLen is 0

            // There is no body to scan, so pass the header portion of the entity.
            // Calculate the header length by using first and last field's data.
            HeaderField* hdrField = mimeEntity->header();

            entityBuffer = hdrField->buffer();
            headerOffset = hdrField->nameOffset(); // the offset of first field.

            // Get the last field.
            while (hdrField->next() != NULL)
                hdrField = hdrField->next();

            // The length of the text to scan is the last
            // field's nameOffset +nameLen + valueLen - first header nameOffset
            mimeDatasize =  hdrField->nameOffset() + hdrField->nameLen() +
                            hdrField->valueLen() - headerOffset;

            DIAGC("symantecScanner", 1,
                  "BodyLen is 0, passing header with offset=%d and length=%d",
                   headerOffset, mimeDatasize);
        }

        DIAGC("symantecScanner", 7,
               "encoding = %s, charset =%s, body size=%d, header offset=%d",
               encoding.AsPtr(), charset.AsPtr(), mimeDatasize,
               headerOffset);
       
        SymString str_NewBody;
        SymString filename;
        string virus;
       
        // Since the symantec engine doesn't seem to repair a base64 encoded
        // MIME part, we need to decode the attachment before scanning.
        // If the message is not a baase64 encoded or should be cleaned rather
        // than repaird, just scan the entire MIME part.
        // Just in case, the config key to disable the decode/encode process
        // disableBase64decode, which is undocumented key.
        if (encoding != "base64" || symantecApp->isDisbBase64Decode() || !bRepair) {

            StringInfo* strBody = new StringInfo();
            if (strBody != NULL) {
                strBody->str = entityBuffer + headerOffset;
                strBody->len = mimeDatasize;
            }

            StringInfo* strHeader = new StringInfo();
            strHeader->str = "";
            strHeader->len = 0;

            result = _doScanning(strHeader, strBody, msg, virus, bRepair);

            if (result == SymantecVerdictOkay && symantecApp->isRescanUUEncode()) {

                result = scanUUencodePart(msg, strBody->str, strBody->len, virus,
                                          filename, bRepair, str_NewBody);
            }

            // set verdict with message
            msg->setSymantecVerdict(result);

            if (bRepair) {

                if (result == SymantecVerdictSuspectRepaired) {

                    if (!str_NewBody.size()) {

                        DIAGC("symantecScanner", 5, "Copy repaired part");

                        // assigned repaired part of message
                        str_NewBody = SymString(msg->getRepairedBody()->str, msg->getRepairedBody()->len);
                    }
                }
            }

            // release memory
            delete strBody;
            delete strHeader;

        } else {  // base 64 encoding handling - start
            DIAGC("symantecScanner", 5, "Base 64 scanning");
            //----------------------------------------
            //   DECODE and ENCODE if we are Repairing
            //----------------------------------------
            
            // scan the mime header and mime body inside a boundary enclosure
            // separately so as not to miss any virus.
            SymantecVerdict hdrResult, bdyResult;
            hdrResult = bdyResult = SymantecVerdictUnknown;
            
            // -- scan the MIME header part --
            SymString str_NewHeader;

            StringInfo* strBody = new StringInfo();
            strBody->str = "";
            strBody->len = 0;

            StringInfo* strHeader = new StringInfo();
            if (strHeader != NULL) {
                strHeader->str = entityBuffer + headerOffset;
                strHeader->len = mimeEntity->bodyOffset() - headerOffset;
            }

            hdrResult = _doScanning(strHeader, strBody, msg, virus, bRepair);

            // set header scan result
            msg->setSymantecVerdict(hdrResult);

            // check if header part repaired
            if (bRepair && hdrResult == SymantecVerdictSuspectRepaired) {

                str_NewHeader = SymString(msg->getRepairedBody()->str, msg->getRepairedBody()->len);;
            }

            delete strBody;
            delete strHeader;

            SymString encoded;
            SymString decRepairedBdy;
            SymString encRepairedBdy;

            SymString newBody; // used if string repaired

            // check if the bodyBuffer is null and body length is null skip scanning body
            if (mimeEntity->bodyLen() != 0 && mimeEntity->bodyBuffer() != NULL) {

                // -- scan the base64 encoded body part --
                // first decode the base64ed file
                encoded.append((mimeEntity->bodyBuffer() + mimeEntity->bodyOffset()),
                              mimeEntity->bodyLen());

                SymString decoded = MxDecodeBase64(encoded);
                char* strDecoded = new char[Len(decoded)];
                if (strDecoded != NULL) {
                    memcpy(strDecoded, decoded.AsPtr(), (Len(decoded)));
                }

                // Pass body as decoded file to SymantecEngine
                StringInfo* strBody = new StringInfo();
                if (strBody != NULL) {
                    strBody->str = strDecoded;
                    strBody->len = Len(decoded);
                }
                StringInfo* strHeader = new StringInfo();
                strHeader->str = "";
                strHeader->len = 0;
                
                bdyResult = _doScanning(strHeader, strBody, msg, virus, bRepair);

                // set verdict with message
                msg->setSymantecVerdict(bdyResult);

                if (bRepair && bdyResult == SymantecVerdictSuspectRepaired) {
                    // save repaired message part
                    newBody = SymString(msg->getRepairedBody()->str, msg->getRepairedBody()->len);
                }

                // release memory
                delete strBody;
                delete strHeader;

                // found message part - clean
                if (hdrResult == SymantecVerdictOkay  && 
                    bdyResult == SymantecVerdictOkay && symantecApp->isRescanUUEncode()) {

                    bdyResult = scanUUencodePart(msg, strDecoded, Len(decoded), 
                                                 virus, filename, bRepair, newBody);
                }

                // rescannedSuspectedAttachment with encoded string
                // if the part doesn't seem to be infected
                if (symantecApp->isRescanSuspecAttach() && 
                    hdrResult == SymantecVerdictOkay &&
                    bdyResult == SymantecVerdictOkay) {

                    // 
                    StringInfo* strEncHeader = new StringInfo();
                    strEncHeader->str = "";
                    strEncHeader->len = 0;

                    StringInfo* strEncoded = new StringInfo();
                    if (strEncoded != NULL) {
                        char* tmp = new char[encoded.size()];
                        if (tmp != NULL) {
                            memcpy(tmp, encoded.AsPtr(), encoded.size());
                            strEncoded->str = tmp;
                            strEncoded->len = encoded.size();
                        }
                    }
 
                    bdyResult = _doScanning(strEncHeader, strEncoded, msg, virus, bRepair); 
                   
                    // set verdict with message
                    msg->setSymantecVerdict(bdyResult);
 
                    //take action on results
                    if (bRepair && bdyResult == SymantecVerdictSuspectRepaired) {

                        SymString tmpStr;
                        tmpStr = SymString(msg->getRepairedBody()->str, msg->getRepairedBody()->len);
                        newBody = MxEncodeBase64(tmpStr);    
                    }

                    // release memory
                    if (strEncoded->str) delete []strEncoded->str;
                    delete strEncoded;
                    delete strEncHeader;
                }

                // set symantec scan verdict
                msg->setSymantecVerdict(bdyResult);

                if (strDecoded) delete [] strDecoded;
            }

            // prepare repaired mime part  
            if (bRepair) {
                if (bdyResult == SymantecVerdictSuspectRepaired ||
                   hdrResult == SymantecVerdictSuspectRepaired ) {

                    DIAGC("symantecScanner", 5, "Base64 repaired part copy"); 
                    result = SymantecVerdictSuspectRepaired;
                    SymString tmphdr;
                
                    if (hdrResult == SymantecVerdictSuspectRepaired) {

                       // if header is repaired
                       tmphdr = str_NewHeader;
                    } else {
                       // header cleaned, used as it is from message and write back in a message
                       if (mimeEntity->bodyBuffer() != NULL)
                          tmphdr =  SymString((mimeEntity->bodyBuffer() + headerOffset),
                                       (mimeEntity->bodyOffset() - headerOffset));

                    }

                    str_NewBody += SymString(tmphdr.AsPtr());

                    if (newBody.size()) {
                        // body repaired
                        str_NewBody += SymString(newBody.AsPtr());
                    } else {
                        // body cleaned
                        str_NewBody += SymString(encoded.AsPtr());
                    }
                
                } else {

                    result = bdyResult;
                }
            } else {
                result = bdyResult;     
            }

            // set verdict
            msg->setSymantecVerdict(bdyResult);
                  
        } // End - base64 handling

        // Always get filename from MIME Body first.
        // If filename is missing, try using the name returned from Symantec Engine.
        filename = Len(strFileName) ? strFileName : ( Len(filename) ? filename : "Your Message");

        DIAGC("symantecScanner", 7, "filename = %s, result=%d", filename.AsPtr(), result);

        // Set the timeout verdict.
        if (result == SymantecVerdictTimeout || result == SymantecVerdictFailed
            || result == SymantecVerdictScanFailed) {

            DIAGC("symantecScanner", 5, "ScanFailed; VerdictResult=%d", result);
            msg->setSymantecVerdict(result);
            return result;
        }

        if (bRepair) {

            if (result == SymantecVerdictOkay) {
                mfwBody->Write(entityBuffer + headerOffset, (mimeDatasize));
                return result;
            }
        } else {
       
            if (result != SymantecVerdictSuspectRepaired && result != SymantecVerdictSuspectNoRepaired) {
            
                mfwBody->Write(entityBuffer + headerOffset, (mimeDatasize));
                return result;
            }
        }

        SymString strFrom(msg->getMailFrom()->str, msg->getMailFrom()->len);
        SymString str_NoAngleSender = strFrom.Segment(1, int_cast(strFrom.size()) - 2);

        SymString newPart, actionTaken;
        SymString origNotice;
        newPart.Clear();
        origNotice.Clear(); 

        // If the MIME part is repaired then add repaired MIME part.
        // Also add a repairedAttachmentNotice.
        // If suspect part failed to repaired then attached deletd notice and 
        // suspect part must removed from the message
        if (bRepair) {
         
            if (result == SymantecVerdictSuspectRepaired && str_NewBody.size()) {

                DIAGC("symantecScanner", 5, "Adding repaired mime part !");
                mfwBody->Write(str_NewBody.AsPtr(), str_NewBody.size() );

                actionTaken = "Repaired";
            } else if (result == SymantecVerdictSuspectNoRepaired) {

                actionTaken = "Cleaning"; 
                // A plain text must be a single part with no parent and not pointing to
                // next Mime. Also the bodyOffset must be zero.
                if (mimeEntity->type() == ENTITY_TYPE_SIMPLE && !mimeEntity->parent() &&
                    !mimeEntity->next() && !mimeEntity->bodyOffset()) {
                    if (!Len(disp)) {

                       newPart = symantecApp->getSymDelPlainTextNotice();

                       // set details in attachment notice in case suspect found
                       newPart.S("<Virusname>",  msg->getCurrentVirusName(), "g");
                       newPart.S("<Filename>",   msg->getCurrentInfecFileName(), "g");
                       newPart.S("<Sender>",     str_NoAngleSender, "g");
                       newPart.S("<Date>",       msg->getDateSubmitted(),   "g");
                       newPart.S("<Postmaster>", symantecApp->getVirusNoticeFrom(), "g");
                       newPart.S("\n",           "\r\n", "g");
                       newPart += "\r\n";
                       mfwBody->Write(newPart);
                    }
                }
            }
            msg->setRepairFailed(false);
            msg->setRepaired(true);
            msg->setSymantecVerdict(result);
 
        } else {
            SymString startBoundary;
            SymString endBoundary;
            
            SymString bdry;
            Entity * parent = mimeEntity->parent();
            if (parent) {
                bdry.assign(parent->boundary());
            } else {
                bdry.assign(mimeEntity->boundary());
            }

            if (bdry.size()) { 
                // create boundry
                startBoundary.append("\r\n--");
                startBoundary.append(bdry.AsPtr());
                startBoundary.append("\r\n");
                endBoundary.append("\r\n--");
                endBoundary.append(bdry.AsPtr());
                endBoundary.append("--\r\n");
            }

            // Clean case - suspect part removed by notice 
            // A plain text must be a single part with no parent and not pointing to
            // next Mime. Also the bodyOffset must be zero.
            if (mimeEntity->type() == ENTITY_TYPE_SIMPLE && !mimeEntity->parent() &&
               !mimeEntity->next() && !mimeEntity->bodyOffset()) {

                // attach notice as per type of mime part - plain text or attachment
                if (Len(disp)) {
                    startBoundary.clear();
                    endBoundary.clear();
                    origNotice = symantecApp->getSymDelAttachNotice();
                } else {

                    origNotice = symantecApp->getSymDelPlainTextNotice();
                }

                // Rewrite the headers in case they are mentioned in
                // deletedPlainTextNotice.
                _rewriteHeaders(newPart, origNotice, msg, mfwHeader);

            } else {

                if (Len(disp)) {
                    startBoundary.clear();
                    endBoundary.clear();
                    // attachment delete notice
                    newPart = symantecApp->getSymDelAttachNotice();
                } else {

                    // delete plain text notice
                    newPart = symantecApp->getSymDelPlainTextNotice();
                }
            }
            
            // action is "clean"
            actionTaken = "Cleaning";
            msg->setRepairFailed(true);

            // set details in attachment notice in case suspect found
            newPart.S("<Virusname>",  msg->getCurrentVirusName(), "g");
            newPart.S("<Filename>",   msg->getCurrentInfecFileName(), "g");
            newPart.S("<Sender>",     str_NoAngleSender, "g");
            newPart.S("<Date>",       msg->getDateSubmitted(),   "g");
            newPart.S("<Postmaster>", symantecApp->getVirusNoticeFrom(), "g");
            newPart.S("\n",           "\r\n", "g");
            newPart += "\r\n";

            if (bdry.size() && !Len(disp)) {
                mfwBody->Write(startBoundary);
            }
            mfwBody->Write(newPart);
            if (bdry.size() && !Len(disp)) {
                mfwBody->Write(endBoundary);
            }
        }      

        if (symantecApp->isMsgcatLoggingEnabled())
            REPORT_SymnAVVirusDetected(SLOGHANDLE, Notification, actionTaken,
                             getVirusContext(virus, filename, msg));
        else
            LOG(Notification, "AVVirusDetected", "%s %s",
                             actionTaken.AsPtr(),
                             getVirusContext(virus, filename, msg).AsPtr());

    }

    return result;
}

//+---------------------------------------------------------------------------
//
// Method:    SymantecScanner::_rewriteHeaders
//
// Synopsis:  Rewrite original headers with the ones that are present in
// deletedPlainTextNotice.
//
//----------------------------------------------------------------------------

void SymantecScanner::_rewriteHeaders(SymString& newPart,
                                    SymString& origNotice,
                                    MsgInfo* msg,
                                    SMappedFileWrite* mfwHeader)
{
    // Extract MIME header part from 'deletedPlainTextNotice'
    int p = origNotice.Index("\n\n");

    // Does 'deletedPlainTextNotice' have a MIME header?

    if (p <= 0) {
        newPart = origNotice;
        return;
    }

    // Split the MIME header part of deletedPlainTextNotice
    // and rewrite 'newPart'
    // The deletedPlainTextNotice is assumed to be a single part
    // and each of the header fields is a single line.
    SymStringArray hdrBody = origNotice.Split("\n\n");
    SymString headerString = hdrBody[0];
    SymStringArray mimeHeader = headerString.Split("\n");
    int numMimeHeader = mimeHeader.size();

    // In most cases, "Content-Disposition" field is accompained
    // with Content-Type when the message is a multipart mixed.
    // But the deletedPlainTextNotice may not have a "content-disposition"
    // since the notice message would be a single part.  So, make sure to
    // remove the Content-Disposition from the original header if there isn't
    // "Content-Disposition" in the deletedPlainTextNotice.

    // What headers do we have in the notice?

    int noticeDisposition = -1;
    int noticeContentType = -1;
    int noticeTransferEncoding = -1;
    for (int i = 0; i < numMimeHeader; i++) {
         SymString mime = mimeHeader[i];

        if (!strncasecmp("Content-Type:", mime.AsPtr(), strlen("Content-Type:"))) {
            noticeContentType = i;
        }
        if (!strncasecmp("Content-Disposition:", mime.AsPtr(),
                         strlen("Content-Disposition:"))) {
            noticeDisposition = i;
        }
        if (!strncasecmp("Content-Transfer-Encoding:", mime.AsPtr(),
                         strlen("Content-Transfer-Encoding:"))) {
            noticeTransferEncoding = i;
        }
    }
    // +2 is skip \n\n that we had indexed earlier.
    newPart = origNotice.Segment(p + 2, origNotice.size());

    SymString headerfile = msg->getSMappedHeader()->getFilename();
    SymString cleanHeaderFile(headerfile + ".clean");
    mfwHeader->CreateCached(cleanHeaderFile.AsPtr());

    SymString header(msg->getHeader()->str, msg->getHeader()->len);
    SymStringArray lines = header.Split("\r\n");

    bool origContentType = false;
    bool origTransferEncoding = false;

    for (int j = 0; j < int_cast(lines.size()); j++) {
        SymString line = lines[j];
        bool found = false;

        // Skip new line or characted starting with space
        // TODO: Note we are not assembling multi line headers to single line
        // and then comparing the headers. We are essentially skipping those.
        if (isspace(*(line.AsPtr()))) {
            mfwHeader->Write(line);
            mfwHeader->Write("\r\n");
            continue;
        }

        // If there is no Content-disposition present in deletedPlainTextNotice
        // then we skip the  disposition header from original header.
        if (noticeDisposition == -1 &&
            !strncasecmp("Content-Disposition:", line.AsPtr(),
                         strlen("Content-Disposition:"))) {
            continue;
        }

        // Rewrite the headers that are present in Notice.
        // Note: This might look a performance lag, since we iterate
        // for every header in original header. But keeping in mind that
        // Notice would header be very small we will see a O(n) complexity most
        // of the times.
        for (int i = 0; i < numMimeHeader; i++) {
            SymString mime = mimeHeader[i];
            if (!strncasecmp(line.AsPtr(), mime.AsPtr(), mime.Index(":"))) {
                found = true;
                mfwHeader->Write(mime);
                mfwHeader->Write("\r\n");
                break;
             }
        }

        // In original header, if we don't see content-type or encoding then we
        // need to append the headers if present in Notice.
        if (origContentType == false &&
            !strncasecmp("Content-Type:", line.AsPtr(), strlen("Content-Type:"))) {

            origContentType = true;
        }

        if (origTransferEncoding == false &&
            !strncasecmp("Content-Transfer-Encoding:", line.AsPtr(),
            strlen("Content-Transfer-Encoding:"))) {

            origTransferEncoding = true;
        }

        if (!found) {
            mfwHeader->Write(line);
            mfwHeader->Write("\r\n");
        }
    } //end of original header for loop

    msg->headerRewritten(); 

    // Append the headers that are not present in original, but
    // present in Notice headers.
    if (!origContentType && noticeContentType != -1) {
        mfwHeader->Write(mimeHeader[noticeContentType]);
        mfwHeader->Write("\r\n");
    }

    if (!origTransferEncoding && noticeTransferEncoding != -1) {
        mfwHeader->Write(mimeHeader[noticeTransferEncoding]);
        mfwHeader->Write("\r\n");
    }

    if (symantecApp->isMsgcatLoggingEnabled()) {
        REPORT_SymnAVMessageHeadersRewritten(SLOGHANDLE, Notification, msg->getMessageID().c_str());
    }
    else {
        LOG(Notification, "AVMessageHeaderRewritten", "msgid=%s", msg->getMessageID().c_str());
    }

}

//+---------------------------------------------------------------------------
//
// Method:    SymantecScanner::cleanMessage
//
// Synopsis:  Break the message into seperate SMime part, scan and clean
//            the mail.
//
//----------------------------------------------------------------------------
SymantecVerdict SymantecScanner::cleanMessage(MsgInfo* msg,
                                          SMappedFileWrite* mfwHeader,
                                          SMappedFileWrite* mfwBody,
                                          SMappedFileRead* mfrHeader,
                                          SMappedFileRead* mfrBody,
                                          bool bRepair)
{
    DIAGC("symantecScanner", 5, "Entering cleanMessage");
    if (!_initialized) {
        DIAGC("symantecScanner", 5, "Exit VerdictInitFailed cleanMessage");
        msg->setSymantecVerdict(SymantecVerdictFailed);
        return msg->getSymantecVerdict();
    }

    if (msg->getSMappedBody() == NULL) {
        DIAGC("symantecScanner", 5, "Exit VerdictUnknown cleanMessage");
        return SymantecVerdictUnknown;
    }

    MxMessage* theMxMsg = NULL;
    theMxMsg = createMxMessage(msg);
    if (!theMxMsg) {
        msg->setSymantecVerdict(SymantecVerdictFailed);
        DIAGC("symantecScanner", 5, "Exit UnableToCreateMxMsg cleanMessage");
        return msg->getSymantecVerdict();
    }

    Parser* parser;
    Entity *mimeEntity;
    SymantecVerdict verdict = SymantecVerdictFailed;

    try {

        parser  = theMxMsg->getParser();
        mimeEntity = parser->getParsedMessage();

        if (!mimeEntity) {

            msg->setSymantecVerdict(SymantecVerdictBadMsg);
            delete theMxMsg;
            theMxMsg = NULL;
            DIAGC("symantecScanner", 5, "Exit BadMsg cleanMessage");
            return SymantecVerdictBadMsg;
        }
     
        SymString bodyfile = msg->getSMappedBody()->getFilename();
        bodyfile += ".clean";

        mfwBody->CreateCached(bodyfile);

        verdict = parseBodyPart(msg, mimeEntity,
                                          mfwHeader, mfwBody, bRepair);

    } catch(exception& ex) {

        LOG(Notification, "Exception cleanMessage", "%s", ex.what());
    } catch(...) {

        LOG(Notification, "Generic exception");
    }

    if (verdict == SymantecVerdictScanFailed || 
        verdict == SymantecVerdictFailed) {

        // Release memory
        parser->deleteParsedMessage();
        delete theMxMsg;

        DIAGC("symantecScanner", 5, "Exit VerdictFailed cleanMessage");
        return verdict;
    }

    if (msg->isMsgPartSuspect()) {

        if (!msg->isRepaired()) {
            if (symantecApp->isMsgcatLoggingEnabled()) {
                REPORT_SymnAVMessageCleaned(SLOGHANDLE, Notification, msg->getMessageID().c_str());
            }
            else
                LOG(Notification, "AVMessageCleaned", "msgid=%s", msg->getMessageID().c_str());
        } else {
            if (symantecApp->isMsgcatLoggingEnabled()) {
                REPORT_SymnAVMessageRepaired(SLOGHANDLE, Notification, msg->getMessageID().c_str());
            }else
               LOG(Notification, "AVMessageRepaired", "msgid=%s", msg->getMessageID().c_str());
        }

        StringInfo* newheader = new StringInfo;
        if (newheader != NULL) {
            newheader->str = NULL;
            newheader->len = 0;

            if (mfwHeader->IsCached()) {

                newheader->str = mfwHeader->GetCachePtr();
                newheader->len = mfwHeader->GetCacheLen();    
            } else {

                mfwHeader->Close(true);
                mfrHeader = new SMappedFileRead(mfwHeader->getFilename());
                newheader->str = mfrHeader->getBuffer();
                newheader->len = mfrHeader->getSize();
            }
            msg->setNewHeader(newheader);
        }

        StringInfo* newbody = new StringInfo;
        if (newbody != NULL) {
            newbody->str = NULL;
            newbody->len = 0;

            if (mfwBody->IsCached()) {
                newbody->str = mfwBody->GetCachePtr();
                newbody->len = mfwBody->GetCacheLen();
            } else {

                mfwBody->Close(true);
                mfrBody = new SMappedFileRead(mfwBody->getFilename());
                newbody->str = mfrBody->getBuffer();
                newbody->len = mfrBody->getSize();
            }
            msg->setNewBody(newbody);
        }

        // set final repair/clean process verdict
        if (bRepair) {
            verdict = SymantecVerdictSuspectRepaired;
        } else {
            verdict = SymantecVerdictSuspectNoRepaired;
        }

    } else if (symantecApp->isScanAndClean() &&
               symantecApp->isRemoveAllMsgParts() &&
               !msg->isMsgPartSuspect()) {

        // If 'scanMessage' found suspecious message part but 'cleanMessage' 
        // doesn't find any part suspecious then remove all parts in the message
        // and put 'removedAllMessagePartsNotice' in.

        SymString newPart = symantecApp->getSymRemAllMsgPartsNotice();
        SymString strFrom(msg->getMailFrom()->str, msg->getMailFrom()->len);
        SymString str_noAngleSender = strFrom.Segment(1, int_cast(strFrom.size())-2);
        
        newPart.S("<Virusname>",  msg->getVirusName(), "g");
        newPart.S("<Filename>",   msg->getFileName(), "g");
        newPart.S("<Sender>",     str_noAngleSender, "g");
        newPart.S("<Date>",       msg->getDateSubmitted(), "g");
        newPart.S("<Postmaster>", symantecApp->getVirusNoticeFrom(), "g");
        newPart.S("\n",           "\r\n", "g");
        newPart += "\r\n";

        mfwBody->ReplaceCache(newPart);

        if (symantecApp->isMsgcatLoggingEnabled())   
            REPORT_SymnAVMessageBodyRewritten(SLOGHANDLE, Notification, msg->getMessageID().c_str());
        else
            LOG(Notification, "AVMessageBodyRewritten", "msgid=%s", msg->getMessageID().c_str());

        verdict = SymantecVerdictSuspectRepaired;
        msg->setSymantecVerdict(verdict); 
    }

    // Release memory 
    parser->deleteParsedMessage();
    delete theMxMsg;
 
    DIAGC("symantecScanner", 5, "Exiting cleanMessage");

    return verdict; 
}

