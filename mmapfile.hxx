//----------------------------------------------------------------------------
//
// File Name:    mmapfile.hxx
//
// Synopsis:    This file contains MappedFileRead and MappedFileWrite class
//              declaration. This is copied from OMU and de-coupled from 
//              OMU (cleaned up OMU related code).
//----------------------------------------------------------------------------
// Copyright 2017 Synchronoss, Inc. All Rights Reserved.
//
// The copyright to the computer software herein is the property of 
// Synchronoss, Inc. The software may be used and/or copied only with
// The written permission of Synchronoss, Inc. or in accordance with the
// terms and conditions stipulated in the agreement/contract under which
// the software has been supplied.
//----------------------------------------------------------------------------

#ifndef mmapfile_hxx
#define mmapfile_hxx
#ifdef mmapfile_cxx
char mmapfile_hxx_RCSID[]="$Id";
#endif

#include <string.h>

#include <xmimecontent.hxx>

typedef unsigned long   u_long;
typedef unsigned char   byte;


namespace symantecscan {

#ifdef BYTESNKMAGIC
#undef BYTESNKMAGIC
#endif
#define BYTESNKMAGIC (('B'<<24)|('S'<<16)|('N'<<8)|'K')


class FileSnk
{
  public:
    FileSnk();
    FileSnk(int fd);
    ~FileSnk();

    virtual long    nextOut(); // return offset of next byte we'll write
    virtual void    nextOut(long);	        // change where we write next

    // Open an existing file in write-only mode.
    virtual bool open(const char* pathname);
    // Open an exsiting file, or create a new file, in write-only or
    // read-write mode.  The oflags argument should be a FileOpenMode
    // (optionally OR'ed with a FileCreateMode) from fcntl.hxx.  A file
    // will only be created if a FileCreateMode is used.
    // Permissions for newly created files will be 0600.
    // If necessary, oflags will be forced to use OPEN_WRITEONLY.
    virtual bool open(const char* pathname, int oflag);
    // Open or create a file with specified oflags and permissions.
    // perms should be a FilePermission from fcntl.hxx.
    virtual bool open(const char* pathname, int oflag, mode_t perms);
    virtual long    setDesc(int fd, const char* path = NULL);
    virtual int     close();
    int     Close(bool);
    bool sync();			// sync file to underlying disk
    int     fstat(struct stat*);
    bool truncate(off_t);
    void    useMappedIO();
    void    useSyncedIO();
    void    alignOutput(off_t = 512);

    // write a raw block of data
    void swrite(const void* cP, size_t count);
 

    inline bool opened() { return ofd >= 0; }
    inline int getErrno() { return sysErrno; }
    inline const char * getPathname() { return pathname; }
    inline int getDesc() { return ofd; }
    inline void giveUpDesc() { flush(); ofd = -1; }
    inline void flush() { _flush(); }
    inline bool lastFlushFailed() { return _flushFailed; }

  protected:
    // ptr to first unsaved byte in output buf
    byte* odirty;
    // segment size for segment-aligned output
    off_t oalign;
    // where to write current output buffer to
    // (if the file is mapped, this points to just BEFORE the mapped window)
    long offset;
    // the pathname, if provided
    char *pathname;
    // file descriptor
    int ofd;
    // the system errno (if any) on open or read
    int sysErrno;
    // size of file (including any extension)
    long fileSize;
    // whether or not this file should be mapped
    bool mappable;
    // whether or not this file should be msync'd or fsync'd
    bool syncable;
    // a flag to indicate if a flush has already thrown an exception
    bool _flushFailed;

    void unmap();
    void retruncate();
    long logicalSize();
    void checkFileSize();

    virtual void _flush(bool finished = true);	// Empty a buffer.
    virtual bool _flushBuf(byte *, int);
    void commonInit();

protected:
    long estimateMaxFileSize;
    const char *_szKey;

    // ptr to next free byte in output buf
    byte* onext;
    // ptr to byte following output buf
    byte* olimit;
    // ptr to beginning of output buf
    byte* obase;
    // The highwater mark in the buffer (continuous region in the buffer)
    // Allows us to seek within the buffer without the need to flush right away
    byte* ohighwater;
    // magic number (must equal BYTESNKMAGIC)
    int snk_magic;




};



class MappedFileRead
{
  public:
    MappedFileRead();
    MappedFileRead(const char *);
    MappedFileRead(int);
    ~MappedFileRead();

    inline u_long getSize() { return _n_Length; }
    inline const char *getBuffer() { return _p_Buffer; }
    inline bool IsOK() { return _f_OK; }
    inline const char* getFilename() {
	return _p_Filename ? _p_Filename : "file"; }
    inline int GetFd() { return _fd_File; }
    bool Open(const char *file);
    void Close();

  private:
    int _fd_File;
    u_long _n_Length;
    char *_p_Buffer;
    char* _p_Filename;
    bool _f_OK;
    bool _f_NeedsClosing;

    void _Map();
};

#define MMAPPED_FILE_WRITE_MAGIC (('S'<<24)|('M'<<16)|('F'<<8)|'W')

enum MapFileCreateOptions
{
    MapFileCreateNormal,
    MapFileCreateSynced
};

class MappedFileWrite
{
  public:
    MappedFileWrite();
    ~MappedFileWrite();

    void Write(const char *, u_long);
    inline void Write(const char* str) { Write(str, strlen(str)); }
    inline void Write(const SymString &str) { Write(str, Len(str)); }
    virtual bool Create(const char *str,
		       MapFileCreateOptions options = MapFileCreateNormal);
    void CreateCached(const char *str);
    void FlushCache();
    void Sync();
    bool Flush(bool f_Sync = false);
    bool Close(bool f_Sync = false);
    inline const char* getFilename() { return _str_Filename; }
    inline bool IsOpen() { return _snk.opened(); }
    inline bool IsCached() { return _f_InMem; }
    inline const char *GetCachePtr() { return _str_Mem.AsPtr(); }
    inline int GetCacheLen() {  return Len(_str_Mem); }
    inline int GetFd() { return _snk.getDesc(); }
    // Close File without writing data to disk
    inline void Reset() { _snk.Close(true); }
    int GetFileSize();
    inline void ReplaceCache(const SymString &s) { _str_Mem = s; }
    inline void DeleteCache() { _f_InMem = false; _str_Mem.clear(); }

  private:
    FileSnk _snk;
    u_long _n_MemLimit;
    SymString _str_Filename;
    SymString _str_Mem;
    bool _f_InMem;
    int _n_Magic;
};

#ifdef SMappedFileRead 
#undef SMappedFileRead 
#endif 
#define SMappedFileRead  MappedFileRead


#ifdef SMappedFileWrite 
#undef SMappedFileWrite 
#endif 
#define SMappedFileWrite  MappedFileWrite

class MxMessage
{
  private:
    Parser*         _parser;
    SMappedFileRead* _header;
    SMappedFileRead* _body;

  public:
    MxMessage(SMappedFileWrite* hdr, SMappedFileWrite* body);
    MxMessage(const char* hdrFile,  const char* bodyFile);
    ~MxMessage();

    Parser* getParser() { return _parser; }
};



}
#endif
