//----------------------------------------------------------------------------
//
// File Name:    symantechandler.hxx
//
// Synopsis:     Handler class implementation
//----------------------------------------------------------------------------
// Copyright 2017 Synchronoss, Inc. All Rights Reserved.
//
// The copyright to the computer software herein is the property of
// Synchronoss, Inc. The software may be used and/or copied only with
// The written permission of Synchronoss, Inc. or in accordance with the
// terms and conditions stipulated in the agreement/contract under which
// the software has been supplied.
//----------------------------------------------------------------------------

#ifndef __SYMANTECHANDLER_HXX__
#define __SYMANTECHANDLER_HXX__

#include <iostream>
#include <vector>
#include <map>

#include "syutils.hxx"
#include "symconstants.hxx"
#include "symantecapp.hxx"
#include "msginfo.hxx"

using namespace std;

class Handler
{
  public:
    // ctor
    Handler(IMxMailDelivery* MailDelivery);
    // destructor
    ~Handler();

    // Core functions
    SymantecCode Initialize(SymantecHookType hookType = SymantecInbound);
    SymantecCode PreProcess();
    SymantecCode Process();
    SymantecCode Commit();

    // Process on mail as per action defined
    void deferMessage();
    void takeBadMsgAction();
    void commitNoChange();
    void commitTimeout();
    void commitResponse();
    void commitXHeader();
    void takeScanFailedAction();
    bool generateNotice(const char* header, const char* body, bool sender=true);

    // Getters for data members
    inline MsgInfo* getMsgInfo() { return _msgInfo; }
    inline string getDispStrInfo(SymantecAction symAction);

    SymantecCode setSymantecActionPre(SymantecAction symAction);
 
  public:
    typedef enum {
        ActionDefault = 0,
        ActionNewHeader = 1,
        ActionNewBody = 2,
        ActionNewXScannedHdr = 3,
        ActionNewHeaderAndBody = 4
    } ActionKey;

    typedef struct tagRecipInfo {
        StringInfo* rcptTo;
        StringInfo* rcptToDisp;
    } RecipInfo;

    typedef vector<RecipInfo*>  RecipInfoArray;
    typedef vector<StringInfo*> RecipCosArray;

    typedef struct tagMailInfo {
        StringInfo* header;
        StringInfo* body;
        RecipInfoArray recipInfoArray;
    } MailInfo;

    typedef map<ActionKey, MailInfo> MailInfoArray;

  private:
    void _postProcess();
    void _addVirusNoticeRecipInfo(RecipInfo* recipInfo, 
                                  Recipient* recip, SymantecAction symAction);
    void _addMailInfo(SymantecAction symAction, Recipient* recip=NULL);
    void _cleanMainInfoArray();

    RecipInfo* _createRecipInfo(StringInfo* disp, Recipient* recip);

  private:
    MailInfoArray    _mailActionArray;
    RecipInfoArray   _virusNoticeRecipArray;

    // mail attributes
    int  _recipCount;
    int  _virusNoticeCount;

    MsgInfo*          _msgInfo;
    IMxMailDelivery*  _mailDelivery;
    SMappedFileWrite* _wHeader;
    SMappedFileWrite* _wBody;
    SMappedFileRead*  _rHeader;
    SMappedFileRead*  _rBody; 

    bool _cleanMail;
    bool _RepairMsg; 
    bool _CleanVirus;
    bool _ScanVirus;

    string           _strVNoticeRecipients;
    string           _strRcptDisp;
    SymantecHookType _hookType;
};

#endif
