//----------------------------------------------------------------------------
//
// File Name:    symconstants.hxx
//
// Synopsis:     The file contains data type declarations and typedefs
//               used in symantec extension service.
//----------------------------------------------------------------------------
// Copyright 2017 Synchronoss, Inc. All Rights Reserved.
//
// The copyright to the computer software herein is the property of
// Synchronoss, Inc. The software may be used and/or copied only with
// The written permission of Synchronoss, Inc. or in accordance with the
// terms and conditions stipulated in the agreement/contract under which
// the software has been supplied.
//----------------------------------------------------------------------------

#include "Symantec.hxx"

#ifndef __SYM_CONSTANTS_HXX__
#define __SYM_CONSTANTS_HXX__

#include "extapi.hxx"

// typedefs for user defined data type 
typedef unsigned char uchar;

// module name 
const char* const EXTN_SERVICE = "symnAVScan";
const char* const MODULE_NAME  = "symnAVExtSvc";

// general config key name
const char* const KEY_SYMACTION                = "symnAVAction";
const char* const KEY_SYMSERVERCONN            = "symnAVServerConnection";
const char* const KEY_SYMVRSNOTICEHEADERSENDER = "symnAVVirusNoticeHeaderSender";
const char* const KEY_SYMVRSNOTICEBODYSENDER   = "symnAVVirusNoticeBodySender";
const char* const KEY_SYMTIMEOUTSECS           = "symnAVTimeoutSecs";
const char* const KEY_SYMTIMEOUTACTION         = "symnAVTimeoutAction";
const char* const KEY_SYMMSGCATLOGGING         = "enableMsgcatLogging";
const char* const KEY_SYMCOSAUTHORITATIVE      = "symnAVCOSAuthoritive";
const char* const KEY_SYMCONNPOOLSIZE          = "symnAVConnectionPoolSize";
const char* const KEY_SYMOUTBOUNDACTION        = "symnAVOutboundAction";
const char* const KEY_SYMDELATTACHNOTICE       = "deletedAttachmentNotice";
const char* const KEY_SYMDELPLAINTEXTNOTICE    = "deletedPlainTextNotice";
const char* const KEY_SYMVIRUSNOTICEFRM        = "virusNoticeFrom";
const char* const KEY_SYMVIRUSNOTICE           = "virusNotices";
const char* const KEY_SYMVIRUSNOTICEBODYRECIP  = "virusNoticeBodyRecipients";
const char* const KEY_SYMVIRUSNOTICEHEADERRECIP= "virusNoticeHeaderRecipients";
const char* const KEY_SYMSERVERCONNRETRIES     = "symnAVServerConnRetries";
const char* const KEY_SYMSCANBYMSGTYPE	       = "symnAVScanByMsgType";
const char* const KEY_SYMSCANFAILEDACTION      = "symnAVScanFailedAction";
const char* const KEY_SYMSCANFAILNOTICEBODY    = "symnAVScanFailNoticeBody";
const char* const KEY_SYMSCANFAILNOTICEHEADER  = "symnAVScanFailNoticeHeader";
const char* const KEY_SYMSPERETRYINTERVAL      = "symnSpeRetryInterval";
const char* const KEY_SYMREPORTDEFINFO         = "symnReportDefInfoEnabled";
const char* const KEY_SYMICAPHTTPCLIENTIP      = "symnICAPHttpClientIPEnable";
const char* const KEY_SYMVERSION               = "symnAVVersion";

// 'repair' feature config keys
const char* const KEY_SYMREPAIREDATTACHNOTICE  = "repairedAttachmentNotice";
const char* const KEY_SYMREPAIREDPLAINTEXNOTICE= "repairedPlainTextNotice";
const char* const KEY_SYMDISBASE64DECODE       = "disableBase64Decode";
const char* const KEY_SYMRESCANSUSPECATTACH    = "rescanSuspectedAttachment";
const char* const KEY_SYMSCANANDCLEAN          = "symnScanAndClean";
const char* const KEY_SYMREMVALLMSGPARTS       = "removeAllMessageParts";
const char* const KEY_SYMREMALLMSGPARTSNOTICE  = "removedAllMessagePartsNotice";
const char* const KEY_SYMRESCANUUENCODE        = "rescanUUEncode";


const char* const SYMX_HEADER = "X-Sncr-SymnAVExtSvc";

const char* const LDAP_INBOUNDACTION  = "mailsymnavinboundvirusaction";
const char* const LDAP_OUTBOUNDACTION = "mailsymnavoutboundvirusaction";

// Log severity
#undef Notification
#undef Warning
#undef Error
#undef Urgent
#undef Fatal

#define Notification IM_NOTIFICATION
#define Warning IM_WARNING
#define Error IM_ERROR
#define Urgent IM_URGENT
#define Fatal IM_FATAL

#ifdef LOG
#undef LOG
#endif

#define LOG SymantecLog::writeLog
#define DIAGC SymantecLog::writeTrace

// The first two arguments of REPORT_XXXX is logHandle and IM_Error,
// as we are not checking for error returned by log api, NULL is passed.
// The below macro is used to pass first two arguments
// for REPORT_xxx function.
#define SLOGHANDLE SymantecLog::symantecLogHandle, NULL

#define CFG static MxSConfig 
#define CFG_BOUND CFG
#define CFG_BOOL  CFG
#define CFG_SERV_RESTART IM_CFG_SERV_RESTART
#define CFG_TRIVIAL IM_CFG_TRIVIAL

typedef enum
{
   SymantecInbound = 0,
   SymantecOutbound = 1,
   SymantecPostValidate = 2
} SymantecHookType;

// Syamntec Scan return code
typedef enum
{
    SymantecSuccess    = 0,
    SymantecFailure    = 1,
    SymantecNotCleaned = 2, // Symantec Engine identify a virus in a message 
                            // but could not clean it
    SymantecNotScanned = 3,
    SymantecCleanMail  = 4,
    SymantecTimeout    = 5,
    SymantecNoAction   = 6,
    SymantecScanFailed = 7
} SymantecCode;

// Symantec extension service option on unsafe mail
typedef enum
{
    SymantecActionNull =1,
    SymantecActionAllow,      // Allow viruses to be delivered
    SymantecActionSideline,   // Sideline mail that has viruses 
    SymantecActionAVSpool,    // Move the mails to sidelined/avspool director
    SymantecActionDiscard,    // Discard mails that have viruses
    SymantecActionReject,     // Reject mails that have viruses
    SymantecActionDefer,      // Defer message
    SymantecActionRepair,     // Repair mails that have viruses
    SymantecActionClean,      // Clean mails that have viruses
    SymantecActionInvalid
} SymantecAction;

// Symantec extension service action basis on COS or system configuration
typedef enum
{
    SymantecUnknown,
    SymantecInboundCos,       // Using Recipients CoS value
    SymantecInboundConfig,    // Using Config key, Symantec (Inbound) Action value
    SymantecOutboundCos,      // Using Sender's CoS value
    SymantecOutboundConfig,   // Using Config key, symantecOutboundAction value
    SymantecPostValidateCos,
    SymantecPostValidateConfig,
    SymantecVarious
} SymantecActionBase;

// Symantec message scan verdict
typedef enum
{
    SymantecVerdictOkay,              // Mail is clean
    SymantecVerdictSuspect,           // Mail is suspect of having a virus
    SymantecVerdictSuspectRepaired,   // Mail is infected with virus and got repaired
    SymantecVerdictSuspectNoRepaired, // Mail is infected with virus and not repaired	
    SymantecVerdictBadMsg,            // Message was infected. It could not be cleaned.
    SymantecVerdictTimeout,           // Symantec Protection Engine timed out
    SymantecVerdictFailed,            // Symantec Protection Engine failed to return verdict
    SymantecVerdictScanFailed,        // Scan process failed
    SymantecVerdictUnknown
} SymantecVerdict;

// Virus notice type as per COS
typedef enum 
{
    SymantecVirusNoticeNull,
    SymantecVirusNoticeInvalid,
    SymantecVirusNoticeNone,
    SymantecVirusNoticeCosOwner
} SymantecVirusNoticeCosType;

// Configuration specific constant
typedef enum
{
    SCfg = 1,
    SCfgInt,
    SCfgIntBound,
    SCfgBool,
    SCfgTime
} SConfigType;

typedef unsigned long long ulong64;
typedef char MsgNumBuf[32];
typedef unsigned char uchar;

#endif

