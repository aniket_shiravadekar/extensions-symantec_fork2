//----------------------------------------------------------------------------
//
// File Name:    mmapfile.cxx
//
// Synopsis:    This file contains MappedFileRead and MappedFileWrite class
//              implementation. This is copied from OMU and de-coupled from 
//              OMU (cleaned up OMU related code).
//----------------------------------------------------------------------------
// Copyright 2017 Synchronoss Inc. All Rights Reserved.
//
// The copyright to the computer software herein is the property of 
// Synchronoss Inc. The software may be used and/or copied only with
// The written permission of Synchronoss Inc. or in accordance with the
// terms and conditions stipulated in the agreement/contract under which
// the software has been supplied.
//----------------------------------------------------------------------------
#define mmapfile_cxx
char mmapfile_cxx_RCSID[]="$Id: //messaging/mainline/extensions/symantec/mmapfile.cxx#6 $";

//System includes
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include "errno.h"
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>

#include <cstddef>


//Local includes
#include "syutils.hxx"
#include "mmapfile.hxx"
#include "util.hxx"

#define SCFG static MxSConfig

namespace symantecscan {

int SymIsDirectory(const char* pathName);
int SymFileExists(const char* pathName);
int SymCreateDirectory (const SymString & str_PathName, mode_t mode);
SymString SymPathFromFilename(const char* filename);
int SymMakeDir(const char* pathName, mode_t mode=0770, int *pErr=NULL);

//+---------------------------------------------------------------------------
// If set to true, memory mapping will be used to read files.  This may
// causes segmentation faults or other problems if there are disk errors.
//+---------------------------------------------------------------------------
SCFG useMmapReads("useMmapReads", false, NULL, EXTN_SERVICE, SCfgBool, CFG_TRIVIAL);

//+---------------------------------------------------------------------------
// If set to true, memory mapping will be used to write files.  This may
// causes segmentation faults or other problems if there are disk errors
// (including disk full).
//+---------------------------------------------------------------------------
SCFG useMmapWrites("useMmapWrites", false, NULL, EXTN_SERVICE, SCfgBool, CFG_TRIVIAL);

//+---------------------------------------------------------------------------
// size of our in-memory buffers.
//+---------------------------------------------------------------------------
const size_t FileSrc_BUFLEN = 16 * 1024;
typedef byte Buffer[FileSrc_BUFLEN];

//+---------------------------------------------------------------------------
// must be a power of two
//+---------------------------------------------------------------------------
#define MAX_MAP_SIZE (32 * 1024)

//+---------------------------------------------------------------------------
// somewhere for buffer pointers to point to when there's no buffer.
//+---------------------------------------------------------------------------
static unsigned char nullbuf[1] = { 0 };

//+---------------------------------------------------------------------------
// Convenience function to assign filename from string.
//+---------------------------------------------------------------------------
inline void setPathname(char*& pathname, const char* string)
{
    free(pathname);
    pathname = strdup(string);
}

//+---------------------------------------------------------------------------
// Convenience function to assign filename from file descriptor.
//+---------------------------------------------------------------------------
inline void setPathname(char*& pathname, int fd)
{
    char buf[30];
    snprintf(buf, sizeof(buf), "<fd=%d>", fd);
    free(pathname);
    pathname = strdup(buf);
}

//+---------------------------------------------------------------------------
//  Will truncate a file one of two ways depending on the platform.
//  BSD's ftruncate() is avaliabe on Sun/OSF/Linux/AIX.
//
//  Returns 0 on success.
//+---------------------------------------------------------------------------
int truncateFile(int fd, off_t len)
{
    int result;
    result = ftruncate(fd, len);
    if (result == -1)
	result = errno;
    return result;
}

//+---------------------------------------------------------------------------
// initialize with a fresh buffer, but no file.
//+---------------------------------------------------------------------------
FileSnk::FileSnk()
{
    commonInit();
}

//+---------------------------------------------------------------------------
// initialize with a file descriptor.
//+---------------------------------------------------------------------------
FileSnk::FileSnk(int fd)
{
    commonInit();
    setDesc(fd, NULL);
}

//+---------------------------------------------------------------------------
// clean up
//+---------------------------------------------------------------------------
FileSnk::~FileSnk( )
{
    close();
    delete [] obase;
    delete [] pathname;
}

void FileSnk::commonInit()
{
    ofd = -1;
    offset = 0;
    obase = new Buffer;
    onext = obase;
    olimit = obase + FileSrc_BUFLEN;
    oalign = 0;
    odirty = obase;
    ohighwater = NULL;
    mappable = false;
    sysErrno = 0;
    pathname = NULL;
    fileSize = -1;
    estimateMaxFileSize = -1;
    _flushFailed = false;
    syncable = true;
    snk_magic = BYTESNKMAGIC;
}

void FileSnk::alignOutput(off_t oa)
{
    oalign = oa;
}

//+---------------------------------------------------------------------------
// Called by ByteSnk to write out a buffer.
//+---------------------------------------------------------------------------
bool FileSnk::_flushBuf(byte *out, int len)
{
    if (mappable) {
        // can't do this when using mmap.  it doesn't buy us anything
        // anyway.  The caller should just write the data into the
        // output buffer.
	//
        return false;
    }
    if (len == 0)		// no data to write
	return true;

    while (len > 0) {
	int cc = int_cast(write(ofd, out, size_t_cast(len)));

	if (cc <= 0) {
	    sysErrno = errno;
	    _flushFailed = true;
	    DIAGC("flushFailed", 1, "FileSnk::_flushBuf setting flushFailed");
	    REPORT_SymnAVWriteFail(SLOGHANDLE, Urgent, pathname, "flushing buffers", sysErrno);
	    throw sysErrno;
	}
	len -= cc;
	out += cc;
	offset += cc;
    }

    return true;
}

//+---------------------------------------------------------------------------
// Called by BufSnk when it runs out of space.
// We write out the buffer and reset our pointers.
//+---------------------------------------------------------------------------
void FileSnk::_flush(bool finished)
{
    assert(snk_magic == BYTESNKMAGIC);

    if (!mappable) {
	if (onext == odirty) {
	    return;
	}
	long cc = onext - obase;
	if (ohighwater > onext)
	    cc = ohighwater - obase;

	if (cc == 0) {
	    return;
	}
	long beforeBlock = (oalign ? offset % oalign : 0);
	long afterBlock = 0;
	// Handle an unaligned starting offset.
	if (beforeBlock) {
	    beforeBlock = oalign - beforeBlock;
	    if (beforeBlock > cc) {
		beforeBlock = cc;
	    }
	    cc -= beforeBlock;
	    // Write partial block at start.
	    _flushBuf(obase, int_cast(beforeBlock));
	}
	if (cc) {
	    long backup = 0;
	    afterBlock = (oalign ? cc % oalign : 0);
	    if (afterBlock == 0 || nextOut() < fileSize) {
		// Aligned output, or not at end of file.
		cc -= afterBlock;
		// Write aligned blocks.
		_flushBuf(obase + beforeBlock, int_cast(cc));

		if (afterBlock) {
		    // Write partial block at end.
		    _flushBuf(obase + beforeBlock + cc, int_cast(afterBlock));
		    backup = afterBlock;
		}
	    } else {
		// Unaligned output at end of file.
		long pad = oalign - afterBlock;
		memset(onext, 0, pad);
		// Write aligned blocks with padding.
		_flushBuf(obase + beforeBlock, int_cast(cc + pad));
		// Truncate padding.
		truncate(offset - pad);
		backup = oalign;
	    }
	    if (backup) {
		// Back file pointer up to start of block.
		if (lseek(ofd, -backup, SEEK_CUR) < 0) {
		    sysErrno = errno;
		    REPORT_SymnAVSeekFail(SLOGHANDLE, Urgent, sysErrno);
                    throw sysErrno;
		}
		offset -= backup;
		if (obase < (onext - afterBlock)) {
		    memcpy(obase, onext - afterBlock, afterBlock);
		}
	    }
	}
	odirty = onext = obase + afterBlock;
	ohighwater = NULL;

    } else {
        // if nowhere to flush data, forget it
        if (ofd == -1)
            return;
        if (finished) {
            retruncate();
            return;
        }
        // do we need to map more memory?
        if (onext < olimit)
            return;

        unmap();

        // page-align offset
        long bufOffset = offset & (MAX_MAP_SIZE-1);
        offset -= bufOffset;

	// Note: this code will crash the server if the
	// file can't be extended (due to insufficient disk space)
        long requiredSize = offset + MAX_MAP_SIZE;
        checkFileSize();
        if (requiredSize > fileSize) {
            // extend the file
            int err = ftruncate(ofd, requiredSize); // XXX
            assert(err >= 0);
            fileSize = requiredSize;
        }
        char *buf = (char *) mmap(NULL, MAX_MAP_SIZE, PROT_READ | PROT_WRITE,
            MAP_SHARED, ofd, offset);
        assert(buf && buf != (char *) MAP_FAILED);
        obase = (byte *) buf;
        onext = obase + bufOffset;
        olimit = obase + MAX_MAP_SIZE;
    }
}

void FileSnk::checkFileSize()
{ 
    if (fileSize == -1) {
	struct stat st;
	if (fstat(&st) < 0)
	    assert(0);
	fileSize = st.st_size;
    }
}

//+---------------------------------------------------------------------------
// truncate a file to match the logical size (minus any extension we did)
//+---------------------------------------------------------------------------
void FileSnk::retruncate()
{
    if (!mappable || !obase)
        return;
    long size = logicalSize();
    checkFileSize();
    unmap();
    if (fileSize != size)
        // error check
        ftruncate(ofd, size);
    fileSize = size;
}

//+---------------------------------------------------------------------------
// get the local size of the file (i.e. including any bytes we've written,
// but not including any bytes in additional unwritten pages we've mapped)
//+---------------------------------------------------------------------------
long FileSnk::logicalSize()
{
    long mapsize = (olimit - obase);
    checkFileSize();
    
    // if we're not extending the file, just return the file size
    if (offset + mapsize < fileSize)
        return fileSize;

    // we're extending the file, so adjust the size
    return offset + (onext - obase);
}

int FileSnk::close()
{
    return Close(false);
}

int FileSnk::Close(bool skipFlush)
{

    int result = -1;
    if (ofd >= 0) {
        //Probally file system if full
        if ((!skipFlush) && (!_flushFailed)) 
    	    _flush();
        else {
          DIAGC("flushFailed", 1,
                "FileSnk::Close skipping flush, _flushFailed is %s",(_flushFailed ? "true" : "false"));
        }
        unmap();
	result = ::close(ofd);
	if (result)
	    sysErrno = errno;
	ofd = -1;
    }
    return result;
}

int FileSnk::fstat(struct stat *buf)
{
    int result = ::fstat(ofd, buf);
    if (result)
	sysErrno = errno;
    return result;
}

bool FileSnk::open(const char* path)
{
    return open(path, O_CREAT | O_TRUNC | O_WRONLY, 0600);
}

bool FileSnk::open(const char* path, int oflag)
{
    return open(path, oflag, 0600);
}

bool FileSnk::open(const char* path, int oflag, mode_t mode)
{
    close();
    setPathname(pathname, path);

    // Memory mapped files need read/write access
    // Note: This assumes that method useMappedIO() is called before open ...
    if (mappable) {
	oflag &= ~O_WRONLY;
	oflag |= O_RDWR;
    } else if (!(oflag & (O_WRONLY|O_RDWR))) {
	oflag |= O_WRONLY;
    }
    ofd = ::open(pathname, oflag, mode);

    return ofd >= 0;
}


//+---------------------------------------------------------------------------
// set file descriptor, start writing at beginning, and write data
// format.
//+---------------------------------------------------------------------------
long FileSnk::setDesc(int fd, const char *path)
{
    if (path)
        setPathname(pathname, path);
    else
        setPathname(pathname, fd);

    _flush();			// flush any pending data
    offset = 0;

    ofd = fd;

    return nextOut();
} 

//+---------------------------------------------------------------------------
// reset where we're writing to.
//+---------------------------------------------------------------------------
void FileSnk::nextOut(long where)
{
    int whence;
    if (where == -1) {
	whence = SEEK_END;
	where = 0;
    } else {
	whence = SEEK_SET;
    }

    // we are moving the cursor (onext) to point to another part of the file.
    // if "where" is localed within the current buffer, just recalc onext to 
    //  point there
    if (pathname != NULL && ((offset + (olimit - obase) ) > where ) 
	&& (offset <= where)
	&& (ohighwater != NULL && where <= (offset + (ohighwater - obase)) || ohighwater == NULL)) {
	onext = obase + (where - offset);
    } else {
	// we need to flush the buffer to disk and reset "where" to point
	// to the begining of the buffer
	_flush();			// flush any pending data
	odirty = onext = obase;
	ohighwater = NULL;
	unmap();

	if ((where = lseek(ofd, where, whence)) < 0) {
	    sysErrno = errno;
	    REPORT_SymnAVSeekFail(SLOGHANDLE, Urgent, sysErrno);
	} else {
	    offset = where;
	}
    }
}


//+---------------------------------------------------------------------------
// return where we're writing to.
//+---------------------------------------------------------------------------
long FileSnk::nextOut()
{
    return
      offset				// offset of this buffer
      + (onext - obase);		// + amount in this buffer
}

bool FileSnk::truncate(off_t len)
{
    int err = truncateFile(ofd, len);
    if (err) {
	sysErrno = err;
	REPORT_SymnAVTruncateFailed(SLOGHANDLE, Error, pathname, len, sysErrno);
    } else {
	fileSize = len;
    }
    return err == 0;
}

//+---------------------------------------------------------------------------
// use mapped I/O when writing out the file
//+---------------------------------------------------------------------------
void FileSnk::useMappedIO()
{
    if (useMmapWrites.getBool()) {
        fileSize = -1;
        mappable = true;
        delete obase;
        odirty = onext = olimit = obase = ohighwater = NULL;
    }
}

void FileSnk::useSyncedIO()
{
    syncable = false;
}

void FileSnk::unmap()
{
    if (!mappable || !obase)
        return;

    if (munmap((char *) obase, olimit - obase) < 0) { 
        perror("munmap"); // XXX
        assert(0);
    } 

    // advance offset by size of area we've written out
    offset += onext - obase;
    
    // force a fill() the first time we attempt to read anything
    odirty = olimit = onext = obase = ohighwater = NULL;
}

//+---------------------------------------------------------------------------
// sync the system buffers to disk for this file
//+---------------------------------------------------------------------------
bool FileSnk::sync()
{
    if (syncable) {
	if (mappable && obase) {
	    // sync the mmaped area
	    if (msync((char *) obase, olimit-obase, MS_SYNC)) {
		sysErrno = errno;
		return false;
	    }
	}
    }
    flush();
    if (syncable) {
	if (fsync(ofd)) {
	    sysErrno = errno;
	    return false;
	}
    }
    return true;
}

//+---------------------------------------------------------------------------
// write a raw block of data
//+---------------------------------------------------------------------------
void FileSnk::swrite(const void* cP, size_t count)
{
    assert(snk_magic == BYTESNKMAGIC);
    const char *cp = (const char *) cP;
    ptrdiff_t bufsize = olimit - obase;
    const size_t countOrig = count;
    for (;;) {
        if (count <= (olimit - onext)) {
            // write out rest of data
            memcpy(onext, cp, count);
            onext += count;
	    if (ohighwater < onext)
		ohighwater = onext;
            break;
        }

        // try to write the data without copying it
        if (onext == obase && bufsize && _flushBuf((byte*)cp, int_cast(bufsize))) {
            cp += bufsize;
            count -= bufsize;
	    assert(count < countOrig);  // check for underflow
            continue;
        }
        
        // write out all data we can, then flush
        size_t remaining = olimit - onext;
        memcpy(onext, cp, remaining);
        onext += remaining;
	if (ohighwater < onext)
	    ohighwater = onext;
        count -= remaining;
	cp += remaining;
        _flush(false);
    }
}




SCFG cacheLimitInKB("cacheLimitInKB", (long int)256, "", EXTN_SERVICE, SCfgIntBound, CFG_TRIVIAL);
SCFG mapfileAlignment("mapfileAlignment", (long int)0, "", EXTN_SERVICE, SCfgIntBound, CFG_TRIVIAL);


MappedFileWrite::MappedFileWrite()
    : _f_InMem(false), _n_Magic(MMAPPED_FILE_WRITE_MAGIC)
{
    _snk.alignOutput(mapfileAlignment.getInt());
    _n_MemLimit = 256 * 1024;
    _n_MemLimit = size_t_cast(cacheLimitInKB.getInt() * 1024);
}

MappedFileWrite::~MappedFileWrite()
{
    assert(_n_Magic == MMAPPED_FILE_WRITE_MAGIC);
    Close();
    _n_Magic = 0;
}

bool MappedFileWrite::Flush(bool f_Sync)
{
    assert(_n_Magic == MMAPPED_FILE_WRITE_MAGIC);
    if (!_snk.opened())
        return true;

    _snk.flush();
    if (f_Sync && !_snk.sync())
        return false;
    
    return true;
}

bool MappedFileWrite::Close(bool f_Sync)
{
    assert(_n_Magic == MMAPPED_FILE_WRITE_MAGIC);
    if (!_snk.opened())
        return true;
    bool ok;
    if (!_snk.lastFlushFailed())
        ok = Flush(f_Sync);
    else {
        DIAGC("flushFailed",1,"MappedFileWrite::Close, skipping Flush");
        ok = false;
    }
    _snk.close();

    return ok;
}

void MappedFileWrite::FlushCache()
{
    if (!_f_InMem)
        return;

    if (!Create(_str_Filename))
        return;

    Write(_str_Mem);
    DeleteCache();
}

void MappedFileWrite::Write(const char *p_Ptr, u_long n_Len)
{
    assert(_n_Magic == MMAPPED_FILE_WRITE_MAGIC);
    if (_f_InMem)
    {
        if (n_Len + size_t_cast(Length(_str_Mem)) >= _n_MemLimit)
        {
            FlushCache();
        }
        else
        { 
            _str_Mem.append(p_Ptr, n_Len);
            return;
        } 
    }
    
    if (!IsOpen()) {
	REPORT_SymnAVMmapFileNotOpen(SLOGHANDLE, Urgent, getFilename());
        throw;
    }

    _snk.swrite((const void *) p_Ptr, n_Len);
}

void MappedFileWrite::Sync()
{
    if (IsOpen())
        if (!_snk.sync())
	{
	    REPORT_SymnAVFsyncFail(SLOGHANDLE, Urgent, getFilename(), _snk.getErrno());
            throw;
	}
}

int MappedFileWrite::GetFileSize()
{
    return (_f_InMem) ? Length(_str_Mem) : int_cast(_snk.nextOut());
}

bool MappedFileWrite::Create(const char *file, MapFileCreateOptions options)
{
    Close();

    if (_str_Filename != file)
        _str_Filename = file;

    int flags = O_RDWR | O_EXCL | O_TRUNC | O_CREAT;
    if (options == MapFileCreateSynced)
	flags |= O_SYNC;

    int fd = open(file, flags, 0660);
    if (fd == -1)
    {
        // could be that it is a delay bucket create, try to create dir
        SymMakeDir(SymPathFromFilename(file));
        fd = open(file, flags, 0660);
        if (fd == -1)
            return false;
    }
    _snk.setDesc(fd, file);
    _snk.useMappedIO();
    if (options == MapFileCreateSynced) {
	_snk.useSyncedIO();
    }

    _f_InMem = false;

    return true;
}

void MappedFileWrite::CreateCached(const char *file)
{
    _str_Filename = file;
    _f_InMem = true;
}

//+---------------------------------------------------------------------------
//	Synopsis: Create a memory-mapped file object corresponding to
//                str_File.  Check IsOK() to see if the object was
//                created successfully.
//----------------------------------------------------------------------------
MappedFileRead::MappedFileRead(const char* file)
    : _f_OK(false), _p_Buffer(NULL), _n_Length(0), _p_Filename(NULL),
      _f_NeedsClosing(false), _fd_File(-1)
{
    Open(file);
}


MappedFileRead::MappedFileRead(int fd)
    : _f_OK(false), _p_Buffer(NULL), _n_Length(0), _p_Filename(NULL),
      _f_NeedsClosing(false), _fd_File(fd)
{
    _Map();
}


MappedFileRead::MappedFileRead()
    : _f_OK(false), _p_Buffer(NULL), _n_Length(0), _p_Filename(NULL),
      _f_NeedsClosing(false), _fd_File(-1)
{
    _Map();
}


bool MappedFileRead::Open(const char *file)
{
    _p_Filename = strdup(file);
    _fd_File = open(file, O_RDONLY, 0);
    if (_fd_File == -1)
    {
        return false;
    }

    _f_NeedsClosing = true;
    _Map();
    return true;
}


void MappedFileRead::_Map()
{
    struct stat Stat;
    if (fstat(_fd_File, &Stat) < 0)
    {
        return;
    }

    if (!S_ISREG(Stat.st_mode))
    {
        return;
    }

    _f_OK = true;
    _n_Length = Stat.st_size;

    // Unix chokes on zero length files for mmap
    if (Stat.st_size > 0)
    {
	// Memory map the file

	// we use MAP_PRIVATE rather than MAP_SHARED
	// because we're only reading and HP's mmap() doesn't
	// let you have multiple open MAP_SHARED mmaps on the same file
	if ((_p_Buffer = (char *) mmap(NULL, Stat.st_size, PROT_READ,
				       MAP_PRIVATE, _fd_File, 0))
	    == (char *)MAP_FAILED)
	{
            _p_Buffer = NULL;
            _f_OK = false;
	    REPORT_SymnAVMmapFailed(SLOGHANDLE, Urgent, getFilename(), errno);
	    throw;
	}
    }
}


//+---------------------------------------------------------------------------
//	Synopsis:	Destructor.
//----------------------------------------------------------------------------
MappedFileRead::~MappedFileRead()
{
    Close();

    free(_p_Filename);
}


//+---------------------------------------------------------------------------
//	Synopsis:	Unmap the file.
//----------------------------------------------------------------------------
void MappedFileRead::Close()
{
    if (_p_Buffer)
    {
        munmap(_p_Buffer, _n_Length);
        _p_Buffer = NULL;
    }

    if (_fd_File >= 0 && _f_NeedsClosing)
        ::close(_fd_File);
    _fd_File = -1;
    _f_NeedsClosing = false;
    _n_Length = 0;
    _f_OK = false;
}

MxMessage::MxMessage(SMappedFileWrite* hdr, SMappedFileWrite* body)
          : _parser(NULL)
          , _header(NULL) 
          , _body(NULL) 
{
    _parser = new Parser(hdr->GetCachePtr(), body->GetCachePtr(),
                         size_t_cast(hdr->GetCacheLen()), size_t_cast(body->GetCacheLen()));
}


MxMessage::MxMessage(const char* hdrFile, const char* bodyFile)
          : _parser(NULL)
          , _header(NULL) 
          , _body(NULL) 
{
    try {
        _header = new SMappedFileRead(hdrFile);
        if(!_header->IsOK()) {
            REPORT_SymnAVMmapFailed(SLOGHANDLE, Error, hdrFile, errno);
            throw;
        }
    
        _body = new SMappedFileRead(bodyFile);
        if(!_body->IsOK()) {
            REPORT_SymnAVMmapFailed(SLOGHANDLE, Error, bodyFile, errno);
            throw;
        }

    } catch(...) {
        delete _header;
        delete _body;
        _header = _body = NULL;
        throw;
    }
    
    DIAGC("symantec", 2, "Creating MxMessage using header(%s) and body(%s)",
          hdrFile, bodyFile);
    
    _parser = new Parser(_header->getBuffer(), _body->getBuffer(),
                        _header->getSize(), _body->getSize());
}
    

MxMessage::~MxMessage()
{
    delete _parser;
    delete _header;
    delete _body;
}



int SymIsDirectory(const char* pathName)
{
    struct stat Stat;
     
    // get info. about the pathname
    if (stat (pathName, &Stat) < 0) return 0;
	      
    // check if it's a directory
    return (S_ISDIR (Stat.st_mode));
}

int SymFileExists(const char* pathName)
{
   return (access (pathName, F_OK) == 0);
}

int SymCreateDirectory (const SymString & str_PathName, mode_t mode)
{
    if (mkdir (str_PathName, mode) < 0) {
        //
        return 0;
    }

    return 1;
}

int SymMakeDir(const char* pathName, mode_t mode, int *pErr)
{
    // Initialize the error return value, if specified
    if (pErr)
	*pErr = 0;


    SymString str_PathName = pathName;
    // did we get an empty string for the pathname?
    if (str_PathName.size() == 0) return 0;

    // check if the directory already exists
    if (SymIsDirectory (str_PathName)) return 1;

    StringArray strl_SubDirs  = str_PathName.Split("/");

    SymString str_SubDir;

    // Add "." if it is a relative path (no leading '/')
    if (str_PathName[0] != '/')
    {
        str_SubDir = '.';
    }

    // recursively make the directory
    for (int index =0; index < int_cast(strl_SubDirs.size()); index++)
    {
        str_SubDir += '/';
        str_SubDir += strl_SubDirs[index];

        if (SymFileExists (str_SubDir))
	    continue;
        // make the subdirectory
        if (SymCreateDirectory(str_SubDir, mode))
	    continue;

        if (pErr) *pErr = errno;

        // The directory may have been created since we
        // called FileExists(), if there are other processes
        // trying to create it.

        if (!(SymIsDirectory(str_SubDir)))
        {
            return 0;
        }
    }

    return SymIsDirectory (pathName);

}


//+---------------------------------------------------------------------------
//  Synopsis:   Extract the path portion of a filename (with path component).
//
//              Meant to be used as a utility function with the Sdbfile
//              file/path constructor.
//
//  Returns:    The path name as an SString or "." if the path is empty.
//----------------------------------------------------------------------------
SymString SymPathFromFilename(const char* filename)
{
    SymString str_FileName(filename);
    StringArray strl = str_FileName.Split("/");
    strl.pop_back();
    SymString str_Path = Join(strl, "/");

    if (filename[0] == '/')
        str_Path = "/" + str_Path;

    return (str_Path == "" ? SymString(".") : str_Path);
}

}
