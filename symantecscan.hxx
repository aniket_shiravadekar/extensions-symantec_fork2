//----------------------------------------------------------------------------
//
// File Name:    symantecscan.hxx
//
// Synopsis:     SymantecScanner class declaration
//----------------------------------------------------------------------------
// Copyright 2017 Synchronoss, Inc. All Rights Reserved.
//
// The copyright to the computer software herein is the property of
// Synchronoss, Inc. The software may be used and/or copied only with
// The written permission of Synchronoss, Inc. or in accordance with the
// terms and conditions stipulated in the agreement/contract under which
// the software has been supplied.
//----------------------------------------------------------------------------

#ifndef __SYMANTECSCAN_H__
#define __SYMANTECSCAN_H__

#include <iostream>
#include <string.h>

#include "symcsapi.h"
#include "syutils.hxx"
#include "msginfo.hxx"
#include "mmapfile.hxx"

using namespace std;
using namespace symantecscan;

//+---------------------------------------------------------------------------
//
// Synopsis: Scan and clean the mail for viruses. It uses Symantec ICAP library
//			to send data to Symatec Engine for scanning.
//---------------------------------------------------------------------------
class SymantecScanner
{
  public:
    // Constructor and Destructor
    SymantecScanner();
    ~SymantecScanner();

    // Main function to scan and clean message
    SymantecVerdict scanMessage(const StringInfo* strHeader, const StringInfo* strBody, 
                                MsgInfo*, bool bRepair);
    SymantecVerdict cleanMessage(MsgInfo* msg,
                                 SMappedFileWrite* mfwHeader,
                                 SMappedFileWrite* mfwBody,
                                 SMappedFileRead* mfrHeader,
                                 SMappedFileRead* mfrBody,
                                 bool bRepair);

    int getScanProblemInfo(HSCANRESULTS hResults, 
                           int iWhichProblem, 
                           string& virusName, 
                           string& filename,
                           MsgInfo* msg);
    inline bool isValid() { return _initialized; }

  private:
    SymantecVerdict _doScanning(const StringInfo* strHeader, const StringInfo* strBody,
                                MsgInfo* msg, string& virusName, bool bRepair);
    string _getSymantecClientConfig();
    void   _rewriteHeaders(SymString& newPart,
                         SymString& origNotice,
                         MsgInfo* msg,
                         SMappedFileWrite* mfwHeader);
    SymantecVerdict parseBodyPart(MsgInfo*          msg,
                                  Entity*           mimeEntity,
                                  SMappedFileWrite* mfwHeader,
                                  SMappedFileWrite* mfwBody,
                                  bool              bRepair);

    MxMessage* createMxMessage(MsgInfo* msg);
    SymantecVerdict scanUUencodePart(MsgInfo* msg, const char* data, int length,
                                     string& virus, const char* filename,
                                     bool bRepair, SymString& str_NewBody);
    SymString getVirusContext(SymString vname, SymString fname, MsgInfo* msg);

  private:
    HSCANCLIENT  _scanClient;
    bool         _initialized;

    string _strConnConfig;
};

// mime identifier
const char* const MIME_FORM = "This is a multipart message in MIME format.";
const int MIME_LEN = 43;
#endif
