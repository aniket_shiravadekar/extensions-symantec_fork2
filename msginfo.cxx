//----------------------------------------------------------------------------
//
// File Name:    msginfo.cxx
//
// Synopsis:     MsgInfo and Recipient class implementation
//----------------------------------------------------------------------------
// Copyright 2017 Synchronoss, Inc. All Rights Reserved.
//
// The copyright to the computer software herein is the property of
// Synchronoss, Inc. The software may be used and/or copied only with
// The written permission of Synchronoss, Inc. or in accordance with the
// terms and conditions stipulated in the agreement/contract under which
// the software has been supplied.
//----------------------------------------------------------------------------

#include <sys/types.h>
#include <unistd.h>

#include "msginfo.hxx"
#include "symantecapp.hxx"
#include "mmapfile.hxx"
#include "im_stat_internal.hxx"
#include "xmimecontent.hxx"

namespace symantecscan
{

//+---------------------------------------------------------------------------
//
//Method:    MailUser::MailUser
//
//Synopsis:  Constructor for MailUser class
//
//----------------------------------------------------------------------------
MailUser::MailUser(IMxMailgram* Mailgram) : _mailgram(Mailgram),
                                            _envelope(NULL),
                                            _result(0),
                                            _actionType(SymantecUnknown),
                                            _action(SymantecActionInvalid),
                                            _noticeStr(),
                                            _actionStr()
{
    _result = _mailgram->getEnvelope(&_envelope);
    if( _result < 0 ){
        LOG(Error, "AVExtServerDataReadFailed", 
            "Error reading Envelope information from extension server");
        throw(_result);
    }
}

//+---------------------------------------------------------------------------
//
//Method:    MailUser::~MailUser
//
//Synopsis:  Destructor for MailUser class
//
//----------------------------------------------------------------------------
MailUser::~MailUser()
{}

//+---------------------------------------------------------------------------
//
// Method:    MailUser::setSymantecAction
//
// Synopsis:  Sets _action variable of type SymantecAction    
//
//----------------------------------------------------------------------------
void MailUser::setSymantecAction(SymantecAction symAction)
{
    _action = symAction;
}

//+---------------------------------------------------------------------------
//
// Method:     MailUser::getCosAttr
//
// Synopsis:   returns CoS value for the corresponding CoS attribute.
//
// Inputs:     CoS attribute key
//
//----------------------------------------------------------------------------
SymString MailUser::getCosAttr(const char* cosAttrs,
                              const char* cosName, bool isString)
{
    SymString str(cosAttrs);
    int iStart, iEnd;

    if ((iStart = str.Index(cosName)) < 0 )
        return "";

    SymString endStr = ":";
    SymString deliStr = " ";
    int incStartPos = 1;
    int len;
    if( isString && (str.Index("\"", iStart) >= 0 ))
    {
        endStr = ":\"";
        deliStr = "\"";
        incStartPos = 2;
    }

    if ((iEnd = str.Index(endStr, iStart)) < 0 )
        return "";

    iStart = iEnd + incStartPos;

    if ((iEnd = str.Index(deliStr, iStart)) >= 0 )
    {
        len = iEnd - iStart;
    } else {
        //When attribute is last element, then we deliStr will not be found.
        len = Len(str) - iStart;
    }

    SymString value = str.Segment(iStart, len);
    DIAGC("mailUser", 1, "CoS: %s=%s", cosName, value.AsPtr());
    return value;
}

//+---------------------------------------------------------------------------
//
// Method:    Recipient::Recipient
//
//Synopsis:   Constructor for Recipient class
//
//----------------------------------------------------------------------------
Recipient::Recipient(IMxMailgram* Mailgram, int index)
            : MailUser(Mailgram),
            _index(index),
            _bAddress(false),
            _bRemote(false)
{
}

//+---------------------------------------------------------------------------
//
// Method:    Recipient::~Recipient
//
//Synopsis:   Destructor for Recipient class
//
//----------------------------------------------------------------------------
Recipient::~Recipient()
{}

//+---------------------------------------------------------------------------
//
// Method:    Recipient::isRemoteAddress
//
// Synopsis:   returns address is remote or local
//
//----------------------------------------------------------------------------
bool Recipient::isRemoteAddress()
{
    _bRemote = (getAddrStrInfo()->str[0] == '<') ? true : false;
    return _bRemote;
}

//+---------------------------------------------------------------------------
//
// Method:     Recipient::getAddrStrInfo
//
// Synopsis:   returns recipient's address as StringInfo*
//
//----------------------------------------------------------------------------
const StringInfo* Recipient::getAddrStrInfo()
{
    IMxMailAddress*   mailAddress;
    const StringInfo* strInfoAddress;

    _result = _envelope->getRcptTo(_index, &mailAddress);
    _result = mailAddress->getAddress( &strInfoAddress);

    return strInfoAddress;
}

//+---------------------------------------------------------------------------
//
// Method:     Recipient::getRcptToDisp
//
// Synopsis:   returns recipient's Disposition
//
//----------------------------------------------------------------------------
const char* Recipient::getRcptToDisp()
{
    const StringInfo* infoRcptToDisp;
    _result = _mailgram->getRcptToDisp( _index, &infoRcptToDisp);

    if(!_result)
        return infoRcptToDisp->str;

    return NULL;
}

//+---------------------------------------------------------------------------
//
// Method:     Recipient::getDisposition
//
// Synopsis:   returns recipient's Disposition
//
//----------------------------------------------------------------------------
int Recipient::getDisposition(StringInfo** infoRcptToDisp)
{
    return _mailgram->getRcptToDisp( _index, (const StringInfo**) infoRcptToDisp);
}

//+---------------------------------------------------------------------------
//
// Method:     Recipient::getRcptToAttr
//
// Synopsis:   returns recipient's CoS attributes in the
//             form "key:value key:value......"
//
//----------------------------------------------------------------------------
const char* Recipient::getRcptToAttr()
{
    const StringInfo* infoRcptToAttr;
    _result = _mailgram->getRcptToAttr(_index, &infoRcptToAttr);
    if (!_result) {
        return infoRcptToAttr->str;
    }
    return NULL;
}

//+---------------------------------------------------------------------------
//
// Method:     Recipient::getSymantecAction
//
// Synopsis:   Parse Recipient's CoS Attributes and get virus action
//             for recipient.
//
// Inputs:     None
//
//----------------------------------------------------------------------------
SymantecAction Recipient::getSymantecAction()
{
    _actionStr = getCosAttr(getRcptToAttr(), LDAP_INBOUNDACTION);
    DIAGC("recipient", 5, "Recipient action= %s", _actionStr.AsPtr());
    _action = symantecApp->getSymantecAction(_actionStr);

    // if user action set to invalid or empty then system level action override it.
    if (_action == SymantecActionInvalid || _action == SymantecActionNull) {
   
        // check system action only CoSAuth set to false
        if (!symantecApp->isCOSAuthoritative()) {
            _action = symantecApp->getSymantecAction( symantecApp->getSymantecInboundAction());
        }
    }
    return _action;
}

//+---------------------------------------------------------------------------
//
// Method:     Recipient::getAddress
//
// Synopsis:   returns recipient's address as const char*
//
//----------------------------------------------------------------------------
const StringInfo* Recipient::getAddress()
{
    const StringInfo* addrInfo = getAddrStrInfo();

    if (addrInfo != NULL)
        return addrInfo;

    return NULL;
}

//+---------------------------------------------------------------------------
//
// Method:    Sender::Sender
//
//Synopsis:   Constructor for class Sender
//
//----------------------------------------------------------------------------
Sender::Sender(IMxMailgram* Mailgram)
            : MailUser(Mailgram)
{
}

//+---------------------------------------------------------------------------
//
// Method:    Sender::~Sender
//
//Synopsis:   Destructor for class Sender
//
//----------------------------------------------------------------------------
Sender::~Sender()
{}

//+---------------------------------------------------------------------------
//
// Method:     Sender::getAddrStrInfo
//
// Synopsis:   returns sender's address as StringInfo*
//
//----------------------------------------------------------------------------
const StringInfo* Sender::getAddrStrInfo()
{
	
    IMxMailAddress* mailAddress;
    const StringInfo* strInfoAddress;

    _result = _envelope->getMailFrom( &mailAddress );
    _result = mailAddress->getAddress( &strInfoAddress );

    return strInfoAddress;
}

//+---------------------------------------------------------------------------
//
// Method:     Sender::getMailFromAttr
//
// Synopsis:   returns sender's CoS attributes in the
//             form "key:value key:value......"
//
//----------------------------------------------------------------------------
const char* Sender::getMailFromAttr()
{
    const StringInfo* infoMailFromAttr;
    _result = _mailgram->getMailFromAttr( &infoMailFromAttr);

    if(!_result)
        return infoMailFromAttr->str;

    return NULL;
}

//+---------------------------------------------------------------------------
//
// Method:     Sender::getSymantecAction
//
// Synopsis:   Parse Sender CoS Attributes and get virus action
//             for sender.
//
// Inputs:     None
//
//----------------------------------------------------------------------------
SymantecAction Sender::getSymantecAction()
{
    // Read action string from Sender's CoS attributes
    _actionStr = getCosAttr(getMailFromAttr(), LDAP_OUTBOUNDACTION);
    DIAGC("sender", 5, "Sender action=%s", _actionStr.AsPtr());
    _action = symantecApp->getSymantecAction(_actionStr);

    // if user action set to invalid or empty then system level action override it.
    if (_action == SymantecActionInvalid || _action == SymantecActionNull) {
    
        // Check system action only if CoSAuth set to false
        if (!symantecApp->isCOSAuthoritative()) {
            _action = symantecApp->getSymantecAction( symantecApp->getSymantecOutboundAction());
        }
    }
    return _action;
}

//+---------------------------------------------------------------------------
//
// Method:     MsgInfo::MsgInfo
//
// Synopsis:   The information from Extension Server is read
//             and pointers are assigned to data members
//
//----------------------------------------------------------------------------
MsgInfo::MsgInfo(IMxMailDelivery* MailDelivery)
        :_newHeader(NULL),
        _newBody(NULL),
        _MailSender(NULL),
        _pRepairedBody(NULL),
        _pRepairedHeader(NULL),
        _mailDelivery(MailDelivery),
        _mailgram(NULL),
        _envelope(NULL),
        _mailMessage(NULL),
        _sfwHeader(NULL),
        _sfwBody(NULL),
        cleanHeader(NULL),
        cleanBody(NULL),
        _fTimeConsume(0),
        _initTime(0),
        _result(0),
        _rcptToCount(0),
        _bRepaired(false),
        _repairFailed(false),
        _headerRewritten(false),
        _bSymantecXHeaderAdded(false),
        _outboundMail(false),
        _bMsgPartSuspect(false),
        _msgID(),
        _subject(),
        _virusname(),
        _fileName(),
        _dateSubmitted(),
        _problemDetails(),
        _currVirusName(),
        _currInfcFileName(),
        _recipientList(),
        _peerIP(),
        _mailContext()
			
{
    DIAGC("msgInfo", 5, "Entering ctor MsgInfo");

    _initTime = MethodTimingSnapShot::getRealTimeClock();
	
    // Clear recipient array
    _recipArray.clear();
    
    // clean up previous object
    _cleanRecipArray();    

    // set scan verdict, need in case exception occured
    _virusVerdict = SymantecVerdictOkay;
    _scanVirusVerdict = SymantecVerdictOkay;

    _dateSubmitted = IM_GetRFC822Date();

    // Getting mailgram
    _result = _mailDelivery->getMailgram(&_mailgram);
    if (_result < 0) {
        LOG(Error, "AVExtServerDataReadFailed", "Error getting MailGram");
        throw(_result);
    }    

    // Getting mail envelop
    _result = _mailgram->getEnvelope(&_envelope);
    if (_result < 0) {
        LOG(Error, "AVExtServerDataReadFailed", "Error getting Envelop");
        throw(_result);
    }

    _result = _envelope->getRcptToCount(&_rcptToCount);
    if (_result < 0) {
        LOG(Error,"AVExtServerDataReadFailed", "Error getting Recipient count");
        throw(_result);
    }
    DIAGC("msgInfo", 5, "Recipient count =%d", _rcptToCount);
   
    _result = _mailgram->getMessage(&_mailMessage);
    if (_result < 0) {
        LOG(Error,"AVExtServerDataReadFailed", "Error getting Message");
        throw(_result);
    }

    // store sender
    _result = _storeSender();

    // store recipient
    _storeRecipients();

    setMessageID();
	
    _peerIP = readFromHeader("[", "]");

    _subject = readFromHeader("subject:", false);
	
    getMailFrom();
    getRecipientList();

    _buildMailContext();

    DIAGC("msgInfo", 5, "Exit MsgInfo");
}


//+---------------------------------------------------------------------------
//
// Method:     MsgInfo::~MsgInfo
//
//Synopsis:    Destructor for class MsgInfo
//
//----------------------------------------------------------------------------
MsgInfo::~MsgInfo()
{
    _cleanRecipArray();
    delete _MailSender;	
    _MailSender = NULL;

    if (_pRepairedBody) {
        if (_pRepairedBody->str) delete []_pRepairedBody->str;

        _pRepairedBody->str = NULL;
        delete _pRepairedBody;
        _pRepairedBody = NULL;
    }

    if (_pRepairedHeader) {
        if (_pRepairedHeader->str) delete []_pRepairedHeader->str;
    
        _pRepairedHeader->str = NULL;
        delete _pRepairedHeader;
        _pRepairedHeader = NULL;
    }
}

//+---------------------------------------------------------------------------
//
// Method:     MsgInfo::_cleanRecipArray
//
// Synopsis:   deletes each elements of array
//
//----------------------------------------------------------------------------
void MsgInfo::_cleanRecipArray()
{
    // loop through and delete all each object in the array
    for(int index=0; index < int_cast(_rcptToCount); index++) {
       delete _recipArray[index];
    }

    _recipArray.clear();
}

//+---------------------------------------------------------------------------
//
// Method:     MsgInfo::getMailFrom
//
// Synopsis:   returns MailFrom as StringInfo
//
//----------------------------------------------------------------------------
const StringInfo* MsgInfo::getMailFrom()
{
    DIAGC("msgInfo", 5, "Entry getMailFrom");
    IMxMailAddress*	pMailFrom;
    const StringInfo* pInfoMailFrom;

    _result = _envelope->getMailFrom( &pMailFrom );
	
    pMailFrom->getAddress( &pInfoMailFrom );

    return pInfoMailFrom;
}

//+---------------------------------------------------------------------------
//
// Method:     MsgInfo::getBody
//
// Synopsis:   returns body information of mail
//
//----------------------------------------------------------------------------
const StringInfo* MsgInfo::getBody()
{	
    const StringInfo* pInfoBody;
    IMxMessageBody* pBody;

    _result = _mailMessage->getBody( &pBody );
    _result = pBody->getBody( &pInfoBody);

    return pInfoBody;
}

//+---------------------------------------------------------------------------
//
// Method:     MsgInfo::getHeader
//
// Synopsis:   returns Header information (Envelope data) of mail
//
//----------------------------------------------------------------------------
const StringInfo* MsgInfo::getHeader()
{
    const StringInfo* pInfoHeader;
    IMxMessageHeader* pHeader;

    _result = _mailMessage->getHeader( &pHeader);
    _result = pHeader->getHeader( &pInfoHeader );

    return pInfoHeader;
}

//+---------------------------------------------------------------------------
//
// Method:     MsgInfo::readFromBody
//
// Synopsis:   returns the string from body between start and end tokens
//
// Inputs:     Start token and end token
//
//----------------------------------------------------------------------------
SymString MsgInfo::readFromBody(const char* cStart, const char* cEnd, int length)
{
    SymString strBody(getBody()->str, getBody()->len);
    SymString newStrBody;
    if (length == 0 || length > Len(strBody))
        newStrBody = strBody;
    else
        newStrBody = strBody.Segment(0, length);

    return _searchSString(newStrBody, cStart, cEnd);   
}

//+---------------------------------------------------------------------------
//
// Method:    MsgInfo::getSubject
//
// Synopsis:  Parse header and returns "Subject:" field
//
//----------------------------------------------------------------------------
string MsgInfo::getSubject()
{
    return _subject;
}

//+---------------------------------------------------------------------------
//
// Method:    MsgInfo::deleteSymantecXHeader
//
// Synopsis:  Search X-Opwv-SymantecExtSvc  header and remove
//            When the message is bounced this header is found in body
//
//----------------------------------------------------------------------------
void MsgInfo::deleteSymantecExtSvcXHeader(bool isBody)
{
    SymString newStr;
    SymString str = isBody ? getBody()->str  : getHeader()->str;
    const char* endChar = "\r\n";

    int iStart, iEnd;
    iStart = str.Index(symantecApp->getXOpwvHeader());
    
    if (iStart < 0)
        return; // String not found

    newStr = str.Segment(0, iStart);
    iEnd   = str.Index(endChar, iStart);

    if (iEnd >=0)
        newStr += str.Segment(iEnd + int_cast(strlen(endChar)), Len(str)-1);
    
    if (Len(newStr) <= 0)
        return;
    StringInfo* newText = new StringInfo;
    if (newText != NULL) {
        int len = Len(newStr);
        char* pstr = new char[len];
        if (pstr != NULL) {
            memcpy(pstr, newStr.AsPtr(), len);
            newText->str = pstr;
            newText->len = len;
        }
    }

    if (!isBody) {
        if (_newHeader) {
            if (_newHeader->str) delete []_newHeader->str;
            delete _newHeader;
        }
        _newHeader = newText;
    } else {
        if (_newBody) {
            if (_newBody->str) delete [] _newBody->str;
            delete _newBody;
        }
        _newBody = newText;
    }
}

//+---------------------------------------------------------------------------
//
// Method:    MsgInfo::addSymantecExtSvcXHeader
//
// Synopsis:  Append X-Opwv-SymantecExtSvc header
//
//----------------------------------------------------------------------------
void MsgInfo::addSymantecExtSvcXHeader(const char* value)
{
    SymString strHeader(getHeader()->str, getHeader()->len);
    
    strHeader += SymString(symantecApp->getXOpwvHeader()) + value;
    strHeader += "\r\n";

    if (_newHeader) {
        if (_newHeader->str) delete []_newHeader->str;
        delete _newHeader;
    }

    _newHeader = new StringInfo;
    if (_newHeader) {
        int len = strHeader.size();
        char* pstr = new char[len];
        if (pstr != NULL) {
            memcpy(pstr, strHeader.AsPtr(), len);
            _newHeader->str = pstr;
            _newHeader->len = len;
        }
    }

    // setting the flag that header has been added
    _bSymantecXHeaderAdded = true;
}

//+---------------------------------------------------------------------------
//
// Method:     MsgInfo::setMessageID
//
// Synopsis:   sets message-id. If MTA fails passing it,
//             message-id is parsed from header.
//
// Inputs:     None
//
//----------------------------------------------------------------------------
void MsgInfo::setMessageID()
{
    IMxSmtpControlInfo* pIMxSmtpControlInfo;
    DIAGC("msgInfo", 5, "Entering setMessageID");

    // Get message id from smtp
    if ( _mailDelivery->getControlInfo(&pIMxSmtpControlInfo) != -1 ) {
        const StringInfo* msgID;

        pIMxSmtpControlInfo->getMsgID(&msgID);
			
        if (msgID != NULL) {
            _msgID.assign(msgID->str, msgID->len);
            return;
        }
    }	

    DIAGC("msgInfo", 5, "Exit setMessageID"); 
}

//+---------------------------------------------------------------------------
//
// Method:     MsgInfo::setMessageID
//
// Synopsis:   set message-id. If MTA fails passing it,
//             message-id is parsed from header.
//
// Inputs:     None
//
//----------------------------------------------------------------------------
string MsgInfo::getMessageID()
{
    return _msgID;
}

//+---------------------------------------------------------------------------
//
// Method:    MsgInfo::_storeSender
//
// Synopsis:  Create "Sender" object
//
//----------------------------------------------------------------------------
SymantecCode MsgInfo::_storeSender(void)
{
    SymantecCode retCode = SymantecSuccess;
    DIAGC("msgInfo", 5, "Entering storeSender");
    try {
        _MailSender = new Sender(_mailgram);
    } catch(...) {
		
        LOG(Error, "AVExtServerDataReadFailed", "Error creating Sender object");
        retCode = SymantecFailure;
        delete _MailSender;
        _MailSender = NULL;
    }

    DIAGC("msgInfo", 5, "Exit storeSender");
    return retCode;
}

//+---------------------------------------------------------------------------
//
// Method:    MsgInfo::_storeRecipients
//
// Synopsis:  Create Recipient object and store it in the vector.
//
//----------------------------------------------------------------------------
SymantecCode MsgInfo::_storeRecipients(void)
{
    DIAGC("msgInfo", 5, "Entering storeRecipients");
    SymantecCode retCode = SymantecSuccess;

    for(unsigned int idx=0;  idx != _rcptToCount; ++idx) {
        Recipient* recip = NULL;
	
        try {
            recip = new Recipient( _mailgram, idx );
            _recipArray.push_back( recip );
        } catch(...) {
            LOG(Error, "AVExtServerDataReadFailed", "Not able to create Recipient objects");
            retCode = SymantecFailure;
            if ( recip)
                delete recip;
			
            // clean up previous object
            _cleanRecipArray();
            break;
        }
    }

    DIAGC("msgInfo", 5, "Exit storeRecipients");
    return retCode;
}

//+---------------------------------------------------------------------------
//
// Method:     MsgInfo::getRecipient
//
// Synopsis:   return recipient pointer stored at index
//
//----------------------------------------------------------------------------
Recipient* MsgInfo::getRecipient(int index)
{
    DIAGC("msgInfo", 5, "Entering getRecipient");
    try {
        if (index < int_cast(_recipArray.size()))
            return _recipArray.at(index);
    } catch(exception &ex) {
        LOG(Notification, "getRecipient exception ", "%s", ex.what());
    }
    return NULL;
}

//+---------------------------------------------------------------------------
//
// Method:    MsgInfo::buildMailContext
//
// Synopsis:  This is be used to log some mail information, viz MsgID,
//            MailFrom, Peer IP.
//
//----------------------------------------------------------------------------
void MsgInfo::_buildMailContext()
{
    _mailContext  = " Received from=" + _peerIP;
    _mailContext += ";msgid="  + _msgID;
    _mailContext += ";sender=" + SymString(getMailFrom()->str, getMailFrom()->len);
    _mailContext += ";rcpts="  + SymString(getRecipientList());

    DIAGC("SymantecMsgInfo", 1, "MailContext=%s", _mailContext.AsPtr());
}

//+---------------------------------------------------------------------------
//
// Method:    MsgInfo::getTotalTime
//
// Synopsis:  return total time taken for processing in milliseconds
//----------------------------------------------------------------------------
const char* MsgInfo::getTotalTime()
{
    hrtime_t currTime = MethodTimingSnapShot::getRealTimeClock();

    float duration = currTime - _initTime;
    duration /=  1000.0f;
    sprintf(_TimeBuffer,"time_ms=%5.3f", duration ); // Scale to ms.
    _fTimeConsume = duration;
    return _TimeBuffer;
}

//+---------------------------------------------------------------------------
//
// Method:    MsgInfo::setVirusName
//
// Synopsis:  sets name of the virus.
//
//----------------------------------------------------------------------------
void MsgInfo::setVirusName(string& name)
{
    if (_virusname.size()) _virusname.append("; ");
    _virusname.append(name);
}


//+---------------------------------------------------------------------------
//
// Method:    MsgInfo::setProblemDetails
//
// Synopsis:  sets virus name, file name and problem string
//
//----------------------------------------------------------------------------
void MsgInfo::setProblemDetails(string virusName, string fileName)
{
    // set virus name
    setVirusName(virusName);
    // set file name
    setFileName(fileName.c_str());
    // create problem string and set it
    if (_problemDetails.size()) _problemDetails.append("; ");
   
    _problemDetails.append(fileName.c_str());
    _problemDetails.append(" (");
    _problemDetails.append(virusName.c_str());
    _problemDetails.append(") ");
}

//+---------------------------------------------------------------------------
//
// Method:    MsgInfo::setSymantecVerdict
//
// Synopsis:  sets scan verdict and flag to indicate part of message suspecious.
//
//----------------------------------------------------------------------------
void MsgInfo::setSymantecVerdict(SymantecVerdict verdict, bool scanSet)
{
    _virusVerdict = verdict;

    // set part of a message is suspect, 
    // it helps in 'removedAllMessageParts' config. make effective
    if (verdict == SymantecVerdictSuspectNoRepaired ||
        verdict == SymantecVerdictSuspectRepaired) {
        _bMsgPartSuspect = true;
    }
}


//+---------------------------------------------------------------------------
//
// Method:    MsgInfo::clearSuspectMsgPartFlag
//
// Synopsis:  Clear flag before repair or clean process start.
//
//----------------------------------------------------------------------------
void MsgInfo::clearSuspectMsgPartFlag()
{
    _bMsgPartSuspect = false;
}


//+---------------------------------------------------------------------------
//
// Method:    MsgInfo::setSymantecActionType
//
// Synopsis:  set action type
//
//----------------------------------------------------------------------------
void MsgInfo::setSymantecActionType(SymantecActionBase symActionType)
{
    _symantecActionType = symActionType;	
}

//+---------------------------------------------------------------------------
//
// Method:    MsgInfo::getRecipientList
//
// Synopsis:  construct a string containing all recipient addresses
//
//----------------------------------------------------------------------------
const char* MsgInfo::getRecipientList()
{
    // Build recipient list if requested
    if ( Len(_recipientList) == 0) {

        for (int idx=0; idx != int_cast(_rcptToCount); ++idx) {

            const StringInfo* tmp = getRecipient(idx)->getAddress();
            SymString strNoAngleRecipient(tmp->str, tmp->len );
            if (idx) {
                _recipientList += ", ";
            }
            _recipientList += strNoAngleRecipient;
        }

        DIAGC("msgInfo", 4, "RecipientList=%s", _recipientList.AsPtr());
    }

    return _recipientList;
}

//+---------------------------------------------------------------------------
//
// Method:     MsgInfo::readFromHeader
//
// Synopsis:   returns the string from header between start and end tokens
//
// Inputs:     Start token and end token
//
//----------------------------------------------------------------------------
SymString MsgInfo::readFromHeader(const char* cStart, const char* cEnd, int length)
{
    SymString strHeader(getHeader()->str, getHeader()->len);
    SymString newStrHeader;

    if ( length ==0 || length > Len(strHeader))
        newStrHeader = strHeader;
    else
        newStrHeader = strHeader.Segment(0, length);

    return _searchSString(newStrHeader, cStart, cEnd);
}

//+---------------------------------------------------------------------------
//
// Method:     MsgInfo::readFromHeader
//
// Synopsis:   returns the string from header "fldname" field.
//
// Inputs:     field name and ':'.  e.g. "Subject:"
//             should decode or not
//             will be used as a perl string or no
//
//----------------------------------------------------------------------------
SymString MsgInfo::readFromHeader(const char* fldname, bool decode, bool perl)
{
    char* start = (char*)strstr(getHeader()->str, fldname);
	
    if (!start)
        return SymString();

    start += strlen(fldname);

    SymString line;
    char* end;

    do {
        end = strchr(start, '\n');
		
        if (!end)
            end = start + strlen(start);
		
        line += SymString(start, end - start).Trim();
        start = end + 1;

    } while (*end && *start && isspace(*start));

    line = escape(line, perl);

    return line;
}

//+---------------------------------------------------------------------------
//
// Method:     MsgInfo::_searchSString
//
// Synopsis:   returns the string between start and end tokens
//
// Inputs:     Start token and end token
//
//----------------------------------------------------------------------------
SymString MsgInfo::_searchSString(const SymString& str, const char* cStart, const char* cEnd)
{
    SymString newStr;
    int iStart, iEnd;
    if ((iStart = str.Index(cStart)) >= 0 &&
        (iEnd = str.Index(cEnd, iStart)) >= 0 ) {

         iStart = iStart + int_cast(strlen(cStart)) - 1;
         newStr = str.Segment(iStart, iEnd - iStart + 1);
    }

    return newStr;
}

//+---------------------------------------------------------------------------
//
// Method:    MsgInfo::_createSMappedFileWrite
//
// Synopsis:  Creates SMappedFileWrite for header and body
//
//----------------------------------------------------------------------------
SymantecCode MsgInfo::_createSMappedFileWrite(SMappedFileWrite*& sfw,
                                              const char* fileExtn,
                                              const StringInfo* strInfo)
{

    DIAGC("msgInfo", 5,  "Creating %s SMappedFileWrite", fileExtn);

    try {

        sfw = new SMappedFileWrite();
        char* filename = mkTempName( fileExtn, NULL, true);
        sfw->CreateCached( filename );
        delete []filename;
        sfw->Write( strInfo->str, strInfo->len);

    } catch(...) {

        DIAGC("msgInfo", 5,  "Exception in SMappedFileW");
        delete sfw;
        sfw = NULL;
        return SymantecFailure;

    }
    
    DIAGC("msgInfo", 5,  "Exit");
    return SymantecSuccess;
}

//+---------------------------------------------------------------------------
//
// Method:    MsgInfo::getSMappedHeader
//
// Synopsis:  returns SMappedFileWrite for Mail Header
//
//----------------------------------------------------------------------------
SMappedFileWrite* MsgInfo::getSMappedHeader()
{
    if ( _sfwHeader == NULL &&
          (_createSMappedFileWrite(_sfwHeader, ".Header", getHeader()) != SymantecSuccess))
    {
        if (!symantecApp->isMsgcatLoggingEnabled())
          REPORT_SymnAVFioCreateFail(SLOGHANDLE, Urgent, "Cached Header file");
        else
          LOG(Urgent, "FioCreateFail",
                    "Not able to create Cached Header file");
        return NULL;
    }

    return _sfwHeader;
}

//+---------------------------------------------------------------------------
//
// Method:    MsgInfo::getSMappedBody
//
// Synopsis:  returns SMappedFileWrite for Mail Body
//
//----------------------------------------------------------------------------
SMappedFileWrite* MsgInfo::getSMappedBody()
{
    if ( _sfwBody == NULL &&
          (_createSMappedFileWrite(_sfwBody, ".Body", getBody()) != SymantecSuccess))
    {
        if (!symantecApp->isMsgcatLoggingEnabled())
          REPORT_SymnAVFioCreateFail(SLOGHANDLE, Urgent, "Cached Body file=");
        else
          LOG(Urgent, "FioCreateFail",
                    "Not able to create Cached Body file");
        return NULL;
    }

    return _sfwBody;

}

bool MsgInfo::isMsgWithAttachment(const StringInfo* strHeader, const StringInfo* strBody)
{
    DIAGC("msgInfo", 5,  "Entry isMsgWithAttachment");
    bool attachFile = false;

    // Construct parser,
    Parser* parser = new Parser(strHeader->str, strBody->str, strHeader->len, strBody->len);

    if (!parser) return false;

    // Get mime entity, root entity
    Entity *mimeEntity = parser->getParsedMessage();
    
    // CHILD ENTITY
    Entity* childEntity = mimeEntity->child();

    while (childEntity != NULL) {
        
        Entity* deepChild = childEntity->child();
        int type = 0;

        while (deepChild != NULL) {
            
            attachFile = findInfo(deepChild, type);

            if (attachFile) {
                DIAGC("msgInfo", 5, "Attachment Found!");
                delete parser;
                return true;
            } else DIAGC("msgInfo", 5, "No Attachment!");

            deepChild = deepChild->child();
        }
        
        attachFile = findInfo(childEntity, type);
        if (attachFile) {
            DIAGC("msgInfo", 5, "Attachment Found!");
            delete parser;
            return true;
        } else DIAGC("msgInfo", 5, "No Attachment!");
        
        childEntity = childEntity->next();
    }

    delete parser;
    DIAGC("msgInfo", 5,  "Exit isMsgWithAttachment");
    return false;
}


bool MsgInfo::findInfo(Entity* childEntity, int& type)
{
    bool attach = false;

    // check for attachment
    HeaderField* hfd = childEntity->findHeader("Content-Disposition");
    if (hfd != NULL) {

        // check if it's type attachment
        char* cntTyp = new char[100];
        int lenNeed = 0;
        hfd->value(cntTyp, 100, lenNeed);
        if (strstr(cntTyp, "attachment")) {

            attach = true;
        }
        delete []cntTyp;
    }

    return attach;
}

//+---------------------------------------------------------------------------
//
// Method:     escape
//
// Synopsis:   optionally escape all '$; or '\\' chars
//             in input/output string
//
//             line: reference to string to escape
//             perl: option to escape $ or \\ chars,
//                   as required by perl
//----------------------------------------------------------------------------
SymString escape(const SymString& line, bool perl)
{
    const char* sub = line.AsPtr();
    SymString value;
    while (*sub) {
        if (perl && *sub == '$')
            value += '$';
        if (perl && *sub == '\\')
            value += '\\';
        value += *(sub++);
    }
    return value;
}

//+---------------------------------------------------------------------------
// The new config API's Register and Unregister function only work when,
// imconfserv is running, when we run the unit test imconfserv will not be
// running, so during UnregisterConfigKey unit test cores.
// As a workaround, the functions getSymantecTmpDir()
// return Config key values when symantecApp object is
// initialized (when imextserv loads symantec extsvc), or else it will return
// the default value.
//+---------------------------------------------------------------------------

CFG tmpDirParm("tmpDir", "tmp", "", EXTN_SERVICE, SCfg, CFG_SERV_RESTART);

char* getSymantecTmpDir(char * buf, int bufsize)
{
    if (symantecApp != NULL) 
        return tmpDirParm.getPath(buf, bufsize - 1);

    // if symantecApplication is not initialized then
    // return default value.
    memset(buf, 0, sizeof(bufsize));
    snprintf(buf, bufsize -1, "tmp");

    return buf;
}

// used by mkTempName()
static char ourFilePrefix[40];
static pid_t pid = 0;
static int   tmpDirLen;
static char* tmpDir = NULL;

//+---------------------------------------------------------------------------
// Synopsis:    Create a unique file name in our tmp directory, ext must
//              either be NULL or point to a string that starts with ".".
//              IM-<pid>-Symantec-<num>.<ext> file will be created
//+---------------------------------------------------------------------------
char* mkTempName(const char *ext, char *pathBuffer, bool useRunDir)
{
    static unsigned long fNum = 1;

    if (pid == 0) {
        pid = getpid();
        snprintf(ourFilePrefix, sizeof(ourFilePrefix), "IM-%d-Symantec-", pid);
    }

    if (tmpDir == NULL) {
        char buf[MAX_PATH_NAME];

        getSymantecTmpDir(buf, MAX_PATH_NAME);

        tmpDir = strdup(buf);

        tmpDirLen = int_cast(strlen(tmpDir));
    }

    // Ultimately, MAX_PATH_NAME can be quite large (4K on Linux!) so
    // don't use it here to size the final buffer. Too much waste. The
    // 32 character fudge accounts for elements of the path whose size we
    // cannot pre-determine (like fNum).
    size_t pathnameLen = tmpDirLen + strlen(ourFilePrefix) +
                      (ext ? strlen(ext) : 0) + 32;

    if (pathBuffer == NULL) {
        pathBuffer = new char[pathnameLen];
    }

    snprintf(pathBuffer, pathnameLen, "%s/%s%lu%s",
             tmpDir, ourFilePrefix, fNum++, (ext ? ext : ""));

    return pathBuffer;
}


}
