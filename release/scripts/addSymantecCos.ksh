#!/bin/ksh

FILENAME=./tmpSymantec.ldif
\rm -f ${FILENAME}

HOST=`cat $INTERMAIL/config/hostname`

IMCONFGET="imconfget -localconfig -s -h $HOST "

eval `$IMCONFGET ldapRootDn`
eval `$IMCONFGET ldapRootPwd`
eval `$IMCONFGET -m imdirserv ldapPort`


RESULT=`ldapsearch -D $ldapRootDn -w $ldapRootPwd -h $HOST -p $ldapPort -b 'cn=schema' -s base 'objectclass=*' objectclasses | \grep -i mailuserprefs |grep -i mailsymnavOutboundVirusAction | wc -l `

if [  "$RESULT" -eq 0 ]; then
    :
else
    echo "Symantec attributes already added. Skipping......"
    return 0;
fi



OLD_DEFINITION=`ldapsearch -D $ldapRootDn -w $ldapRootPwd -h $HOST -p $ldapPort -b 'cn=schema' -s base 'objectclass=*' objectclasses | \grep -i mailuserprefs | cut -d '=' -f2 | cut -d ')' -f1 `

cat > ${FILENAME} << _END_
##
## Add the Symantec attribute types to the LDAP schema
##
dn: cn=schema
changeType: modify
add: attributeTypes
attributetypes: ( 1.3.6.1.4.1.2415.1000.1.3.85
    NAME 'mailsymnavInboundVirusAction'
    EQUALITY caseIgnoreMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.26 SINGLE-VALUE )
attributetypes: ( 1.3.6.1.4.1.2415.1000.1.3.86
    NAME 'mailsymnavOutboundVirusAction'
    EQUALITY caseIgnoreMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.26 SINGLE-VALUE )


##
## Add the Symantec COS attributes to the mailLocalUserPrefs objectclass
##
## Note that this is an example that will need to be modified for each
## customer. The '<old definition>' is a list of the attributes in the
## mailLocalUserPrefs objectclass.
##
## It is also up to the customer to determine what objectclass to add the
## new attributes to. It is strongly recommended not to add them to
## mailUserPrefs since this will lead to failures when applying schema
## patches in the future
##
dn: cn=schema
changeType: modify
delete: objectClasses
objectclasses: $OLD_DEFINITION ) )
-
objectclasses: $OLD_DEFINITION
     $ mailsymnavInboundVirusAction $ mailsymnavOutboundVirusAction ) )

##
##
## Add modify the default COS to include Symantec access
##
## Note that this is an example. Each customer needs to determine what
## COSs get the Symantec attributes with what default values.
##
dn: cn=default,cn=admin root
changeType: modify
add: mailsymnavInboundVirusAction
mailsymnavInboundVirusAction: Allow
-
add: mailsymnavOutboundVirusAction
mailsymnavOutboundVirusAction: Allow

_END_

ldapmodify -D $ldapRootDn -w $ldapRootPwd -h $HOST -p $ldapPort -f ${FILENAME}


\rm -f ${FILENAME}

