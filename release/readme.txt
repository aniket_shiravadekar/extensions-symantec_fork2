
This document describes the operation of 'impatch', a program used to
install and uninstall InterMail patches.  An InterMail patch is a set
of files and procedures used to update an existing InterMail
installation. A patch is delivered as a single TAR file. Contained
within this file are all the files necessary to complete the
upgrade.

There are a few requirements for impatch to function properly, so
please read this document carefully.


Usage and Requirement Summary
-----------------------------

usage: ./impatch [ -h ] [ -u ] [ -v ] [-s Yes|No] [ -d ] 

        -h   Print this usage statement
        -u   Uninstall a patch
        -v   Print version of impatch
        -d   Enable debug messages
        -sX  shutdown InterMail prior to (un)installation (X=Yes/No)


1.  Impatch must be run as the 'root' user

2.  The $INTERMAIL environment variable must be set properly

3.  Impatch is meant to be executed from the directory in which it
    resides

    If installing a patch, impatch and the patch data will be delivered
    together in a single TAR file.  Extracting the TAR file will create
    a new directory which contains the patch data and the impatch
    program. The name of the new directory is the same as the name of
    the patch.

    If uninstalling a patch, impatch should be run from the
    $INTERMAIL/patch/<patch_name> directory. This directory is created
    when a patch is successfully installed. The version of impatch used
    to install the patch is also saved here and should be used to
    uninstall the patch.

4.  If more than one patch is to be installed, each patch must be
    installed in the correct order. See the release notes supplied with
    the patch for dependencies and patch order.

5.  Patches should be uninstalled in the reverse order they were installed.
    This is necessary to ensure the state of the InterMail installation
    is restored properly. A history of installed patches is saved
    in the $INTERMAIL/patch directory.

6.  InterMail should normally be shutdown before installing or
    uninstalling patches.


 
Operation (Installation)
------------------------

A patch is delivered as a TAR file (and possibly compressed with the
UNIX 'compress' utility). Extract the TAR file in a temporary scratch
area with sufficient disk space. The recommended directory is /var/tmp.
The result is a new directory containing the impatch program and the
patch data itself.

This directory will resemble the following format:

    <patch_name>/       Directory after extracting TAR file
      impatch           The impatch program
      README            This file
      manifest          The complete file list for this release
      preop             List of procedures to run before installation
      postop            List of procedures to run after installation
      patch/            Directory of files to be patched
      scripts/          Directory of pre- and post- procedures
    
As the root user, switch to this new directory, set the INTERMAIL
environment variable run the impatch program. 

Impatch first consults the $INTERMAIL/patch/patch_history file in
order to warn the user of any obvious misapplications of the patch.

A new directory named $INTERMAIL/patch/<patch_name> is created.  Any
files replaced during the upgrade are stored here in the event that the
patch is later uninstalled. The log file, and the impatch program
itself are also stored here. All these files are required to properly
restore the system to the state prior to the patch install.

This directory will resemble the following format:

    $INTERMAIL/patch/<patch_name>/
      impatch
      install.log
      <saved dirs>/

Impatch executes a series of pre-installation procedures as listed in
the 'preop' file. If any of the preop scripts fail, then impatch exits.
At this point, no files have been installed, but the pre-installation
scripts may have modified the InterMail environment. Refer to the 
'install.log' file for indications of the problem.

Impatch recurses the 'patch' directory and copies each file into it's
proper location within the InterMail installation.  If a file from the
patch replaces an existing file, then the original file is saved
first. The 'install.log' file is updated as each directory and
file is created or replaced during the install process.

Impatch executes a series of post-installation procedures as
listed in the 'postop' file much like it did with the pre-installation
procedures. Should any of these procedures fail, processing
continues, but the 'install.log' file should be consulted and
the problem addressed.

The checksum of each new or replaced file is computed and
stored in the 'install.log' file.

Finally, the 'patch_history' file is updated to reflect the new
installation.


Operation (Uninstallation)
--------------------------

During installation, impatch saved each replaced file, as well as a
copy of the impatch program itself, in a directory named
$INTERMAIL/patch/<patch_name>.

As the root user, switch to this this directory, set the INTERMAIL
environment variable run the impatch program.

Impatch first consults the $INTERMAIL/patch/patch_history file and will
issue a warning if the patch is being removed in the wrong order.

A log file is created $INTERMAIL/patch/uninstall.log and all operations
are posted here.

The 'install.log' file contains a list of file operations used during
the installation of the patch. This information is now used to replace
the original files and remove any newly added files and directories.

Finally, the 'patch_history' file is updated to reflect the removal of
the patch.


Special Handling
----------------

Impatch is designed to preserve the InterMail installation.
All replaced files are first saved in a backup directory as
discussed above. There are also a few special cases that the
should be aware of.

1.  During a patch installation, if a file being replaced is already
    present in the backup directory $INTERMAIL/patch/<patch_name>,
    then the file is NOT backed up again. It is assumed this patch
    has already been installed before and the backup succeeded
    the first time.

2.  For patch removal, each file replaced during installation is
    restored. Prior to the restoring the original, impatch checks if
    the installed file has changed since it was installed.  If so, the
    file is renamed to <filename>.<number>.sav and left in the same
    directory as the original file (in the InterMail tree). It is
    assumed the file was modified in some way and the changes should
    not be lost.


Notes
-----

The following environment variables are available to the pre- and
post-op commands.

    INTERMAIL           : the INTERMAIL environment variable
    PATCH_COMMON_GID    : the numeric UNIX GID for the InterMail user
    PATCH_COMMON_GROUP  : the UNIX groupname for the InterMail user
    PATCH_COMMON_UID    : the numeric UNIX UID for the InterMail user
    PATCH_COMMON_USER   : the UNIX username for the InterMail user
    PATCH_NAME          : the name of the patch being installed
    PATCH_PGM           : the name of the patch program
    PATCH_PRODUCT       : the name of the product being installed
    PATCH_SAVE_DIR      : the patch archive directory
    PATCH_SRC_DIR       : the patch source directory
    PATCH_VERSION       : the version of the patch program
    PATH                : the PATH environment variable
 
Also, within the 'preop' and 'postop' files, you can use
these variables. For example, a line in the preop file could
be:

    checkLicense $INTERMAIL/license/all.lic


