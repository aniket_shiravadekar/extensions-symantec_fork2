//----------------------------------------------------------------------------
//
// File Name:    symantecapp.hxx
//
// Synopsis:     SymantecConfig and SymantecApplication class implementation
//----------------------------------------------------------------------------
// Copyright 2017 Synchronoss, Inc. All Rights Reserved.
//
// The copyright to the computer software herein is the property of
// Synchronoss, Inc. The software may be used and/or copied only with
// The written permission of Synchronoss, Inc. or in accordance with the
// terms and conditions stipulated in the agreement/contract under which
// the software has been supplied.
//----------------------------------------------------------------------------

#ifndef __SYMANTECAPP_H__
#define __SYMANTECAPP_H__

#include <sys/socket.h>
#include <sys/ioctl.h>
#include <linux/netdevice.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <unistd.h>

#include <iostream>
#include <map>

#include <extapi.hxx>

#include "syutils.hxx"
#include "symantecscan.hxx"
#include "symconstants.hxx"

using namespace std;
using namespace symantecscan;

class SymantecConfig
{
  public:
    // construct config instance
    inline static SymantecConfig* getInstance()
    {
        DIAGC("symantecConfig", 5, "Entering getInstance");
        if (!_pSymantecConfig) {
            _pSymantecConfig = new SymantecConfig();
        }

        return _pSymantecConfig;
    }

    // release config instance
    static void releaseInstance()
    {
        if (_pSymantecConfig)
            delete _pSymantecConfig;
    }

    void populateCfgArray();
    string getValidatedLongStr(string& str);

    // getter for symantec specific config keys
    inline const char* getSymantecInboundAction() { 
        return _strInboundAction.c_str(); 
    }
    inline const char* getSymantecOutboundAction() { 
        return _strOutboundAction.c_str(); 
    }
    inline const char* getSymantecServerConnection() { 
        return _strSymantecServerConnection.c_str(); 
    }
    inline const char* getSymAVNoticeBodySender() { 
        return _strSymAVNoticeBodySender.c_str(); 
    }
    inline const char* getSymAVNoticeHeaderSender() { 
        return _strSymAVNoticeHeaderSender.c_str(); 
    }
    inline const char* getSymTimeoutSecs() { 
        return _strSymantecTimeoutSecs.c_str(); 
    } 
    inline const char* getSymDelAttachNotice() { 
        return _strSymDelAttachNotice.c_str(); 
    }
    inline const char* getSymDelPlainTextNotice() { 
        return _strSymDelPlainTextNotice.c_str(); 
    }
    inline const char* getSymTimeoutAction() { 
        return _strSymantecTimeoutAction.c_str(); 
    }
    inline const char* getSymVirusScanFrm() {
        return _strSymVirusScanFrm.c_str();
    }
    inline const char* getSymVirusNotice() {
        return _strSymVirusNotice.c_str();
    }
    inline const char* getSymVirusNoticeBodyRecip() {
        return _strSymVirusNoticeBodyRecip.c_str();
    }
    inline const char* getSymVirusNoticeHeaderRecip() {
        return _strSymVirusNoticeHeaderRecip.c_str();
    }
    inline const char* getSymRepairedAttachNotice() {
        return _strSymRepairedAttachNotice.c_str();
    }
    inline const char* getSymRepairedPlainTextNotice() {
        return _strSymRepairedPlainTextNotice.c_str();
    }
    inline const char* getSymAVScanByMsgType() {
    	return _strSymAVScanByMsgType.c_str();
    }
    inline const char* getSymAVScanFailedAction() {
        return _strSymAVScanFailedAction.c_str();
    }
    inline const char* getSymAVScanFailNoticeBody() {
        return _strSymAVScanFailNoticeBody.c_str();
    }
    inline const char* getSymAVScanFailNoticeHeader() {
        return _strSymAVScanFailNoticeHeader.c_str();
    }
    inline const char* getSymSpeRetryInterval() {
        return _strSymantecSpeRetryInterval.c_str();
    }
    inline const char* getSymAVVersion() {
        return _strSymnAVVersion.c_str();
    }    
    inline bool getSymMsgCatLogFlg() { return _bSymMsgCatLog; }
    inline bool getSymCoSAuthFlg() { return _bSymCoSAuth; }
    inline long getSymTimeoutLimitSecs() { return _nSymTimeout; }
    inline long getSymServerConnRetry() { return _nSymServerRetry; }
    inline long getSymSpeRetryIntervalLimit() { return _nSymSpeRetry; }
   
    inline const char* getSymRemAllMsgPartsNotice() {
        return _strSymRemAllMsgPartsNotice.c_str();
    }
    inline bool getDisbBase64Decode() { return _bSymDisbBase64Decode; }
    inline bool getRescanSuspecAttach() { return _bRescanSuspecAttach; }
    inline bool getScanAndClean() { return _bScanAndClean; }
    inline bool getRemAllMsgParts() { return _bRemAllMsgParts; }
    inline bool getRescanUUEncode() { return _bRescanUUEncode; }
    inline bool getReportDefInfoEnabled() { return _bReportDefInfo; }
    inline bool getICAPHttpClientIPEnabled() { return _bICAPHttpClientIP; }

  private:
    // Private constructor
    SymantecConfig()
    {
        DIAGC("symantecConfig", 5, "Entering ctor SymantecConfig"); 

        try {
            // Populate config key array
            populateCfgArray();
        } catch (exception &ex) {
            LOG(Notification, "SymantecConfig Exception :", "%s", ex.what());
        }

        DIAGC("symantecConfig", 5, "Exit ctor SymantecConfig");
    }

    // destructor
    ~SymantecConfig()
    {
       for (int i =0; i < int_cast(_arrSymConfg.size()); i++)
           delete _arrSymConfg[i];
    }

    // setters for symantec config keys
    void _setSymAction(MxSConfig*);
    void _setSymTimeoutSecs(MxSConfig*);
    void _setSymServerConn(MxSConfig*);
    void _setSymAVNoticeBodySender(MxSConfig*);
    void _setSymAVNoticeHeaderSender(MxSConfig*);
    void _setSymMsgCatLogging(MxSConfig*);
    void _setSymCosAuth(MxSConfig*);
    void _setSymOutboundAction(MxSConfig*);
    void _setSymAVScanByMsgType(MxSConfig*);
    void _setSymAVScanFailedAction(MxSConfig*);
    void _setSymAVScanFailNoticeBody(MxSConfig*);
    void _setSymAVScanFailNoticeHeader(MxSConfig*);

    void _setSymDelAttachNotice(MxSConfig*);
    void _setSymDelPlainTextNotice(MxSConfig*);
    void _setSymTimeoutAction(MxSConfig*);
    void _setSymVirusScanFrm(MxSConfig*);
    void _setSymVirusNotice(MxSConfig*);
    void _setSymVirusNoticeBodyRecip(MxSConfig*);
    void _setSymVirusNoticeHeaderRecip(MxSConfig*);
    void _setSymServerConnRetry(MxSConfig*);
    void _setSymRepairedAttachNotice(MxSConfig*);
    void _setSymRepairedPlainTexNotice(MxSConfig*);
    void _setSymSpeRetryInterval(MxSConfig*);
    void _setSymnAVVersion(MxSConfig*);

    void _setSymDisBase64Decode(MxSConfig*);
    void _setSymRescanSuspectAttach(MxSConfig*);
    void _setSymnScanAndClean(MxSConfig*);
    void _setSymnRemAllMsgParts(MxSConfig*);
    void _setSymnRemAllMsgPartsNotice(MxSConfig*);
    void _setSymnRescanUUEncode(MxSConfig*);
    void _setSymnReportDefInfoEnabled(MxSConfig*);
    void _setSymnICAPHttpClientIPEnable(MxSConfig*);

  private:

    // single instance of the config
    static SymantecConfig* _pSymantecConfig;

    vector<MxSConfig*> _arrSymConfg;

    long _nSymTimeout;
    long _nSymServerRetry;
    long _nSymSpeRetry;

    bool _bRescanUUEncode;
    bool _bRemAllMsgParts;
    bool _bScanAndClean;
    bool _bRescanSuspecAttach;
    bool _bSymDisbBase64Decode;
    bool _bSymMsgCatLog;
    bool _bSymCoSAuth;
    bool _bReportDefInfo;
    bool _bICAPHttpClientIP;

    string _strInboundAction;	
    string _strSymantecTimeoutSecs;
    string _strSymAVNoticeHeaderSender;
    string _strSymAVNoticeBodySender;
    string _strSymantecServerConnection;
    string _strSymMsgCatLog;
    string _strSymCoSAuth;
    string _strOutboundAction;
    string _strSymantecTimeoutAction;
    string _strSymDelAttachNotice;
    string _strSymDelPlainTextNotice;
    string _strSymVirusScanFrm;
    string _strSymVirusNotice;
    string _strSymVirusNoticeBodyRecip;
    string _strSymVirusNoticeHeaderRecip;
    string _strSymRepairedAttachNotice;
    string _strSymRepairedPlainTextNotice;
    string _strSymRemAllMsgPartsNotice;
    string _strSymAVScanByMsgType;
    string _strSymAVScanFailedAction;
    string _strSymAVScanFailNoticeBody;
    string _strSymAVScanFailNoticeHeader;
    string _strSymantecSpeRetryInterval;
    string _strSymantecServerRetry;
    string _strSymnAVVersion;
};


//+---------------------------------------------------------------------------
// Synopsis: This is the first object created when symantecscan.so file loaded
//           by imextserv. It create log object, initialize SymantecScanner.
//           This object will be destructed when imextserv shut down.
//---------------------------------------------------------------------------
class SymantecApplication
{
  public:
    // This function should called before SymantecApplication constructor
    static void initStat();

    void Initialize();
    void Finalize();
	
    // Constructor
    SymantecApplication(const char* prgName);
    ~SymantecApplication();

    // Getter for data members
    inline SymantecScanner* getSymantecScanner() { return _pSymantecScanner; }

    inline bool isAppInitialized() { return _bApplicationInitialized; }	
    inline bool isSenderNoticeEnabled() { return _bSenderNotice; }
    inline bool isRecipientNoticeEnabled() { return _bRecipientNotice; }
    inline bool isRemoteRecipientNoticeEnabled() { 
        return _bRemoteRecipientNotice; 
    }
    inline bool isSenderOptInNotice() { return _bSenderOptInNotice; }

    StringInfo* getDisposition(SymantecAction symAction);
    SymantecAction getSymantecAction(string symAction);

    inline const char* getHostIP() {
        return _strHostIP.c_str();
    }
   
    // Getters for config keys
    inline const char* getSymantecInboundAction() { 
        return _pSymantecConfig->getSymantecInboundAction(); 
    }
    inline const char* getSymantecOutboundAction() { 
        return _pSymantecConfig->getSymantecOutboundAction(); 
    }
    inline const char* getSymantecServerConnection() { 
        return _pSymantecConfig->getSymantecServerConnection(); 
    }
    inline const char* getSymAVNoticeHeaderSender() { 
        return _pSymantecConfig->getSymAVNoticeHeaderSender(); 
    }
    inline const char* getSymAVNoticeBodySender() { 
        return _pSymantecConfig->getSymAVNoticeBodySender(); 
    }
    inline const char* getSymTimeoutSecs() { 
        return _pSymantecConfig->getSymTimeoutSecs(); 
    }
    inline const char* getSymDelAttachNotice() { 
        return _pSymantecConfig->getSymDelAttachNotice(); 
    }
    inline const char* getSymDelPlainTextNotice() { 
        return _pSymantecConfig->getSymDelPlainTextNotice(); 
    }
    inline const char* getSymTimeoutAction() {
        return _pSymantecConfig->getSymTimeoutAction();
    }
    inline const char* getVirusNoticeFrom() {
        return _pSymantecConfig->getSymVirusScanFrm();
    }
    inline bool isMsgcatLoggingEnabled() { 
        return _pSymantecConfig->getSymMsgCatLogFlg(); 
    }
    inline bool isCOSAuthoritative() { 
        return _pSymantecConfig->getSymCoSAuthFlg(); 
    }
    inline const char* getSymVirusNotice() {
        return _pSymantecConfig->getSymVirusNotice();
    }
    inline const char* getSymVirusNoticeBodyRecip() {
        return _pSymantecConfig->getSymVirusNoticeBodyRecip();
    }
    inline const char* getSymVirusNoticeHeaderRecip() {
        return _pSymantecConfig->getSymVirusNoticeHeaderRecip();
    }
    inline const char* getXOpwvHeader() {
        return _XOpwvHeader.AsPtr();
    }
    inline long getSymTimeoutLimitSecs() {
        return _pSymantecConfig->getSymTimeoutLimitSecs();
    }
    inline long getSymServerConnRetry() {
        return _pSymantecConfig->getSymServerConnRetry();
    }
    inline const char* getSymRepairedAttachNotice() {
        return _pSymantecConfig->getSymRepairedAttachNotice();
    }
    inline const char* getSymRepairedPlainTextNotice() {
        return _pSymantecConfig->getSymRepairedPlainTextNotice();
    }
    inline const char* getSymRemAllMsgPartsNotice() {
        return _pSymantecConfig->getSymRemAllMsgPartsNotice();
    }
    inline bool isDisbBase64Decode() {
        return _bSymDisbBase64Decode;
    }
    inline bool isRescanSuspecAttach() {
        return _bRescanSuspecAttach;
    }
    inline bool isScanAndClean() {
        return _bScanAndClean;
    }
    inline bool isRemoveAllMsgParts() {
        return _bRemAllMsgParts;
    }
    inline bool isRescanUUEncode() {
        return _bRescanUUEncode;
    }
    inline const char* getSymAVScanByMsgType() {
    	return _pSymantecConfig->getSymAVScanByMsgType();
    }
    inline const char* getSymAVScanFailedAction() {
        return _pSymantecConfig->getSymAVScanFailedAction();
    }
    inline const char* getSymAVScanFailNoticeBody() {
        return _pSymantecConfig->getSymAVScanFailNoticeBody();
    }
    inline const char* getSymAVScanFailNoticeHeader() {
        return _pSymantecConfig->getSymAVScanFailNoticeHeader();
    }
    inline long getSymSpeRetryIntervalSecs() {
        return _lSpeRetryInterval;
    }
    inline bool isReportDefInfoEnabled() {
        return _bReportDefInfo;
    }
    inline bool isICAPHttpClientIPEnabled() {
        return _bICAPHttpClientIP;
    }
    inline const char* getSymSpeRetryInterval() {
        return _pSymantecConfig->getSymSpeRetryInterval();
    }
    inline const char* getSymAVVersion() {
        return _pSymantecConfig->getSymAVVersion();
    }

  private:
    // Setters for data members
    void _createDispHashTable();
    void _createSymantecActionHashTable();
    void _createSymantecVirusNoticeHashTable();
    void _addDisp(SymantecAction symAction, const char* disp);
    void _setVirusNotification();
    string _getExtServIP();

  public: 
    struct Compare
    {
        bool operator() ( const string &s1, const string& s2) {
            return strcasecmp(s1.c_str(), s2.c_str()) < 0;
        }
    };

    typedef map<string, SymantecAction, Compare> SymantecActionList;
    typedef map<string, SymantecVirusNoticeCosType, Compare> SymantecVirusNoticeList;
    typedef map<SymantecAction, StringInfo> DispositionHash;

    long nSymConnRetry;

  private:
    
    // Symantec config object
    SymantecConfig* _pSymantecConfig;

    // Symantec AV scanner	
    SymantecScanner*_pSymantecScanner;

    long _lSpeRetryInterval;

    DispositionHash _dispList;
    SymantecActionList _actionList;

    // data members 
    bool _bApplicationInitialized;
    bool _bSenderNotice;
    bool _bRecipientNotice;
    bool _bRemoteRecipientNotice;
    bool _bCOSAuthoritative;
    bool _bSenderOptInNotice;
    bool _bReportDefInfo;
    bool _bICAPHttpClientIP;

    // frequently required data members
    bool _bScanAndClean;
    bool _bRescanUUEncode;
    bool _bRemAllMsgParts;
    bool _bRescanSuspecAttach;
    bool _bSymDisbBase64Decode;

    string _strProgram;
    string _strHostIP;
    SymString _XOpwvHeader;
}; 

extern SymantecApplication* symantecApp;

#endif
