# Project release version
 # NOTE: Make sure there's no trailing ' ' (space) on the end of the VERS string
 # It currently (2000-07-30) breaks the non-solaris installer. (rthille)
 # OMU library version -
 # Installation must also be updated when OMUVER is changed

VERS="Symantec.1.0.0.0 250-158-20170201"

 # MX Dependency
 # The following 3 VER are used to check the minimum dependency
 # of InterMail library versions
OMUVER  = M.9.1.0.26
EXTVER  = M.9.1.0.26

 # Vendor string
VENDOR="Synchronoss Inc."
VENDORINITIALS=SNCR

SYMANTEC_SDK_VER = 7024
EXTSVC_VER = 1.0.0.0

