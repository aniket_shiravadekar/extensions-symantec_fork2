#+############################################################################
#
# File Name:      project.mk
#
# Synopsis:       coverage for the symantec extensions module.
#
# Notes:          This file sets up the defaults for everything built
#
##############################################################################

-include                        $(PROJECT_ROOT)/local_pre.mk

MPLATFORM := $(shell uname -s)
ifeq ($(MPLATFORM), Linux)
ifeq ($(BUILDING_64), 1)
    CONTRIB_PORT_NAME := linux64
else
    CONTRIB_PORT_NAME := linux32
endif
endif

SYMANTEC_LIBDIR           = $(CONTRIB_ROOT)/symantec7024/$(CONTRIB_PORT_NAME)/lib
SYMANTEC_INCDIR           = $(CONTRIB_ROOT)/symantec7024/$(CONTRIB_PORT_NAME)/include

MERC_INC                = $(OMU_ROOT)/include
OMU_DIR                 = $(OMU_ROOT)/omu
IM_DIR                  = $(API_ROOT)/c-api
MXSDK_DIR               = $(API_ROOT)/mxsdk
ES_DIR                  = $(EMAIL_ROOT)/ext/lib

XMIME_DIR               = $(CONTRIB_ROOT)/xmime-3.0/src

MX_INCLUDES             = -I$(IM_DIR) -I$(ES_DIR) -I$(MXSDK_DIR) -I$(XMIME_DIR)/xmime

EMAIL_INCLUDES          = -I$(MSGCAT_ROOT) -I$(MERC_INC) -I$(OMU_DIR)

# InterMail
LIB_IM                  = $(IM_DIR)/libim.so
IM_LIBS                 = $(IM_DIR)/libim.a
LIB_ES                  = $(IM_DIR)/libimext.so
ES_LIBS                 = $(ES_DIR)/libimext.a

LIB_OMU                 = $(OMU_DIR)/libomu.so
OMU_LIBS                = $(OMU_DIR)/libimcustom.a

MXSDK_LIB_STATIC        = $(MXSDK_DIR)/libmxsdk.a $(MXSDK_DIR)/libmime.a $(CONTRIB_ROOT)/xmime-3.0/src/libxmime.a 
SSL_LIB_STATIC          = $(CONTRIB_ROOT)/openssl-1.0.1s/lib/libssl.a $(CONTRIB_ROOT)/openssl-1.0.1s/lib/libcrypto.a
SYMANTEC_LIB            = -L$(SYMANTEC_LIBDIR) -lsymcsapi 
MXSDK_LIBS              = -L$(MXSDK_DIR) -lmime.1.0
LIB_MXSDK               = $(MXSDK_DIR)/libmime.1.0.so




MPLATFORM := $(shell uname -s)

ifeq ($(MPLATFORM), SunOS)
SHARED_LIB_FLAGS        :=      -G
SYMANTEC_DEFINES  = -DSUN
endif

ifeq ($(MPLATFORM), Linux)
SHARED_LIB_FLAGS        :=      -shared
SYMANTEC_DEFINES  = -DLINUX
endif

define GENERATE_TIMESTAMP
        $(RM) $@
        echo    'char ESSymantecVersionStart[] = "BEGIN Symantec VERSION DATA";' \
                'char ESSymantecVersionName[] = """"$(VERS)"";' \
                'char ESSymantecVersionMach[] = "$(BUILD_PLATFORM)";' \
                'char ESSymantecVersionDate[] = "$(shell date)";' \
                'char ESSymantecSDKVer[] = "$(SYMANTEC_SDK_VER)";' \
                'char ESSymantecVersionEnd[] = "END SYMANTEC VERSION DATA";' \
                'char ReleaseDataStart[] = "START RELEASE DATA";' \
                'char BLDVersionStart[] = "START BUILD VERSION DATA";' \
                'char BLDVersionPath[] = "build_root=$(ROOT)";' \
                'char BLDVersionPlat[] = "build_platform=$(BUILD_PLATFORM)";' \
                'char BLDVersionName[] = "build_name=$(BUILD_INFO)";' \
                'char BLDVersionNum[] = "build_branch=$(internalnum)";' \
                'char BLDVersionDate[] = "build_date=$(shell date)";' \
                'char BLDVersionInfo[] = "build_vers="""$(VERS)"";' \
                'char BLDSymantecSDK[] = "symantec_sdk_ver=$(SYMANTEC_SDK_VER)";' \
                'char BLDVersionEnd[] = "END BUILD VERSION DATA";' \
                'char ReleaseDataEnd[] = "END RELEASE DATA";' > $@
endef

