//----------------------------------------------------------------------------
//
// File Name:   syutils.cxx
//
// Synopsis:    object implementation
//----------------------------------------------------------------------------
// Copyright 2017 Synchronoss, Inc. All Rights Reserved.
//
// The copyright to the computer software herein is the property of
// Synchronoss, Inc. The software may be used and/or copied only with
// The written permission of Synchronoss, Inc. or in accordance with the
// terms and conditions stipulated in the agreement/contract under which
// the software has been supplied.
//----------------------------------------------------------------------------

#include "syutils.hxx"

namespace symantecscan {

//+---------------------------------------------------------------------------
// SymantecLog class implementation
//----------------------------------------------------------------------------
IM_FileInfo* SymantecLog::symantecLogHandle = NULL;
IM_FileInfo* SymantecLog::symantecTraceHandle = NULL;

//+---------------------------------------------------------------------------
//
// Method:    SymantecLog::initSymantecLogging
//
// Synopsis:  This function is called from Initialize(). It gives call to 
//            IM_Initxxx() function and initializes symantecLogHandle and 
//            symantecTraceHandle.
//	
//            This function should get called only once when the service library
//            is loaded, i.e. only in Initialize().	
//
//---------------------------------------------------------------------------
void SymantecLog::initSymantecLogging()
{
    int errCode;
    IM_Error imError;
    IM_FileInitArgs* fileInitArgs;

    IM_InitError(&imError);
    errCode = IM_InitFileInitArgs(&fileInitArgs, EXTN_SERVICE, &imError);
    IM_FreeError(&imError);

    IM_InitError(&imError);
    errCode = IM_InitLogFileWriter(&SymantecLog::symantecLogHandle, _moduleName.c_str(), fileInitArgs, &imError);
    IM_FreeError(&imError);

    IM_InitError(&imError);
    errCode = IM_InitTraceFileWriter(&SymantecLog::symantecTraceHandle, _moduleName.c_str(), fileInitArgs, &imError);
    IM_FreeError(&imError);
}

//+---------------------------------------------------------------------------
//
// Method:    SymantecLog::freeSymantecLogging
//
// Synopsis:  This function is called from Finalize(). It gives call to 
//            IM_Freexxx() function and free symantecLogHandle and 
//            symantecTraceHandle.
//
//            This function should get called only once when the service library
//            is unloaded, i.e. only in Finalize().	
//
//---------------------------------------------------------------------------
void SymantecLog::freeSymantecLogging()
{
    int errCode;
    IM_Error imError;

    IM_InitError(&imError);
    errCode = IM_FreeLogFileWriter(SymantecLog::symantecLogHandle, &imError);
    IM_FreeError(&imError);

    IM_InitError(&imError);
    errCode = IM_FreeTraceFileWriter(SymantecLog::symantecTraceHandle, &imError);
    IM_FreeError(&imError);
}

//+---------------------------------------------------------------------------
//
// Method:    SymantecLog::setMainLogEvent
//
// Synopsis: This function called from Initialize() to set the main log event.
//
//---------------------------------------------------------------------------
void SymantecLog::setMainLogEvent(const char* logEvent, IM_MsgId msgId)
{
    IM_Error imError;
    IM_InitError(&imError);

    // errors may occured related to arguments, error can ignore
    IM_SetLogEvent(SymantecLog::symantecLogHandle, logEvent, msgId, &imError);

    IM_FreeError(&imError); 
}

//+---------------------------------------------------------------------------
// MxSConfig class implementation
//----------------------------------------------------------------------------

//value_type= string
MxSConfig::MxSConfig(const char* keyName, const char* defValue, const char* host, 
              const char* prog, SConfigType val, IM_IMPACT impact)
{
    addConfigKey(EXTN_SERVICE, keyName, defValue, val, impact);

    key = keyName;
    defaultval = defValue;
}

//value_type=boolean
MxSConfig::MxSConfig(const char* keyName, bool defValue, const char* host, 
              const char* prog, SConfigType val, IM_IMPACT impact)
{
    addConfigKey(EXTN_SERVICE, keyName, defValue ? "true" : "false", val, impact);
    key = keyName;
    defaultval = defValue;

}

//value type=long 
MxSConfig::MxSConfig(const char* keyName, long defValue, const char* host, 
              const char* prog, SConfigType val, IM_IMPACT impact)
{
    DIAGC("mxSConfig", 5, "Entering ctor MxSConfig");
    char buf[1024];
    snprintf(buf, sizeof(buf), "%ld", defValue);
    addConfigKey(EXTN_SERVICE, keyName, buf, val, impact);
    key = keyName;
    defaultval = defValue;

}

//+---------------------------------------------------------------------------
//
//  Method:     MxSConfig::addConfigKey
//
//  Synopsis:   Adds new config key.
//
//----------------------------------------------------------------------------
void MxSConfig::addConfigKey(const char* program, const char* keyName, const char* defValue, 
                  SConfigType type, IM_IMPACT impact, int min, int max)
{
    memset(&_callbackContext, 0, sizeof(CfgContext));

    _callbackContext.min = min;
    _callbackContext.max = max;
    _callbackContext.impact = impact;
    _callbackContext.type = type;
    _callbackContext.parent = this;

    IM_Error imError;
    IM_InitError (&imError);

    _registration = IM_AddConfigKeyWithMsg(IM_GetMyHost(), program, 
                       keyName, defValue, &ConfigCallback, (void*) &_callbackContext, &imError);
 
    if (!HandleError(imError, _name.c_str())){
        _registration = NULL;
    }
 
    IM_FreeError(&imError);
}

//+---------------------------------------------------------------------------
//
//  Method:     MxSConfig::getString
//
//  Synopsis:   Returns string config key value 
//
//----------------------------------------------------------------------------
const char* MxSConfig::getString(const char* keyName, const char* defValue)
{
    IM_Error imError;
    IM_InitError(&imError);
    string str = IM_GetConfigValue(_registration,
                                   IM_GetMyHost(),
                                   EXTN_SERVICE,
                                   keyName, 
                                   defValue,
                                   &imError);
    
    if (!HandleError(imError, keyName))
    {
        // when an error then assign default value
        str.assign(defValue);
    }

    
    IM_FreeError(&imError);
    DIAGC("mxSConfig", 5, "getString; key=%s; value=%s", keyName, str.c_str());
    return str.c_str();
}

//+---------------------------------------------------------------------------
//
//  Method:     MxSConfig::getLong
//
//  Synopsis:   Returns long config key value
//
//----------------------------------------------------------------------------
long MxSConfig::getLong(const char* keyName, long defValue)
{
    DIAGC("mxSConfig", 5, "Enter getLong");

    IM_Error imError;
    IM_InitError(&imError);
    long newLong = IM_GetConfigValueAsInt(_registration,
                                          IM_GetMyHost(),
                                          EXTN_SERVICE,
                                          keyName, 
                                          defValue,   
                                          &imError);

    DIAGC("mxSConfig", 5, "MxSConfig Return :", "%d", newLong);
    return newLong;
}

//+---------------------------------------------------------------------------
//
//  Method:     MxSConfig::getBool
//
//  Synopsis:   Returns boolean config key value
//
//----------------------------------------------------------------------------
bool MxSConfig::getBool(const char* keyName, bool defValue)
{
    DIAGC("mxSConfig", 5, "Enter getBool");

    IM_Error imError;
    IM_InitError(&imError);
    bool newBool = IM_GetConfigValueAsBool(_registration,
                                           IM_GetMyHost(),
                                           EXTN_SERVICE,
                                           keyName,
                                           defValue,
                                           &imError);

    DIAGC("mxSConfig", 5, "MxSConfig Return :%s", newBool ? "true" : "false");
    return newBool;
}

char* MxSConfig::getPath(char * buf, int bufsize)
{
    DIAGC("mxSConfig", 5, "Enter getPath");

    if (!buf || bufsize <= 0)
        return NULL;

    *buf = '\0';
    const char* value = getString();

    if (!value)
        return NULL;

    char valueBuf[MAX_PATH_NAME];
    memset(valueBuf, 0, sizeof(valueBuf));
    strncpy(valueBuf, value, sizeof(valueBuf) - 1);

    if (valueBuf[0] == '\0')
        return NULL;

    if (valueBuf[0] == '/') { // absolute path
        snprintf(buf, bufsize, "%s", valueBuf);
    } else {            // relative path
        char* intermailDir = getenv("INTERMAIL");
        if (intermailDir == NULL)
            return NULL;
        snprintf(buf, bufsize, "%s/%s",
                      intermailDir, valueBuf);
    }

    DIAGC("mxSConfig", 5, "getPath return :%s", buf);
    return buf;
}

//+---------------------------------------------------------------------------
//
//  Method:     ConfigCallback
//
//  Synopsis:   The key change callback function. For now it just notifies
//  the caller that all changes require a restart, so no processing of key
//  data is required.
//----------------------------------------------------------------------------
ImpactInfo* ConfigCallback(const char* host, const char* module, const char* key, IM_ACTION action,
                const char* value, void* callbackContext)
{

    CfgContext* cfgContext = reinterpret_cast<CfgContext*> (callbackContext);
    ImpactInfo* impactInfo = cfgContext->parent->getImpactInfo();

    memset(impactInfo, 0, sizeof(ImpactInfo));

    if (action != IM_CFG_ASSESS) {
        DIAGC("symantecConfig", 3, "Impact=UPDATE, key=%s, module=%s, value=%s", key, module, value);
        impactInfo->name = cfgContext->impact;
        return impactInfo;
    }

    DIAGC("symantecConfig", 3, "Impact=ASSESS, key=%s, module=%s, value=%s", key, module, value);
	
    switch(cfgContext->type) {
        case SCfg:
        default:
            break;
    }

    impactInfo->name = cfgContext->impact;
    return impactInfo;
}

//+---------------------------------------------------------------------------
//
//  Method:     HandleError
//
//  Synopsis: Handle config key retrieving errors.
//----------------------------------------------------------------------------
bool HandleError(IM_Error& imError, const char* name)
{
    if (imError.number) {
        DIAGC("symantecscan", 1,
              "Error in Config/Stat API. Name=%s, errorCode=%d, errorMsg=%s",
              name, imError.number, imError.string);
        return false;
    }

    return true;
}


//+---------------------------------------------------------------------------
//
//  Method:    SymString::SymString 
//
//  Synopsis:  Constructor for SymString
//
//----------------------------------------------------------------------------
SymString::SymString(char ch, int repeat)
{
    // check for sane argument value
    if (repeat < 0) repeat = 0;

    char* pc = new char [repeat];
    memset(pc, ch, repeat);
    assign(pc, repeat);
    delete [] pc;
}

// Operator '=' overloaded
SymString& SymString::operator=(const SymString& str)
{
    if ( &str != this)
    assign(str.begin(), str.end());
    return *this;
}
 
SymString& SymString::operator= (const std::string& str)
{
    if ( &str != this)
    assign(str.begin(), str.end());
    return *this;
}

SymString& SymString::operator= (const char* str)
{
    assign(str, str + strlen(str));
    return *this;
}
 
SymString& SymString::operator= (const char& c)
{
    assign(static_cast<size_type>(1), c); 
    return *this;
}
  
SymString& SymString::operator+= (const std::string& str)
{
    append(str);
    return *this;
}

SymString& SymString::operator+= (const SymString& str)
{
    append(str);
    return *this;
}

SymString& SymString::operator+= (const char* str)
{
    append(str);
    return *this;
}

SymString& SymString::operator+= (const char& c)
{
    push_back(c);
    return *this;
}
 
bool SymString::operator== (const SymString& str)
{
    return compare(str) == 0;
}

bool SymString::operator== (const std::string& str)
{
    return compare(str) == 0;
}

bool SymString::operator== (const char* str)
{
    return compare(str) == 0;
}
 
bool SymString::operator!= (const SymString& str)
{
    return compare(str) != 0;
}

bool SymString::operator!= (const std::string& str)
{
    return compare(str) != 0;
}


bool SymString::operator!= (const char* str)
{
    return compare(str) != 0;
}

 
char SymString::operator[](int index)
{
    if (index < 0 )  index += int_cast(size());

    if (index >= 0 && index < int_cast(size()))
    return data()[index];

    return 0;
}

int SymString::CompareNoCase(const char* str)
{
    return strncasecmp(c_str(), str, size()) == 0;
}
 
 
int SymString::Index(const char* str, int start) const
{
    return int_cast(find(str, start));
}
 
int SymString::RIndex(const char* str, int start) const
{
    if ( !str )
        return -1;

    if (start <= 0 || start > int_cast(size()))
    start = int_cast(size());

    return int_cast(rfind(str, start));
}

bool SymString::HasPrefix(const char* str) const
{
    bool result = false;
    if (str) {
        int buflen = int_cast(strlen(str));

        if (int_cast(this->length()) < buflen)
        return result;

        result = (strncmp(AsPtr(), str, buflen) == 0) ? true : false;
    }

    return result;
}

SymString SymString::Clone(void) const
{
    if (!empty()) {
        return SymString(c_str(), size());
    } else {
            return SymString();
    }
}

SymString& SymString::Trim(void)
{
    long spaces;

    //Remove whitespace from the beginning of the string.
    for (spaces = 0; spaces < int_cast(size()) && isspace(data()[spaces]); ++spaces)
        ;

    if (spaces > 0)
        erase(0, spaces);

    //Remove whitespace from the end of the string.
    for (spaces = 0; 
         spaces < int_cast(size()) && isspace(data()[size() - spaces - 1]); 
         ++spaces)
        ;

    if (spaces > 0)
        erase(size() - spaces, spaces);
    
    return *this;
}

SymString& SymString::UpperCase()
{
    std::transform(this->begin(), this->end(), this->begin(), 
                 (int(*)(int))(std::toupper));
    return *this;
}
 
SymString SymString::Segment(int start, int length) const
{
    return size() ? substr(start, length).c_str() : "aa";
}


SymStringArray SymString::Split(const char* delimiter)
{
    if (strlen(delimiter) > 1) {
        SymString strDelimiter(delimiter);
        return _split(strDelimiter);
    }

    SymStringArray  strList;
 
    char* tokptr; // Use by strtok_r to store it's state.
    
    char* buf = strdup(c_str());
   
    char* p; 
    p=strtok_r(buf, delimiter,&tokptr);
 
    while( p!=NULL) {
        if(p)
            strList.push_back(p);
 
        p=strtok_r(NULL, delimiter ,&tokptr);
    }
 
    free(buf);
    return strList;
}

// This function allocates memory and return a copy of the string buffer. 
// The caller should free the buffer.
char* SymString::GetBuf() const
{
    char* pc = new char[size()];
    if (pc == NULL) return NULL;

    memmove(pc, c_str(), size());
    return pc;
}

SymString SymString::S(const char* macro, const char* replace, const char* opts)
{
    SymString strReplace(replace);
    return S(macro, strReplace, opts);
}

// This is the special implementation to support symantec virus Notices, 
// so only opts="g" is supported
SymString SymString::S(const char* macro, const SymString& replace, const char* opts)
{
    // only "g" option is supported/implemented.
    if ( strncmp(opts, "g",1) != 0)
        return *this;

    SymString strMacro = macro;
    SymStringArray strList = _split(strMacro);
    std::string tmpStr="";

    if (strList.size() == 0)
        return *this;

    for( SymStringArray::iterator ptr = strList.begin();
         ptr != strList.end(); ptr++) {
        if (tmpStr.size() == 0) {
            // Check if the "macro" is at the begining
            //of the string
            size_t nPos = find(macro,0);
            if (nPos == 0 )
                tmpStr = replace;
        }

        if ( ptr == strList.begin())
            tmpStr += *ptr;
        else 
            tmpStr += replace + *ptr;
    }

    // Check if the "macro" is at the end of the string
    size_t nPos = find(macro, size() - strlen(macro));
    if (nPos != std::string::npos)
        tmpStr += replace;

    assign(tmpStr.begin(), tmpStr.end());

    return *this;
}

SymStringArray SymString::_split(const SymString& delimiter)
{
    std::string input = c_str();
    SymStringArray strList;
    strList.clear();

    long iPos = 0;
    int newPos = -1;
    size_t sizeS2 = delimiter.size();
    size_t isize = size();

    std::vector<int> positions;

    newPos = int_cast(input.find (delimiter, 0));

    if( newPos < 0 ) { return strList; }

    int numFound = 0;

    while( newPos >= iPos ){
        numFound++;
        positions.push_back(newPos);
        iPos = newPos;
        newPos = int_cast(input.find (delimiter, iPos+sizeS2+1));
    }

    for( int i=0; i <= int_cast(positions.size()); i++ ) {
        std::string strTmp;
        if( i == 0 ) { strTmp = input.substr(i, (positions[i]) ); }

        int offset = positions[i-1] + int_cast(sizeS2);

        if( offset < int_cast(isize) ) {
            if( i == int_cast(positions.size()) ) {
                strTmp = input.substr(offset);
            }
            else if( i > 0 ) {
                strTmp = input.substr(positions[i-1] + sizeS2, 
                         positions[i] - positions[i-1] - sizeS2 );
            }
        }
        if( strTmp.size() > 0 ) {
            strList.push_back(strTmp);
        }
    }

    return strList;
}


//+---------------------------------------------------------------------------
// Helper function to join all the elements in the string with
// the seperator and return a SymString.
//---------------------------------------------------------------------------
SymString Join(SymStringArray& strArray, const char* seperator)
{
    SymString newStr;

    if (strArray.empty())
        return newStr;

    SymStringArray::iterator p = strArray.begin();

    // initialize newStr with first element.
    newStr = *p;

    for( p++; p != strArray.end(); p++) {
        newStr += seperator;
        newStr += *p;
    }

    return newStr;
}



//+---------------------------------------------------------------------------
//
// Function:    MxMimeHeaderParser::GetToken()
//
// Synopsis:    Get a token from the header parser.
//
// Returns:     the token
//              If it's a string token, "s" is set to the string
//
//----------------------------------------------------------------------------

int MxMimeHeaderParser::GetToken(SymString &s)
{
    const char *input = _str_Input;
    unsigned long inputlen = Len(_str_Input);
    char c;

    while (1) {
        // skip space
        while (_n_InputPos < inputlen &&
               isspace(input[_n_InputPos]))
            _n_InputPos++;
        if (_n_InputPos == inputlen)
            return k_MHT_EOF;

        c = input[_n_InputPos];

        // parse quoted string
        if (c == '"') {
            s = "";
            _n_InputPos++;
            while (_n_InputPos < inputlen) {
                char c = input[_n_InputPos++];
                if (c == '"')
                    return k_MHT_String;
                if (c == '\\' && _n_InputPos < inputlen)
                    c = input[_n_InputPos++];
                s += c;
            }
            return k_MHT_Error;
        }

        // skip comments
        if (c == '(') {
            _n_InputPos++;
            int level = 1;
            while (_n_InputPos < inputlen) {
                c = input[_n_InputPos++];
                if (c == '(')
                    level++;
                else if (c == '\\' && _n_InputPos < inputlen)
                    _n_InputPos++;
                else if (c == ')' && --level == 0)
                    break;
            }
            // start over
            continue;
        }
        break;
    }

    // skip specials (use RFC2045 if _f_ParsingParameters, otherwise
    // uses RFC821)
    const char *specials = _f_ParsingParameters ?
                              "()<>@,;:\\\"/[]?= " : "()<>@,;:\\\"/[] ";
    if (strchr(specials, c)) {
      if (_n_InputPos<inputlen)
        _n_InputPos++;
        return c;
    }
    // got a regular string token...
    s = "";
    while (1) {
        c = input[_n_InputPos];
        if (c == '"' || c < ' ' || c > 126 || strchr(specials, c))
            break;
        s += c;
        _n_InputPos++;
    }

    // if we couldn't get a string, because of weird chars, flag an error
    if (Len(s) == 0)
        return k_MHT_Error;

    return k_MHT_String;
}

//+---------------------------------------------------------------------------
//
// Function:    MxMimeHeaderPerser::GetString()
//
// Synopsis:    Get a string token from the header parser.
//
// Returns:     the token, or an undefined SString if the current
//              token is not a string.
//
//----------------------------------------------------------------------------

SymString MxMimeHeaderParser::GetString()
{
    if (_n_Token != k_MHT_String) {
        // we were expecting a string token; flag an error
        _f_Error = true;
        return SymString();
    }
    SymString s(_str_TokenText);
    _n_Token = GetToken(_str_TokenText);
    return s;
}

//+---------------------------------------------------------------------------
//  Synopsis:   Decodes a string that has been encoded with the Base64
//              alphabet as described in the MIME specification.
//----------------------------------------------------------------------------

const char * pz_Base64chars =
"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";


SymString MxDecodeBase64 (const SymString & str_EncodedString)
{
    // the workspace
    int    n_InLength = Len (str_EncodedString);
    char * ps_Buffer  = str_EncodedString.GetBuf();
    char * ps         = ps_Buffer;

    // the translation table
    char * ps_Table = (SymString((char)127, 256).GetBuf());

    for (int i=0; i<65; i++)
    {
        ps_Table[(uchar)(pz_Base64chars[i])] = i;
    }

    // translate all characters, throwing away
    // any characters not in the alphabet

    for (int j=0; j<n_InLength; j++)
    {
        if ((*ps = ps_Table[(uchar)(ps_Buffer[j])]) <= 64)
            ps++;
    }

    // delete the translation table
    delete [] ps_Table;

    // calculate the new length (should be multiple of 4)
    n_InLength = (int_cast(ps - ps_Buffer) & ~3);

    char * ps2 = ps = ps_Buffer;

    // decode groups of 4 octets
    for (int k=0; k<n_InLength; k+=4)
    {
        int n;

        // special case of padding at end of stream
        if (ps[3] == 64)
        {
            if (ps[2] == 64)
            {
                n  = *ps++ << 2;
                n += *ps++ >> 4;

                *ps2++ = (char)(n & 255);

                break;
            }

            n  = (*ps++ << 10);
            n += (*ps++ <<  4);
            n += (*ps++ >>  2);

            *ps2++ = (char)(n >> 8);
            *ps2++ = (char)(n &  255);

            break;
        }

        n  = (*ps++ << 18);
        n += (*ps++ << 12);
        n += (*ps++ <<  6);
        n += (*ps++);

        *ps2++ = (char)((n>>16) & 255);
        *ps2++ = (char)((n>>8)  & 255);
        *ps2++ = (char)( n      & 255);
    }

    SymString str_Return (ps_Buffer, int_cast(ps2 - ps_Buffer));

    // delete the work buffer
    delete [] ps_Buffer;

    return str_Return;
}

//+---------------------------------------------------------------------------
//  Synopsis:   Encodes a string with the MIME specification of Base64
//  Returns:    An encoded string containing only characters from the Base64
//              alphabet, and CRLF line breaks every 76 characters.  The
//              string always ends with a CRLF line terminator.
//----------------------------------------------------------------------------
SymString MxEncodeBase64 (const SymString & str_UnencodedString, bool oneline)
{
    int n_InLength = Len (str_UnencodedString);

    if (n_InLength == 0)
    {
        return SymString();
    }

    // number of full groups of 4 encoded characters
    int n_Groups     = n_InLength / 3;

    // length of encoded output
    int n_OutLength  = ((n_InLength+2)/3)*4;

    // number of line CRLF breaks
    int n_LineBreaks = oneline ? 0 : (n_OutLength+75)/76;

    n_OutLength     += 2 * n_LineBreaks;

    // create a buffer for the encoded output
    register char * ps_OutBuf = new char [n_OutLength];
    register char * ps_Out    = ps_OutBuf;

    register const char * ps_In = str_UnencodedString;

    register int   na,  nb,  nc;
    register uchar cha, chb, chc, chd;

    register int n_LineWidth = 0;

    for (int i=0; i<n_Groups; i++)
    {
        na = (uchar)(*ps_In++);
        nb = (uchar)(*ps_In++);
        nc = (uchar)(*ps_In++);

        cha = (uchar)(na >> 2);
        chb = (uchar)(((na << 4) + (nb >> 4)) & 63);
        chc = (uchar)(((nb << 2) + (nc >> 6)) & 63);
        chd = (uchar)(nc & 63);

        *ps_Out++ = pz_Base64chars[cha];
        *ps_Out++ = pz_Base64chars[chb];
        *ps_Out++ = pz_Base64chars[chc];
        *ps_Out++ = pz_Base64chars[chd];

        // insert a CRLF every 76 characters
        if (((n_LineWidth += 4) == 76) && n_LineBreaks)
        {
            *ps_Out++   = '\r';
            *ps_Out++   = '\n';

            n_LineWidth = 0;
            n_LineBreaks--;
        }
    }

    // if input is not a multiple of 3 chars, pad the end
    int n_LeftOver = n_InLength - n_Groups*3;

    if (n_LeftOver == 1)
    {
        int na = (uchar)(*ps_In++);
        *ps_Out++ = pz_Base64chars[na>>2];
        *ps_Out++ = pz_Base64chars[(na&3)<<4];
        *ps_Out++ = '=';
        *ps_Out++ = '=';
    }

    if (n_LeftOver == 2)
    {
        int na = (uchar)(*ps_In++);
        int nb = (uchar)(*ps_In++);
        *ps_Out++ = pz_Base64chars[na>>2];
        *ps_Out++ = pz_Base64chars[((na&3)<<4)+(nb>>4)];
        *ps_Out++ = pz_Base64chars[((nb&15)<<2)];
        *ps_Out++ = '=';
    }

    if (n_LineBreaks)
    {
        *ps_Out++   = '\r';
        *ps_Out++   = '\n';
    }

    SymString str_Return (ps_OutBuf, n_OutLength);

    delete [] ps_OutBuf;

    return str_Return;
}


//+---------------------------------------------------------------------------
//
//  Method:     MethodTimingSnapShot::getRealTimeClock
//
//  Synopsis:   Samples the system's real time clock if available.
//              Where possible, this function uses system calls
//              with extremely low overhead, such as gethrtime() and
//              read_real_time().
//
//  Arguments:  None.
//
//  Outputs:    The value of a real time clock, in micro-seconds.
//              The some systems support nano-second resolution,
//              I'm limited to micro-seconds, because linux
//              only supported gettimeofday which has a resolution of usecs.
//
//----------------------------------------------------------------------------
RealTimeClockSample_t MethodTimingSnapShot::getRealTimeClock()
{
    RealTimeClockSample_t result=0;

    // The following conditional compliation statements are used to
    // invoke the function with the least overhead on each system.
    timeval currentTime;
    gettimeofday(&currentTime,0);
    result = currentTime.tv_sec*1000000 + currentTime.tv_usec;

    return result;
}

//+---------------------------------------------------------------------------
//
//  Method:     MethodTimingSnapShot::MethodTimingSnapShot
//
//  Synopsis:   This constructor records the snapshot label and records
//              the starting time.
//
//  Arguments:  argMethodName  - The label that will be displayed when.
//                               the snapshot is printed.
//
//  Notes:      Users should instantiate instances using this constructor
//              at the opening brace of the code block to be instrumented.
//
//----------------------------------------------------------------------------
MethodTimingSnapShot::MethodTimingSnapShot( char const * const argMethodName)
  : enabled(true), methodName(NULL)
{
    textBuffer[0]='\0';

    if(!IM_IsTraceEnabled() || !IM_GetTraceLevel(argMethodName))
    {
        enabled = false;
        return;
    }

    init(argMethodName);
    startingTime = getRealTimeClock();
}

//+---------------------------------------------------------------------------
//
//  Method:     MethodTimingSnapShot::recordSnapShot
//
//  Synopsis:   This method records the end of the SnapShot interval
//              and formats the output. Clients may call it early
//              if they only want to record and retreive a portion of methods
//              activity.
//
//----------------------------------------------------------------------------
void MethodTimingSnapShot::recordSnapShot()
{
      if(enabled == false)
        return;

      RealTimeClockSample_t endingTime = getRealTimeClock();
      duration = (endingTime - startingTime);
      duration /= 1000.0f; // Scale us to ms.

      // I have to format floats here, because DIAGC doesn't support %f.
      char buffer[20];
      sprintf(buffer,"%5.3f", duration);

      snprintf(textBuffer,255,"%s ran in %s ms",
               methodName->c_str(), buffer);
}

//+---------------------------------------------------------------------------
//
//  Method:     MethodTimingSnapShot::~MethodTimingSnapShot
//
//  Synopsis:   This is the class destructor. It records the end time
//              for the snapshot interval (via recordSnapShot), and
//              prints the SnapShot timing data.
//----------------------------------------------------------------------------
MethodTimingSnapShot::~MethodTimingSnapShot()
{
      if(enabled == true)
      {
          recordSnapShot();

          DIAGC(methodName->c_str(),1,"%s", textBuffer);
      }

      delete methodName;
}

//+---------------------------------------------------------------------------
//
//  Method:     MethodTimingSnapShot::init
//
//  Synopsis:   Allocates the std::string
//
//  Arguments:  name-  The SnapShots names used to initialize the std::string.
//----------------------------------------------------------------------------
void MethodTimingSnapShot::init(char const * const name)
{
    if(methodName != NULL)
    {
        delete methodName;
    }

    methodName = new string(name);
}

}
