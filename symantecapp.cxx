//----------------------------------------------------------------------------
//
// File Name:   symantecApp.cxx
//
// Synopsis:    SymantecApplication object implementation
//----------------------------------------------------------------------------
// Copyright 2017 Synchronoss, Inc. All Rights Reserved.
//
// The copyright to the computer software herein is the property of
// Synchronoss, Inc. The software may be used and/or copied only with
// The written permission of Synchronoss, Inc. or in accordance with the
// terms and conditions stipulated in the agreement/contract under which
// the software has been supplied.
//----------------------------------------------------------------------------

#include "symantecapp.hxx"
using namespace symantecscan;

SymantecConfig* SymantecConfig::_pSymantecConfig = 0;

void SymantecApplication::initStat()
{
    DIAGC("symantecApplication", 5, "Entering initStat");
    int	errCode;
    IM_Error imError;
    IM_FileInitArgs* fileInitArgs;
    IM_InitError(&imError);
    errCode = IM_InitFileInitArgs(&fileInitArgs, EXTN_SERVICE, &imError);
    IM_FreeError(&imError);
}

//+---------------------------------------------------------------------------
//
// Method:     SymantecApplication::SymantecApplication
//
// Synopsis:   Initialize C-Api, create Config and read config info.
//             Create Log object and create disposition lookup table
//
//----------------------------------------------------------------------------
SymantecApplication::SymantecApplication(const char* prgName)
                                        :nSymConnRetry(0),
                                        _pSymantecConfig(NULL),
                                        _pSymantecScanner(NULL),
                                        _lSpeRetryInterval(0),
                                        _bApplicationInitialized(false),
                                        _bSenderNotice(false),
                                        _bRecipientNotice(false),
                                        _bRemoteRecipientNotice(false),
                                        _bCOSAuthoritative(false),
                                        _bSenderOptInNotice(false),
                                        _bScanAndClean(false),
                                        _bRescanUUEncode(false),
                                        _bRemAllMsgParts(false),
                                        _bRescanSuspecAttach(false),
                                        _bSymDisbBase64Decode(false),
                                        _strProgram(prgName),
                                        _strHostIP(),
                                        _XOpwvHeader()
{
    DIAGC("symantecApplication", 5, "Entering ctor SymantecApplication");
    // Create disposition look up table
    _createDispHashTable();
    _createSymantecActionHashTable();
    _strHostIP = _getExtServIP(); 
    DIAGC("symantecApplication", 5, _strHostIP.c_str());
    DIAGC("symantecApplication", 5, "Exit ctor SymantecApplication");
}

//+---------------------------------------------------------------------------
//
// Method:     SymantecApplication::~SymantecApplication
//
// Synopsis:   Releasing resource at time of application shut down.
//----------------------------------------------------------------------------
SymantecApplication::~SymantecApplication()
{
    Finalize();
}

//+---------------------------------------------------------------------------
//
// Method:     SymantecApplication::getDisposition
//
// Synopsis:   Retrieve disposition like 'keep', 'bounce' 'drop' for given action.
//
//----------------------------------------------------------------------------
StringInfo* SymantecApplication::getDisposition(SymantecAction symAction)
{
    DIAGC("symantecApplication", 5, "Entering getDisposition");
    DispositionHash::iterator pDisp = _dispList.find(symAction);
	
    if (pDisp != _dispList.end()) {
        DIAGC("symantecApplication", 5, "getDisposition; Retrun= %s", pDisp->second.str);
        return &pDisp->second;
    } else 
        LOG(Notification, "Disposition Not found !");

    return NULL;
}

void SymantecApplication::_createSymantecActionHashTable()
{
    _actionList.clear();

    _actionList.insert(SymantecActionList::value_type("allow",    SymantecActionAllow));
    _actionList.insert(SymantecActionList::value_type("defer",    SymantecActionDefer));
    _actionList.insert(SymantecActionList::value_type("sideline", SymantecActionSideline));
    _actionList.insert(SymantecActionList::value_type("avspool",  SymantecActionAVSpool));
    _actionList.insert(SymantecActionList::value_type("reject",   SymantecActionReject));
    _actionList.insert(SymantecActionList::value_type("clean",    SymantecActionClean));
    _actionList.insert(SymantecActionList::value_type("repair",   SymantecActionRepair));
    _actionList.insert(SymantecActionList::value_type("discard",  SymantecActionDiscard));
}


SymantecAction SymantecApplication::getSymantecAction(string symAction)
{
    DIAGC("symantecApplication", 5, "Entering getSymantecAction; action=%s", symAction.c_str());
    SymantecActionList::iterator pAction = _actionList.find(symAction);
	
    if (pAction == _actionList.end()) {
        DIAGC("symantecApplication", 5, "AVSymActionInvalid action");
        return SymantecActionInvalid;
    }
	
    DIAGC("symantecApplication", 5, "SymantecACtion found for %s", pAction->first.c_str());
    switch ( pAction->second ) {
        case SymantecActionAllow:
        case SymantecActionDefer:
        case SymantecActionSideline:
        case SymantecActionReject:
        case SymantecActionClean:
        case SymantecActionRepair:
        case SymantecActionDiscard:
            return pAction->second;

        case SymantecActionAVSpool:
        default:
            LOG(Notification, "AVSymActionInvalid action");
            return SymantecActionInvalid;
    }
}
	

void SymantecApplication::Initialize()
{
    DIAGC("symantecApplication", 5, "Entering Initialize");

    try {

        _pSymantecConfig = SymantecConfig::getInstance();
        _pSymantecScanner = new SymantecScanner();

        _setVirusNotification();

        string str(getSymantecInboundAction());

        SymantecAction sa = getSymantecAction(str);

        getDisposition(sa);

        str.assign(getSymantecOutboundAction());

        sa = getSymantecAction(str);

        getDisposition(sa);

        // get scan mark header for reject message
        _XOpwvHeader = SYMX_HEADER;

        nSymConnRetry = _pSymantecConfig->getSymServerConnRetry();
        _bScanAndClean = _pSymantecConfig->getScanAndClean();
        _bRescanUUEncode = _pSymantecConfig->getRescanUUEncode();
        _bRemAllMsgParts = _pSymantecConfig->getRemAllMsgParts();
        _bRescanSuspecAttach = _pSymantecConfig->getRescanSuspecAttach();
        _bSymDisbBase64Decode = _pSymantecConfig->getDisbBase64Decode();
	    _bReportDefInfo = _pSymantecConfig->getReportDefInfoEnabled();
    	_bICAPHttpClientIP = _pSymantecConfig->getICAPHttpClientIPEnabled();
        _lSpeRetryInterval = _pSymantecConfig->getSymSpeRetryIntervalLimit();
        _bApplicationInitialized = _pSymantecScanner->isValid();

        DIAGC("symantecApplication", 5, "Application initialize flag=%s", 
              _bApplicationInitialized ? "true" : "false");

    } catch (exception& ex) {

        LOG(Notification, "Exception occured in symApp initialization", "%s", ex.what());
    } catch(...) {

       LOG(Notification, "Unknown exception occured in symApp initialization");
    }
}


void SymantecApplication::Finalize()
{
    if (_pSymantecScanner)
        delete _pSymantecScanner;

    SymantecConfig::releaseInstance();

}

//+---------------------------------------------------------------------------
//
// Method:     SymantecApplication::_addDisp
//
// Synopsis:   Create an StringInfo object and assign disposition value and
//             insert into lookup table
//
//----------------------------------------------------------------------------
void  SymantecApplication::_addDisp(SymantecAction syAction, const char* disp)
{
    StringInfo strInfo;
    strInfo.str = disp;
    strInfo.len = int_cast(strlen(disp));
    std::pair <DispositionHash::iterator, bool> ret;
	
    ret = _dispList.insert(DispositionHash::value_type(syAction, strInfo));

    if ( ret.second == false) 
    LOG(Notification, "addDisp", "fail to insert value !");
}

//+---------------------------------------------------------------------------
//
// Method:     SymantecApplication::_createDispHashTable
//
// Synopsis:   Create Disposition lookup table
//
//----------------------------------------------------------------------------
void  SymantecApplication::_createDispHashTable()
{
    DIAGC("symantecApplication", 5, "Entering createDispHashTable");
    _dispList.clear();

    _addDisp(SymantecActionInvalid,  "defer");
    _addDisp(SymantecActionDefer,    "defer");
    _addDisp(SymantecActionNull,     "keep");
    _addDisp(SymantecActionAllow,    "keep");
    _addDisp(SymantecActionSideline, "sideline");
    _addDisp(SymantecActionAVSpool,  "sideline avspool");
    _addDisp(SymantecActionReject,   "bounce This mail contains virus");
    _addDisp(SymantecActionDiscard,  "drop");
    _addDisp(SymantecActionClean,    "keep");
    _addDisp(SymantecActionRepair,   "keep");

    DIAGC("symantecApplication", 5, "Exit createDispHashTable");
}

//+---------------------------------------------------------------------------
//
// Method:     SymantecApplication::_setVirusNotification
//
// Synopsis:   Set virus notification flags i.e. Sender, Local sender, 
//             local/remote recipient only.
//
//----------------------------------------------------------------------------
void  SymantecApplication::_setVirusNotification()
{
    _bSenderNotice = _bRecipientNotice = _bRemoteRecipientNotice 
        = _bSenderOptInNotice = false;
    const char* cfgValue = getSymVirusNotice();

    if (cfgValue == NULL || strlen(cfgValue) <= 0 
        || strcasecmp("false", cfgValue) == 0 || strcasecmp("null", cfgValue) == 0) {

        if (isMsgcatLoggingEnabled())
            REPORT_SymnAVInit(SLOGHANDLE, Notification, "Virus Notice disabled");
        else LOG(Notification, "AVSymInit", "Virus Notice disabled"); 

    } else if (strcasecmp("sender", cfgValue) == 0) {

        if (isMsgcatLoggingEnabled())
            REPORT_SymnAVInit(SLOGHANDLE, Notification, "Virus Notices=Sender");
        else LOG(Notification, "AVSymInit", "Virus Notices=Sender");

        _bSenderNotice = true;
    } else if (strcasecmp("recipient", cfgValue) == 0 ||
               strcasecmp("LocalRecipient", cfgValue) == 0) {

        if (isMsgcatLoggingEnabled())
            REPORT_SymnAVInit(SLOGHANDLE, Notification, 
                                "Virus Notices=Local Recipient(s)");
        else LOG(Notification, "AVSymInit", 
                 "Virus Notices=Local Recipient(s)");
        _bRecipientNotice = true;

    } else if (strcasecmp("RemoteRecipient", cfgValue) == 0) {
   
        if (isMsgcatLoggingEnabled())
            REPORT_SymnAVInit(SLOGHANDLE, Notification, 
                                "Virus Notices=Remote Recipient(s)");
        else LOG(Notification, "AVSymInit", 
                 "Virus Notices=Remote Recipient(s)");

        _bRemoteRecipientNotice=true;
    } else if (strcasecmp("AllRecipient", cfgValue) == 0) {
     
        if (isMsgcatLoggingEnabled())
            REPORT_SymnAVInit(SLOGHANDLE, Notification, 
                                "Virus Notices=All Recipient(s)");
        else LOG(Notification, "AVSymInit", 
                 "Virus Notices=All Recipient(s)");

        _bRecipientNotice = true;
        _bRemoteRecipientNotice = true;
    } else if (strcasecmp("both", cfgValue) == 0 || 
               strcasecmp("true", cfgValue) == 0) {

        if (isMsgcatLoggingEnabled())
            REPORT_SymnAVInit(SLOGHANDLE, Notification, 
                                "Virus Notices=Sender and Local Recipient(s)");
        else LOG(Notification, "AVSymInit", 
                 "Virus Notices=Sender and Local Recipient(s)");

        _bRecipientNotice = true;
        _bSenderNotice = true;
    } else if (strcasecmp("senderOptIn", cfgValue) == 0) {
     
        if (isMsgcatLoggingEnabled())
            REPORT_SymnAVInit(SLOGHANDLE, Notification, "Virus Notices=Opted in sender");
        else LOG(Notification, "AVSymInit", "Virus Notices=Opted in sender");
        
        _bSenderOptInNotice=true;
    } else {

        string errMsg("Invalid value virusNotices=");
        errMsg.append(cfgValue);
        errMsg.append(". Virus Notices disabled.");

        LOG(Error, "AVSymInvalidAction", errMsg.c_str());
    }
}

//+---------------------------------------------------------------------------
//
// Method:     SymantecApplication::getExtServIP()
//
// Synopsis:   Retrieve extension server IP address from core socket API. 
//            
//
//----------------------------------------------------------------------------
string SymantecApplication::_getExtServIP()
{
    int s;
    struct ifconf ifconf;
    struct ifreq ifr[50];
    int ifs;
    string extIP("127.0.0.1");

    s = socket(AF_INET, SOCK_STREAM, 0);
    if (s < 0) {
        return extIP;
    }

    ifconf.ifc_buf = (char *) ifr;
    ifconf.ifc_len = sizeof ifr;

    if (ioctl(s, SIOCGIFCONF, &ifconf) == -1) {
        return extIP;
    }

    ifs = ifconf.ifc_len / sizeof(ifr[0]);
    char ip[INET_ADDRSTRLEN];
    struct sockaddr_in *s_in = (struct sockaddr_in *) &ifr[1].ifr_addr;

    if (!inet_ntop(AF_INET, &s_in->sin_addr, ip, sizeof(ip))) {
        return extIP;
    }

    extIP.assign(ip);

    close(s);

  return extIP;
}



//---------------------------------------------------------------------
// Config Key Name  : /host/symantecScan/symantecAction
//
// Description      : When a mail is suspect of virus, Symantec Extension
//                    service will take action as defined in this
//                    config key
//
// Default Value    : [Allow]
// Possible Values  : Allow, Clean, Discard, Sideline, Reject, Repair
// Related Keys     : None
// Servers Affected : imextserv, SymantecScan extension service
// Change Impact    : Restart required
// Example          : /host/symantecScan/symantecAction: [Reject]
//---------------------------------------------------------------------
void SymantecConfig::_setSymAction(MxSConfig* pMxConf)
{
    // SymantecAction key
    _strInboundAction = pMxConf->getString(KEY_SYMACTION, "Allow");

    // key validation part
    if ((strcasecmp(_strInboundAction.c_str(), "Allow") == 0) ||
        (strcasecmp(_strInboundAction.c_str(), "Clean") == 0) ||
        (strcasecmp(_strInboundAction.c_str(), "Repair") == 0) ||
        (strcasecmp(_strInboundAction.c_str(), "Discard") == 0) ||
        (strcasecmp(_strInboundAction.c_str(), "Reject") == 0) ||
        (strcasecmp(_strInboundAction.c_str(), "Sideline") == 0)) {
      
        LOG(Notification, "config key set; ", "key=%s; value=%s", KEY_SYMACTION,
            _strInboundAction.c_str());
    } else {
        LOG(Error, "Invalid value set in Config Key;", "key=%s; value=%s", KEY_SYMACTION,
            _strInboundAction.c_str()); 
    }
}

//---------------------------------------------------------------------
// Config Key Name  : /host/symantecScan/symantecOutboundAction
//
// Description      : When a mail is suspect of virus, Symantec Extension
//                    service will take action as defined in this
//                    config key
//
// Default Value    : [Allow]
// Possible Values  : Allow, Clean, Discard, Sideline, Reject, Repair
// Related Keys     : None
// Servers Affected : imextserv, SymantecScan extension service
// Change Impact    : Restart required
// Example          : /host/symantecScan/symantecOutboundAction: [Reject]
//---------------------------------------------------------------------
void SymantecConfig::_setSymOutboundAction(MxSConfig* pMxConf)
{
    // SymantecOutboundAction key
    _strOutboundAction = pMxConf->getString(KEY_SYMOUTBOUNDACTION, "Allow");

    // key validation part
    if ((strcasecmp(_strOutboundAction.c_str(), "Allow") == 0) ||
        (strcasecmp(_strOutboundAction.c_str(), "Clean") == 0) ||
        (strcasecmp(_strOutboundAction.c_str(), "Repair") == 0) ||
        (strcasecmp(_strOutboundAction.c_str(), "Discard") == 0) ||
        (strcasecmp(_strOutboundAction.c_str(), "Reject") == 0) ||
        (strcasecmp(_strOutboundAction.c_str(), "Sideline") == 0)) {
      
        LOG(Notification, "config key set; ", "key=%s; value=%s", KEY_SYMOUTBOUNDACTION,
            _strOutboundAction.c_str());
    } else {

        LOG(Error, "Invalid value set in Config Key; ", "key=%s; value=%s", KEY_SYMOUTBOUNDACTION,
            _strOutboundAction.c_str()); 
    }
}

//---------------------------------------------------------------------
// Config Key Name  : /host/symantecScan/symantecTimeoutSecs
//
// Description      : Specifies the maximum amount of time, in seconds, 
//                    that the Symantec classifier can spend to determine whether
//                    a virus is present in any given message. 
//                    If the value of this key is set to 0 (the default), 
//                    the timeout is disabled.
//
// Default Value    : [0]
// Initial Value    : [0]
// Possible Values  : Any positive long value
// Related Keys     : None
// Servers Affected : imextserv, SymantecScan extension service
// Change Impact    : Restart required
// Example          : /host/symantecScan/symantecTimeoutSecs: [0]
//---------------------------------------------------------------------
void SymantecConfig::_setSymTimeoutSecs(MxSConfig* pMxConf)
{
    string tmp = pMxConf->getString(KEY_SYMTIMEOUTSECS, "0");
    _strSymantecTimeoutSecs = getValidatedLongStr(tmp);
    _nSymTimeout = atol(_strSymantecTimeoutSecs.c_str());

    // validating key value
    if (_nSymTimeout >= 0 && _nSymTimeout <= 32767) {

        LOG(Notification, "config key set; ", "key=%s; value=%lu; initialValue=%s", KEY_SYMTIMEOUTSECS,
            _nSymTimeout, tmp.c_str() );
    } else {
        _nSymTimeout = 0;
        _strSymantecTimeoutSecs.assign("0");
        LOG(Error, "Invalid value set in config key; ", "key=%s; newValue=%lu; initialValue=%s",
            KEY_SYMTIMEOUTSECS, _nSymTimeout, tmp.c_str() ); 
    }
}


//---------------------------------------------------------------------
// Config Key Name  : /host/symantecScan/symantecTimeoutAction
//
// Description      : Specifies the action to take action upon timeout.
//                    Following possible values,
//                    Allow: Deliver mail to recipient(s) (the default value)
//                    avspool: Sideline mail to the sidelined/avspool directory
//                    Defer: Defer mail
//                    Reject: Return (bounce) mail to sidelined/directory. 
//                   
// Default Value    : [Allow]
// Initial Value    : []
// Possible Values  : [Allow, Avspool, Defer, Reject, Sideline]
// Related Keys     : None
// Servers Affected : imextserv, SymantecScan extension service
// Change Impact    : Restart required
// Example          : /host/symantecScan/symantecTimeoutAction: [Allow]
//---------------------------------------------------------------------
void SymantecConfig::_setSymTimeoutAction(MxSConfig* pMxConf)
{
    _strSymantecTimeoutAction = pMxConf->getString(KEY_SYMTIMEOUTACTION, "Allow");
    
    // key validation part
    if (strcasecmp(_strSymantecTimeoutAction.c_str(), "Allow") == 0 ||
        strcasecmp(_strSymantecTimeoutAction.c_str(), "Avspool") == 0 ||
        strcasecmp(_strSymantecTimeoutAction.c_str(), "Defer") == 0 ||
        strcasecmp(_strSymantecTimeoutAction.c_str(), "Reject") == 0 ||
        strcasecmp(_strSymantecTimeoutAction.c_str(), "Sideline") == 0) {
      
        LOG(Notification, "config key set; ", "key=%s; value=%s", KEY_SYMTIMEOUTACTION,
            _strSymantecTimeoutAction.c_str());
    } else {
        LOG(Error, "Invalid value set in Config Key; ", "key=%s; value=%s", KEY_SYMTIMEOUTACTION,
            _strSymantecTimeoutAction.c_str()); 
    }
}

//---------------------------------------------------------------------
// Config Key Name  : /host/symantecScan/SymantecServerConnection
//
// Description      : Specifies Symantec Proctection Engine IP address
//                    and port no. n below format need for a client 
//                    connection.
//                    e.g.
//                    server:<IP address>:<port no.>;;; 
//
// Default Value    : [127.0.0.1:1344]
// Possible Values  : Connection string as mention above
// Related Keys     : None
// Servers Affected : imextserv, SymantecScan extension service
// Change Impact    : Restart required
// Example          : /host/symantecScan/SymantecServerConnection: 
//                    [0.0.0.0:1344]
//                    [127.0.0.2:1111] 
//---------------------------------------------------------------------
void SymantecConfig::_setSymServerConn(MxSConfig* pMxConf)
{
    string tempConn = pMxConf->getString(KEY_SYMSERVERCONN, 
                                                      "127.0.0.1:1344");

    // preparing connection string in spe required format
    // i.e. server:<IP Address>:<port>:<no of conn. keep alive>;;;
    // delimiter is ';;;' after each SPE server address
    char *pch = NULL;
    pch =  strtok((char*) tempConn.c_str(), "\n");
    while(pch != NULL) {

        // assign formated connection string to data member
        _strSymantecServerConnection.append("server:");
        _strSymantecServerConnection.append(pch);
        _strSymantecServerConnection.append(";;;");
        pch = strtok(NULL, "\n");
    }        
   
    LOG(Notification, "config key set; ", "key=%s; value=%s; initialValue=%s", KEY_SYMSERVERCONN,
        _strSymantecServerConnection.c_str(), tempConn.c_str());
}

//---------------------------------------------------------------------
// Config Key Name  : symnAVVirusNoticeBodySender
//
// Description      : Specifies notification string about virus detection 
//                    in mail attachment or content. 
//                    This notice used as message body to send a notice to sender.
//                    You can specify one or more of the following macros: 
//                    Date, Filename, Postmaster,
//                    Recipients, Sender, and Virusname.
//
// Default Value    : [""]
// Possible Values  : [Any multi-line string that results in an RFC 822-compliant message.]
// Servers Affected : SymantecRemote / SymantecLocal extension service.
// Change Impact    : Extension server and Symantec extension service restart are required 
// Example          : /host//symnAVScan/symnAVVirusNoticeBodySender:
//                    [The mail message you sent to <Recipients> on <Date>]
//---------------------------------------------------------------------
void SymantecConfig::_setSymAVNoticeBodySender(MxSConfig* pMxConf)
{
    _strSymAVNoticeBodySender = pMxConf->getString(KEY_SYMVRSNOTICEBODYSENDER, 
                                                   "");
    LOG(Notification, "config key set; ", "key=%s; value=%s", KEY_SYMVRSNOTICEBODYSENDER,
        _strSymAVNoticeBodySender.c_str());
}

//---------------------------------------------------------------------
// Config Key Name  : symnAVVirusNoticeHeaderSender
//
// Description      : Specifies notification string about virus detection 
//                    in mail attachment or content. 
//                    This notice used as message header to send a notice to sender.
//
// Default Value    : [""]
// Possible Values  : 
// Servers Affected : SymantecRemote / SymantecLocal extension service.
// Change Impact    : Extension server and Symantec extension service 
//                    restart are required 
// Example          : /host/symnAVScan/symnAVVirusNoticeHeaderSender:
//                    [From: AV cleaner <<Postmaster>>]
//                    [Subject: Virus Alert]
//---------------------------------------------------------------------
void SymantecConfig::_setSymAVNoticeHeaderSender(MxSConfig* pMxConf)
{
    _strSymAVNoticeHeaderSender = pMxConf->getString(KEY_SYMVRSNOTICEHEADERSENDER, 
                                                     "");
    LOG(Notification, "config key set; ", "key=%s; value=%s", KEY_SYMVRSNOTICEHEADERSENDER,
        _strSymAVNoticeHeaderSender.c_str());
}

//---------------------------------------------------------------------
// Config Key Name  : enableMsgcatLogging
//
// Description      : Enable logging using msgcat (SymantecScan.m) and disable
//                    logging using CAPI. If disabled it will switch back
//                    to logging using CAPI.
//
// Default Value    : [false]
// Possible Values  : true/false
// Servers Affected : imextserv, Symantec extension service
// Change Impact    : Restart required
// Example          : /host/symnAVScan/enableMsgcatLogging: [true]
//---------------------------------------------------------------------
void SymantecConfig::_setSymMsgCatLogging(MxSConfig* pMxConf)
{
    _strSymMsgCatLog = pMxConf->getString(KEY_SYMMSGCATLOGGING, "false");
    if (strcasecmp(_strSymMsgCatLog.c_str(), "true") == 0) {
        _bSymMsgCatLog = true;
        LOG(Notification, "config key set; ", "key=%s; value=%s; initialValue=%s", KEY_SYMMSGCATLOGGING,
            (_bSymMsgCatLog ? "true" : "false"),_strSymMsgCatLog.c_str());

    } else if (strcasecmp(_strSymMsgCatLog.c_str(), "false") == 0) {
        _bSymMsgCatLog = false;

        LOG(Notification, "config key set; ", "key=%s; value=%s; initialValue=%s", KEY_SYMMSGCATLOGGING,
            (_bSymMsgCatLog ? "true" : "false"),_strSymMsgCatLog.c_str());
    } else {
        _bSymMsgCatLog = false;
        LOG(Error, "Invalid value set to config key; ", "key=%s; value=%s; initialValue=%s",
            KEY_SYMMSGCATLOGGING, (_bSymMsgCatLog ? "true" : "false"),_strSymMsgCatLog.c_str());
    }
}

//---------------------------------------------------------------------
// Config Key Name  : symnAVCOSAuthoritive 
//
// Description      : If the key is set to true, only the mail user's i
//                    CoS (for sender, mailSymantecOutboundVirusAction, 
//                   , for recipients, mailSymantecInboundVirusAction) is used 
//                   to derive the Symantec action on the message. 
//                    
// Default Value    : [false]
// Possible Values  : [True, False]
// Servers Affected : SymantecRemote / SymantecLocal extension service.
// Change Impact    : Extension server and Symantec extension service
//                    restart are required 
// Example          : /host/symnAVScan/symnAVCOSAuthoritive: [False]
//
//---------------------------------------------------------------------
void SymantecConfig::_setSymCosAuth(MxSConfig* pMxConf)
{
    _strSymCoSAuth = pMxConf->getString(KEY_SYMCOSAUTHORITATIVE, "false");
    if (strcasecmp(_strSymCoSAuth.c_str(), "true") == 0) {
        _bSymCoSAuth = true;
        LOG(Notification, "config key set; ", "key=%s; value=%s; initialValue=%s", KEY_SYMCOSAUTHORITATIVE,
           (_bSymCoSAuth ? "true" : "false"),_strSymCoSAuth.c_str());
    } else if (strcasecmp(_strSymCoSAuth.c_str(), "false") == 0) {
        _bSymCoSAuth = false;
        LOG(Notification, "config key set; ", "key=%s; value=%s; initialValue=%s", KEY_SYMCOSAUTHORITATIVE,
           (_bSymCoSAuth ? "true" : "false"),_strSymCoSAuth.c_str());
    } else {
        _bSymCoSAuth = false;
        LOG(Error, "Invalid value set in config key; ", "key=%s; value=%s; initialValue=%s", KEY_SYMCOSAUTHORITATIVE,
           (_bSymCoSAuth ? "true" : "false"),_strSymCoSAuth.c_str());

    }
}

//---------------------------------------------------------------------
// Config Key Name  : deletedAttachmentNotice
//
// Description      : This configuration key is used to replace attachments
//                    that cannot be cleaned by the Symantec AV engine. The
//                    following macros can be used:
//                       <Virusname>, <Filename>, <Sender>,
//                       <Recipients>,<Date>, and <Postmaster>
//
// Default Value    : []
// Initial Value    : [Content-Type: text/plain; charset=us-ascii]
//                    [Content-Transfer-Encoding: 7bit]
//                    []
//                    [--------  Virus Warning Message --------]
//                    []
//                    [<Filename> is removed from here
//                        because it contains a virus (<Virusname>).]
//                    []
//                    [----------------------------------------]
// Possible Values  : String
// Related Keys     : None
// Servers Affected : imextserv, Symantec extension service
// Change Impact    : Restart required
// Example          : /host/symnAVScan/deletedAttachmentNotice: []
//---------------------------------------------------------------------
void SymantecConfig::_setSymDelAttachNotice(MxSConfig* pMxConf)
{
    _strSymDelAttachNotice = pMxConf->getString(KEY_SYMDELATTACHNOTICE, "");
    LOG(Notification, "config key set; ", "key=%s; value=%s", KEY_SYMDELATTACHNOTICE,
        _strSymDelAttachNotice.c_str());
}

//---------------------------------------------------------------------
// Config Key Name  : deletedPlainTextNotice
//
// Description      : This configuration key is used to replace plain text
//                    that cannot be cleaned by the Symantec AV engine. The
//                    following macros can be used:
//                       <Virusname>,  <Sender>,
//                       <Recipients>,<Date>, and <Postmaster>
//
// Default Value    : []
// Possible Values  : String
// Related Keys     : None
// Servers Affected : imextserv, Symantec extension service
// Change Impact    : Restart required
// Example          : /host/symnAVScan/deletedPlainTextNotice:
//                    [Content-Type: text/plain; charset=us-ascii]
//                    [Content-Transfer-Encoding: 7bit]
//                    []
//                    [--------  Virus Warning Message --------]
//                    []
//                    [Your message is removed from here
//                        because it contains a virus (<Virusname>).]
//                    []
//                    [----------------------------------------]
//---------------------------------------------------------------------
void SymantecConfig::_setSymDelPlainTextNotice(MxSConfig* pMxConf)
{
   _strSymDelPlainTextNotice = pMxConf->getString(KEY_SYMDELPLAINTEXTNOTICE, "");
   LOG(Notification, "config key set; ", "key=%s; value=%s", KEY_SYMDELPLAINTEXTNOTICE,
       _strSymDelPlainTextNotice.c_str());
}

//---------------------------------------------------------------------
// Config Key Name  : virusNoticeFrom
//
// Description      : Specifies the address from which virus infection 
//                    notices are sent to senders and/or recipients
//                    of messages (or attachments) that are determined to
//                    contain viruses.
//                    Use this key to provide email users with an address
//                    to contact if they have questions concerning
//                    the virus or its removal.
//
// Default Value    : []
// Possible Values  : [Any valid SMTP address (name@domain).] 
// Servers Affected : Symantec extension service and Extension server
// Change Impact    : Extension server and Symantec extension service 
//                    restart are required 
// Example          : /host/symnAVScan/virusNoticeFrom: [imail1@synchronoss.com]
//
//---------------------------------------------------------------------
void SymantecConfig::_setSymVirusScanFrm(MxSConfig* pMxConf)
{
    _strSymVirusScanFrm = pMxConf->getString(KEY_SYMVIRUSNOTICEFRM, "");
    LOG(Notification, "config key set; ", "key=%s; value=%s", KEY_SYMVIRUSNOTICEFRM,
        _strSymVirusScanFrm.c_str());
}

//----------------------------------------------------------------------------------
// Config Key Name  : virusNotices
//
// Description      : This key determines whom to send notices when
//                    a mail is suspect of virus.
//
// Default Value    : [false]
// Possible Values  : sender -    notice sent only to sender
//                    recipient - notice sent only to local recipient
//                    RemoteRecipient - notice sent only to Remote recipient
//                    AllRecipient - notice sent only to both Remote and local recipient
//                    both/true - notice sent to both sender and local recipient.
//                                 (true is maintained for backward compatibility)
//                    []/false -  No notices will be sent
// Related Keys     : None
// Servers Affected : imextserv, symantecscan extension service
// Change Impact    : Server Restart
// Example          : /host/symnAVScan/virusNotices: [both]
//----------------------------------------------------------------------------------
void SymantecConfig::_setSymVirusNotice(MxSConfig* pMxConf)
{
    _strSymVirusNotice = pMxConf->getString(KEY_SYMVIRUSNOTICE, "false");
    if ( strcasecmp(_strSymVirusNotice.c_str(), "sender") == 0 ||
         strcasecmp(_strSymVirusNotice.c_str(), "recipient") == 0 ||
         strcasecmp(_strSymVirusNotice.c_str(), "LocalRecipient") == 0 ||
         strcasecmp(_strSymVirusNotice.c_str(), "RemoteRecipient") == 0 ||
         strcasecmp(_strSymVirusNotice.c_str(), "AllRecipient") == 0 ||
         strcasecmp(_strSymVirusNotice.c_str(), "both") == 0 ||
         strcasecmp(_strSymVirusNotice.c_str(), "true") == 0 ||
         strcasecmp(_strSymVirusNotice.c_str(), "false") == 0 ||
         _strSymVirusNotice.size() == 0 ) {
        LOG(Notification, "config key set; ", "key=%s; value=%s", KEY_SYMVIRUSNOTICE,
            _strSymVirusNotice.c_str());
    } else {
        string tmp(_strSymVirusNotice.c_str());
        _strSymVirusNotice.assign("false");
        LOG(Error, "Invalid value set in config key; ", "key=%s; value=%s; initialValue=%s", 
            KEY_SYMVIRUSNOTICE, _strSymVirusNotice.c_str(), tmp.c_str());
    }
}

//---------------------------------------------------------------------
// Config Key Name  : virusNoticeBodyRecipients
//
// Description      : Specifies the body of the notice message that 
//                    will be sent to recipients of an infected message.
//                    You can specify one or more of these macros: 
//                    Date, Filename, Postmaster, Sender and Virusname.
//
//
// Default Value    : [""]
// Possible Values  : [Any multi-line string that results in an RFC 822-compliant message.]
// Servers Affected : Symantec extension service and Extension server
// Change Impact    : Extension server and Symantec extension service restart are required 
// Example          : /host/symnAVScan/virusNoticeBodyRecipients: 
//                    [We have detected a virus (<Virusname>) in]
//                    [your mail traffic sent from]
//                    [<Sender> in the file (<Filename>) on <Date>.]
//---------------------------------------------------------------------
void SymantecConfig::_setSymVirusNoticeBodyRecip(MxSConfig* pMxConf)
{
    _strSymVirusNoticeBodyRecip = pMxConf->getString(KEY_SYMVIRUSNOTICEBODYRECIP, "");
    LOG(Notification, "config key set; ", "key=%s; value=%s", KEY_SYMVIRUSNOTICEBODYRECIP,
        _strSymVirusNoticeBodyRecip.c_str());
}

//---------------------------------------------------------------------
// Config Key Name  : virusNoticeHeaderRecipients
//
// Description      : Specifies the header of the notice message that 
//                    will be sent to recipients of an infected message.
//                    You can specify one or more of these macros: 
//                    Date, Filename, Postmaster, Sender and Virusname.
//
// Default Value    : [""]
// Possible Values  : [Any multi-line string that results in an RFC 822-compliant message.]
// Servers Affected : Symantec extension service and Extension server
// Change Impact    : Extension server and Symantec extension service restart are required 
// Example          : /host/symnAVScan/ virusNoticeHeaderRecipients: 
//                    [From: AV cleaner <Postmaster>]
//                     [Subject: Virus Alert]
//                     [Mime-Version: 1.0]
//                      [Content-Type: text/plain; charset=us-ascii]
//---------------------------------------------------------------------
void SymantecConfig::_setSymVirusNoticeHeaderRecip(MxSConfig* pMxConf)
{
    _strSymVirusNoticeHeaderRecip = pMxConf->getString(KEY_SYMVIRUSNOTICEHEADERRECIP, "");
    LOG(Notification, "config key set; ", "key=%s; value=%s", KEY_SYMVIRUSNOTICEHEADERRECIP,
        _strSymVirusNoticeHeaderRecip.c_str());
}

//---------------------------------------------------------------------
// Config Key Name  : symnAVServerConnRetries
// Description      : Number of retries to attempt connect with SPE 
//                    if the service call fails.
//                    
// Default Value    : 0
// Initial Value    : 0
// Possible Value   : Any unsigned int value 0 - 32767
// Related Keys     : SymantecServerConnection 
// Servers Affected : Symantec extension service
// Change Impact    : Restart extension server
// Example          : /host/symnAVScan / symnAVServerConnRetries: [5]
//---------------------------------------------------------------------
void SymantecConfig::_setSymServerConnRetry(MxSConfig* pMxConf)
{
    string tmp = pMxConf->getString(KEY_SYMSERVERCONNRETRIES, "0");
    _strSymantecServerRetry = getValidatedLongStr(tmp);
    _nSymServerRetry = atol(_strSymantecServerRetry.c_str());

    // validating key
    if (_nSymServerRetry >= 0 && _nSymServerRetry <= 32767) {

        LOG(Notification, "config key set; ", "key=%s; value=%lu; initialValue=%s", 
            KEY_SYMSERVERCONNRETRIES,
            _nSymServerRetry, tmp.c_str() ); 
    } else {

        _nSymServerRetry = 0;
        _strSymantecServerRetry.assign("0");
        LOG(Error, "Invalid value set in Config key; ", "key=%s, value=%lu; initialValue=%s", 
            KEY_SYMSERVERCONNRETRIES,
            _nSymServerRetry, tmp.c_str() );
    }
}

//---------------------------------------------------------------------
// Config Key Name  : repairedAttachmentNotice
//
// Description      : This configuration key is used to add a attachment
//                    to inform users we have repaired an virus from an
//                    infected attachment.
//                    following macros can be used:
//                       <Virusname>, <Filename>, <Sender>,
//                       <Recipients>,<Date>, and <Postmaster>
//
// Default Value    : []
// Initial Value    : [Content-Type: text/plain; charset=us-ascii]
//                    [Content-Transfer-Encoding: 7bit]
//                    []
//                    [--------  Virus Warning Message --------]
//                    []
//                    [<Filename> is repaired, because it
//                     contained a virus (<Virusname>).]
//                    []
//                    [----------------------------------------]
// Possible Values  : String
// Related Keys     : SymantecAction (when value is Repair)
// Servers Affected : imextserv, Symantec extension service
// Change Impact    : Restart required
// Example          : /host/symnAVScan/repairedAttachmentNotice: []
//---------------------------------------------------------------------
void SymantecConfig::_setSymRepairedAttachNotice(MxSConfig* pMxConf)
{
    _strSymRepairedAttachNotice = pMxConf->getString(KEY_SYMREPAIREDATTACHNOTICE, "");
    LOG(Notification, "config key set;", "key=%s; value=%s", KEY_SYMREPAIREDATTACHNOTICE,
          _strSymRepairedAttachNotice.c_str());
}


//---------------------------------------------------------------------
// Config Key Name  : repairedPlainTextNotice
//
// Description      : This configuration key is used to add a attachment
//                    to inform users we have repaired an virus from an
//                    infected plain text.
//                    following macros can be used:
//                       <Virusname>, <Filename>, <Sender>,
//                       <Recipients>,<Date>, and <Postmaster>
//
// Default Value    : []
// Initial Value    : [Content-Type: text/plain; charset=us-ascii]
//                    [Content-Transfer-Encoding: 7bit]
//                    []
//                    [--------  Virus Warning Message --------]
//                    []
//                    [<Filename> is repaired, because it
//                     contained a virus (<Virusname>).]
//                    []
//                    [----------------------------------------]
// Possible Values  : String
// Related Keys     : SymantecAction (when value is Repair)
// Servers Affected : imextserv, Symantec extension service
// Change Impact    : Restart required
// Example          : /host/symnAVScan/repairedPlainTextNotice: []
//---------------------------------------------------------------------
void SymantecConfig::_setSymRepairedPlainTexNotice(MxSConfig* pMxConf)
{
    _strSymRepairedPlainTextNotice = pMxConf->getString(KEY_SYMREPAIREDPLAINTEXNOTICE, "");
    LOG(Notification, "config key set;", "key=%s; value=%s", KEY_SYMREPAIREDPLAINTEXNOTICE,
          _strSymRepairedPlainTextNotice.c_str());
}

//----------------------------------------------------------------------------------
// Config Key Name  : disableBase64Decode
// Description      : This key is applicable only for repair action. This key indicates 
//                    whether the MIME part with base64 encoded content, is decoded before 
//                    sending to SPE for scan+repair.
//
// Default Value    : false
// Possible Values  : true/false
// Related Keys     : None
// Servers Affected : imextserv, Symantec extension service
// Change Impact    : Restart required
// Example          : /host/symnAVScan/disableBase64Decode: [false]
//----------------------------------------------------------------------------------
void SymantecConfig::_setSymDisBase64Decode(MxSConfig* pMxConf)
{
    string str = pMxConf->getString(KEY_SYMDISBASE64DECODE, "false");
    if (strcasecmp( str.c_str(), "true") == 0) {
        _bSymDisbBase64Decode = true;
        LOG(Notification, "config key set;", "key=%s; value=%s; initialValue=%s", KEY_SYMDISBASE64DECODE,
           (_bSymDisbBase64Decode ? "true" : "false"), str.c_str() );

    } else if (strcasecmp( str.c_str(), "false") == 0) {
        _bSymDisbBase64Decode = false;
        LOG(Notification, "config key set;", "key=%s; value=%s; initialValue=%s", KEY_SYMDISBASE64DECODE,
           (_bSymDisbBase64Decode ? "true" : "false"), str.c_str() );
    } else {
        _bSymDisbBase64Decode = false;
        LOG(Error, "Invalid value set in config key; ", "key=%s; value=%s; initialValue=%s", KEY_SYMDISBASE64DECODE,
           (_bSymDisbBase64Decode ? "true" : "false"), str.c_str() );
    }
}

//----------------------------------------------------------------------------------
// Config Key Name  : rescanSuspectedAttachment
// Description      : Indicates whether the MIME part encoded in Base64 is rescanned 
//                    as is, after the decoded version of the same is found non-suspicious 
//                    by SPE.
//
// Default Value    : false
// Possible Values  : true / false
// Change Impact    : Restart required
// Example          : /host/symnAVScan/rescanSuspectedAttachment: [false]
//----------------------------------------------------------------------------------
void SymantecConfig::_setSymRescanSuspectAttach(MxSConfig* pMxConf)
{
    string str = pMxConf->getString(KEY_SYMRESCANSUSPECATTACH, "false");
    if (strcasecmp( str.c_str(), "true") == 0) {
        _bRescanSuspecAttach = true;
        LOG(Notification, "config key set; ", "key=%s; value=%s; initialValue=%s", KEY_SYMRESCANSUSPECATTACH,
           (_bRescanSuspecAttach ? "true" : "false"),str.c_str());

    } else if (strcasecmp(str.c_str(), "false") == 0) {
        _bRescanSuspecAttach = false;
        LOG(Notification, "config key set; ", "key=%s; value=%s; initialValue=%s", KEY_SYMRESCANSUSPECATTACH,
           (_bRescanSuspecAttach ? "true" : "false"),str.c_str());
    } else {
        _bRescanSuspecAttach = false;
        LOG(Error, "Invalid value set in config key; ", "key=%s; value=%s; initialValue=%s", KEY_SYMRESCANSUSPECATTACH,
           (_bRescanSuspecAttach ? "true" : "false"),str.c_str());

    }
}

//----------------------------------------------------------------------------------
// Config Key Name  : symnScanAndClean
// Description      : If this configuration key is set to True, Symantec Anti-Virus 
//                    Scan extension service first scans a message and goes for 
//                    cleaning/repairing individual mime parts, only when 
//                    the entire message is found suspicious by SPE. 
//
// Default Value    : false
// Possible Values  : true / false
// Change Impact    : Restart required
// Example          : /host/symnAVScan/symnScanAndClean : [false]
//----------------------------------------------------------------------------------
void SymantecConfig::_setSymnScanAndClean(MxSConfig* pMxConf)
{
    string str = pMxConf->getString(KEY_SYMSCANANDCLEAN, "false");
    if (strcasecmp( str.c_str(), "true") == 0) { 
        _bScanAndClean = true;
        LOG(Notification, "config key set;", "key=%s; value=%s; initialValue=%s", KEY_SYMSCANANDCLEAN,
           (_bScanAndClean ? "true" : "false"),str.c_str() );

    } else if (strcasecmp( str.c_str(), "false") == 0) {
        _bScanAndClean = false;
        LOG(Notification, "config key set;", "key=%s; value=%s; InitialValue=%s", KEY_SYMSCANANDCLEAN,
           (_bScanAndClean ? "true" : "false"),str.c_str() );
    } else {
        _bScanAndClean = false;
        LOG(Error, "Invalid value set in config key; ", "key=%s; value=%s; initialValue=%s", KEY_SYMSCANANDCLEAN,
           (_bScanAndClean ? "true" : "false"),str.c_str() );
    }
}

//----------------------------------------------------------------------------------
// Config Key Name  : removeAllMessageParts
// Description      : Determines whether all the parts in the message are replaced by the 
//                    removedAllMessagePartsNotice message when an entire message is 
//                    detected as suspicious in Scan process but the individual mime 
//                    parts were found non-suspicious and hence not cleaned or repaired. 
//
// Default Value    : false
// Possible Values  : true / false
// Change Impact    : Restart required
// Example          : /host/symnAVScan/removeAllMessageParts: [false]
//----------------------------------------------------------------------------------
void SymantecConfig::_setSymnRemAllMsgParts(MxSConfig* pMxConf)
{
    string str = pMxConf->getString(KEY_SYMREMVALLMSGPARTS, "false");
    if (strcasecmp( str.c_str(), "true") == 0) {
        _bRemAllMsgParts = true;
        LOG(Notification, "config key set;", "key=%s; value=%s; InitialValue=%s", KEY_SYMREMVALLMSGPARTS,
          (_bRemAllMsgParts ? "true" : "false"),str.c_str() ); 

    } else if (strcasecmp( str.c_str(), "false") == 0) {
       _bRemAllMsgParts = false;
       LOG(Notification, "config key set;", "key=%s; value=%s; InitialValue=%s", KEY_SYMREMVALLMSGPARTS,
          (_bRemAllMsgParts ? "true" : "false"),str.c_str() ); 
    } else {
       _bRemAllMsgParts = false;
       LOG(Error, "Invalid value set in config key set; ", "key=%s; value=%s; InitialValue=%s", KEY_SYMREMVALLMSGPARTS,
          (_bRemAllMsgParts ? "true" : "false"),str.c_str() ); 

    }
}

//---------------------------------------------------------------------
// Config Key Name  : removedAllMessagePartsNotice
//
// Description      : This config key is used as attached notice to 
//                    a message if it is found infected in scan process, 
//                    but the individual mime parts were found non-suspicious 
//                    in Clean or Repair process.
//                   
//
//                    following macros can be used:
//                       <Virusname>, <Filename>, <Sender>,
//                       <Recipients>,<Date>, and <Postmaster>
//
// Default Value    : []
// Possible Values  : String
// Related Keys     : SymantecAction (when value is Repair)
// Servers Affected : imextserv, Symantec extension service
// Change Impact    : Restart required
// Example          : 
// /host/symnAVScan/removedAllMessagePartsNotice:
//                    [Content-Type: text/plain; charset=us-ascii]
//                    [Content-Transfer-Encoding: 7bit]
//                    []
//                    [-------- Virus Warning Message --------]
//                    []
//---------------------------------------------------------------------
void SymantecConfig::_setSymnRemAllMsgPartsNotice(MxSConfig* pMxConf)
{
    _strSymRemAllMsgPartsNotice = pMxConf->getString(KEY_SYMREMALLMSGPARTSNOTICE, "");
    LOG(Notification, "config key set;", "key=%s; value=%s",
          KEY_SYMREMALLMSGPARTSNOTICE,
          _strSymRemAllMsgPartsNotice.c_str());
}

//----------------------------------------------------------------------------------
// Config Key Name  : rescanUUEncode
// Description      : Indicates whether a body part is rescanned for only specific 
//                    uuencode content in it, if the entire body part does not appear 
//                    to be infected. This is applicable for 'clean' and 'repair' case.
//
// Default Value    : false
// Possible Values  : True/False
// Change Impact    : Restart required
// Example          : /host/symnAVScan/rescanUUEncode : [false]
//----------------------------------------------------------------------------------
void SymantecConfig::_setSymnRescanUUEncode(MxSConfig* pMxConf)
{
    string str = pMxConf->getString(KEY_SYMRESCANUUENCODE, "false");
    if (strcasecmp( str.c_str(), "true") == 0) {
        _bRescanUUEncode = true;
        LOG(Notification, "config key set; ", "key=%s; value=%s; InitialValue=%s", KEY_SYMRESCANUUENCODE,
           (_bRescanUUEncode ? "true" : "false"),str.c_str() );
    } else if (strcasecmp( str.c_str(), "false") ==0) {
        _bRescanUUEncode = false;
  
        LOG(Notification, "config key set; ", "key=%s; value=%s; InitialValue=%s", KEY_SYMRESCANUUENCODE,
           (_bRescanUUEncode ? "true" : "false"),str.c_str() );
    } else {
        _bRescanUUEncode = false;
  
        LOG(Error, "Invalid value set in config key; ", "key=%s; value=%s; InitialValue=%s", KEY_SYMRESCANUUENCODE,
           (_bRescanUUEncode ? "true" : "false"),str.c_str() );
    }
}

//---------------------------------------------------------------------
// Config Key Name  : symnAVScanByMsgType
//
// Description      : The Config key used to enable virus scan as per message type
//                    i.e. a message with / without attachment and a message with attachment.
//                    Possible options and mention as below,
//                    AllMsg: A message with or without attachment will get scanned for virus.
//                    MsgWithAttachment: A message with attachment will only get scanned for virus.
//                    
// Default Value    : [AllMsg]
// Possible Values  : [AllMsg, MsgWithAttachment]
// Servers Affected : Symantec AV Scan extension service and Extension server
// Change Impact    : Extension server and Symantec extension service restart are required
// Example          : /host/symnAVScan/symnAVScanByMsgType:[AllMsg]
//---------------------------------------------------------------------
void SymantecConfig::_setSymAVScanByMsgType(MxSConfig* pMxConf)
{
    _strSymAVScanByMsgType=pMxConf->getString(KEY_SYMSCANBYMSGTYPE, "AllMsg");

    // validate config key
    if ((strcasecmp(_strSymAVScanByMsgType.c_str(), "AllMsg") == 0) ||
        (strcasecmp(_strSymAVScanByMsgType.c_str(), "MsgWithAttachment") == 0)) {

        LOG(Notification, "config key set; ", "key=%s; value=%s", KEY_SYMSCANBYMSGTYPE,
            _strSymAVScanByMsgType.c_str());
    } else {

        string tmp(_strSymAVScanByMsgType.c_str());
        _strSymAVScanByMsgType.assign("AllMsg");
        LOG(Error, "Invalid value set in Config key;", "key=%s; value=%s; initialValue=%s",
            KEY_SYMSCANBYMSGTYPE,
            _strSymAVScanByMsgType.c_str(),
            tmp.c_str());
   }
            
}

//---------------------------------------------------------------------
// Config Key Name  : symnAVScanFailedAction
//
// Description      : The Config key used to configure an action on a message at
//	              particular scan error or exception occurred.
//		      Possible action and their meaning,
//		      Allow: Deliver a message and attachments.
//		      Discard: Delete a message and attachments. Also, the scan 
//		      failed notification (NDR) will be send to Sender.
//		      Notification message will form with two Config keys symnAVScanFailNoticeBody 
//		      and symnAVScanFailNoticeHeader.
//                    
// Default Value    : [Discard]
// Possible Values  : [Allow, Discard]
// Servers Affected : Symantec AV Scan extension service and Extension server
// Change Impact    : Extension server and Symantec extension service restart are required
// Example          : /host/symnAVScan/symnAVScanFailedAction:[Discard]
//---------------------------------------------------------------------
void SymantecConfig::_setSymAVScanFailedAction(MxSConfig* pMxConf)
{
    _strSymAVScanFailedAction=pMxConf->getString(KEY_SYMSCANFAILEDACTION, "Discard");

    // validate config key
    if ((strcasecmp(_strSymAVScanFailedAction.c_str(), "Allow") == 0) ||
        (strcasecmp(_strSymAVScanFailedAction.c_str(), "Discard") == 0)) {

        LOG(Notification, "config key set; ", "key=%s; value=%s", KEY_SYMSCANFAILEDACTION,
            _strSymAVScanFailedAction.c_str());
    } else {

        string tmp(_strSymAVScanFailedAction.c_str());
        _strSymAVScanFailedAction.assign("Discard");
        LOG(Error, "Invalid value set in Config key; ", "key=%s, value=%s, initialvalue=%s",
            KEY_SYMSCANFAILEDACTION,
            _strSymAVScanFailedAction.c_str(),
            tmp.c_str());
    }
}

//---------------------------------------------------------------------
// Config Key Name  : symnAVScanFailNoticeBody
//
// Description      : Specifies notification string about scan process failed for a mail.
//		      This notice used as message body to send a notice to sender.
//		      You can specify one or more of the following macros: Date, Postmaster, Recipient etc.
//                    
// Default Value    : []
// Possible Values  : [Any multi-line string that results in an RFC 822-compliant message.]
// Servers Affected : Symantec AV Scan extension service and Extension server
// Change Impact    : Extension server and Symantec extension service restart are required
// Example          : /host/symnAVScan/symnAVScanFailNoticeBody:
//		      [The mail message you sent to <Recipients> on <Date>]
//		      [with the file or message body has failed to scan virus. ]
//		      [The message discarded automatically on scan failed event.] 
//---------------------------------------------------------------------
void SymantecConfig::_setSymAVScanFailNoticeBody(MxSConfig* pMxConf)
{
    _strSymAVScanFailNoticeBody=pMxConf->getString(KEY_SYMSCANFAILNOTICEBODY, "");
    LOG(Notification, "config key set; ", "key=%s; value=%s", KEY_SYMSCANFAILNOTICEBODY,
        _strSymAVScanFailNoticeBody.c_str());
}

//---------------------------------------------------------------------
// Config Key Name  : symnAVScanFailNoticeHeader
//
// Description      : Specifies notification string about scan process failed for a mail.
//		      This notice used as message header to send a notice to sender.
//		      You can specify one or more of the following macros: Date, Postmaster etc.
//              
// Default Value    : []
// Possible Values  : [Any multi-line string that results in an RFC 822-compliant message.]
// Servers Affected : Symantec AV Scan extension service and Extension server
// Change Impact    : Extension server and Symantec extension service restart are required
// Example          : /host/symnAVScan/symnAVScanFailNoticeHeader:
//    		      [From: AV cleaner <Postmaster>]
//		      [Subject: Alert]
//		      [Mime-Version: 1.0]
//		      [Content-Type: text/plain; charset=us-ascii]
//		      [Content-Transfer-Encoding: 7bit] 
//---------------------------------------------------------------------
void SymantecConfig::_setSymAVScanFailNoticeHeader(MxSConfig* pMxConf)
{
    _strSymAVScanFailNoticeHeader=pMxConf->getString(KEY_SYMSCANFAILNOTICEHEADER, "");
    LOG(Notification, "config key set; ", "key=%s; value=%s", KEY_SYMSCANFAILNOTICEHEADER,
        _strSymAVScanFailNoticeHeader.c_str());
}

//---------------------------------------------------------------------
// Config Key Name  : symnSpeRetryInterval
//
// Description      : Specified the interval in seconds,the Symantec AV Extension
//                    service should retry connections to Symantec Protection Engine (SPE)
//                    servers that were previously unavailable.
//
// Default Value    : [30]
// Range            :[0-32767]
// Servers Affected : Symantec AV Scan extension service and Extension server
// Change Impact    : Extension server and Symantec extension service restart are required
// Example          : /host/symnAVScan/symnSpeRetryInterval: [30]
//---------------------------------------------------------------------
void SymantecConfig::_setSymSpeRetryInterval(MxSConfig* pMxConf)
{
    string tmp=pMxConf->getString(KEY_SYMSPERETRYINTERVAL, "30");
    _strSymantecSpeRetryInterval = getValidatedLongStr(tmp);
    _nSymSpeRetry = atol(_strSymantecSpeRetryInterval.c_str());

    // validate key
    if (_nSymSpeRetry >= 0 && _nSymSpeRetry <= 32767) {

        LOG(Notification, "config key set; ", "key=%s; value=%lu; InitialValue=%s", 
            KEY_SYMSPERETRYINTERVAL,
            _nSymSpeRetry,tmp.c_str() );
    } else {

        _strSymantecSpeRetryInterval.assign("30");
        _nSymSpeRetry = 30;
        LOG(Error, "Invalid value set in Config key; ", "key=%s; value=%lu; InitialValue=%s",
            KEY_SYMSPERETRYINTERVAL,
            _nSymSpeRetry,tmp.c_str() );
    }
}

//---------------------------------------------------------------------
// Config Key Name  : symnReportDefInfoEnabled
//
// Description      : Specified flag for extra information about virus would 
//		      be added in log (extension server) file. The extension service will 
//		      fetch virus definition date and revision number for a
//		      suspect message and add it in the log.
//
// Default Value    : [false]
// Possible Value   : [true/false]
// Servers Affected : Symantec AV Scan extension service and Extension server
// Change Impact    : Extension server and Symantec extension service restart are required
// Example          : /host/symnAVScan/symnReportDefInfoEnabled: [true]
//---------------------------------------------------------------------
void SymantecConfig::_setSymnReportDefInfoEnabled(MxSConfig* pMxConf)
{
    string str = pMxConf->getString(KEY_SYMREPORTDEFINFO, "false");
    if (strcasecmp( str.c_str(), "true") == 0) {
        _bReportDefInfo = true;
        LOG(Notification, "config key set;", "key=%s; value=%s; InitialValue=%s", KEY_SYMREPORTDEFINFO,
            (_bReportDefInfo ? "true" : "false"),str.c_str() );
    } else if(strcasecmp( str.c_str(), "false") == 0) {
        _bReportDefInfo = false;
        LOG(Notification, "config key set;", "key=%s; value=%s; InitialValue=%s", KEY_SYMREPORTDEFINFO,
            (_bReportDefInfo ? "true" : "false"),str.c_str() );
    } else {
        _bReportDefInfo = false;
        LOG(Error, "Invalid value set in config key; ", "key=%s; value=%s; InitialValue=%s", KEY_SYMREPORTDEFINFO,
            (_bReportDefInfo ? "true" : "false"),str.c_str() );
    }
}

//---------------------------------------------------------------------
// Config Key Name  : symnICAPHttpClientIPEnable
//
// Description      : Specified flag to enable input parameter i.e. IP address
//		      of Extension server to Symantec SDK API. The input parameter (IP address) 
//		      used in encapsulated HTTP request by Symantec SDK API.
//
// Default Value    : [false]
// Possible Value   : [true/false]
// Servers Affected : Symantec AV Scan extension service and Extension server
// Change Impact    : Extension server and Symantec extension service restart are required
// Example          : /host/symnAVScan/symnICAPHttpClientIPEnable: [false]
//---------------------------------------------------------------------
void SymantecConfig::_setSymnICAPHttpClientIPEnable(MxSConfig* pMxConf)
{
    string str = pMxConf->getString(KEY_SYMICAPHTTPCLIENTIP, "false");
    if (strcasecmp( str.c_str(), "true") == 0) {
        _bICAPHttpClientIP = true;
        LOG(Notification, "config key set;", "key=%s; value=%s; InitialValue=%s", KEY_SYMICAPHTTPCLIENTIP,
           (_bICAPHttpClientIP ? "true" : "false"), str.c_str() );
    } else if (strcasecmp( str.c_str(), "false") ==0 ) {
        _bICAPHttpClientIP = false;
        LOG(Notification, "config key set;", "key=%s; value=%s; InitialValue=%s", KEY_SYMICAPHTTPCLIENTIP,
           (_bICAPHttpClientIP ? "true" : "false"), str.c_str() );
    } else {
        _bICAPHttpClientIP = false;
        LOG(Error, "Invalid value set in config key; ", "key=%s; value=%s; InitialValue=%s", KEY_SYMICAPHTTPCLIENTIP,
           (_bICAPHttpClientIP ? "true" : "false"), str.c_str() );

    }
}

//---------------------------------------------------------------------
// Config Key Name  : symnAVVersion
// Description      : It will shows current symantec extension service version.
//
//---------------------------------------------------------------------
void SymantecConfig::_setSymnAVVersion(MxSConfig* pMxConf)
{
    string str = pMxConf->getString(KEY_SYMVERSION, "1.0.0.0");
    if (str.size() == 0) {
        string tmp(str.c_str());
        _strSymnAVVersion.assign("1.0.0.0");

        LOG(Error, "Invalid value set in config key; ", "key=%s; value=%s; InitialValue=%s",
            KEY_SYMVERSION, tmp.c_str(), _strSymnAVVersion.c_str());
    } else {
        _strSymnAVVersion.assign(str.c_str());
        LOG(Notification, "config key set;", "key=%s; value=%s", KEY_SYMVERSION, str.c_str());
    }

}

//+---------------------------------------------------------------------------
//
// Method:     SymantecConfig::populateCfgArray
//
// Synopsis:   Retrieve config key from database and set it with symantec 
//             application
//
//----------------------------------------------------------------------------
void SymantecConfig::populateCfgArray()
{
    MxSConfig* pMxConf = NULL;

    try {
        // symnAVAction config key
        pMxConf = new MxSConfig(KEY_SYMACTION, "Allow", "", EXTN_SERVICE, SCfg, CFG_SERV_RESTART);
        _setSymAction(pMxConf);
        _arrSymConfg.push_back(pMxConf);

        // symnAVOutboundAction key
        pMxConf = new MxSConfig(KEY_SYMOUTBOUNDACTION, "Allow", "", EXTN_SERVICE, SCfg, CFG_SERV_RESTART);
        _setSymOutboundAction(pMxConf);     
        _arrSymConfg.push_back(pMxConf);

        // symnAVTimeoutSecs key
        pMxConf = new MxSConfig(KEY_SYMTIMEOUTSECS, "0", "", EXTN_SERVICE, SCfg, CFG_SERV_RESTART);
        _setSymTimeoutSecs(pMxConf);
        _arrSymConfg.push_back(pMxConf);

        // symnAVTimeoutAction key
        pMxConf = new MxSConfig(KEY_SYMTIMEOUTACTION, "Allow", "", EXTN_SERVICE, SCfg, CFG_SERV_RESTART);
        _setSymTimeoutAction(pMxConf);
        _arrSymConfg.push_back(pMxConf);
			
        // symnAVServerConnection key
        pMxConf = new MxSConfig(KEY_SYMSERVERCONN, "127.0.0.1:1344", "", EXTN_SERVICE, SCfg, CFG_SERV_RESTART);
        _setSymServerConn(pMxConf);
        _arrSymConfg.push_back(pMxConf);

        // symnAVVirusNoticeBodySender key
        pMxConf = new MxSConfig(KEY_SYMVRSNOTICEBODYSENDER, "", "", EXTN_SERVICE, SCfg, CFG_SERV_RESTART);
        _setSymAVNoticeBodySender(pMxConf);
        _arrSymConfg.push_back(pMxConf);

        // symnAVVirusNoticeHeaderSender key
        pMxConf = new MxSConfig(KEY_SYMVRSNOTICEHEADERSENDER, "", "", EXTN_SERVICE, SCfg, CFG_SERV_RESTART);
        _setSymAVNoticeHeaderSender(pMxConf);
        _arrSymConfg.push_back(pMxConf);
    
        // enableMsgcatLogging key
        pMxConf = new MxSConfig(KEY_SYMMSGCATLOGGING, "false", "", EXTN_SERVICE, SCfg, CFG_SERV_RESTART);
        _setSymMsgCatLogging(pMxConf);
        _arrSymConfg.push_back(pMxConf);

        // symnAVCOSAuthoritive key
        pMxConf = new MxSConfig(KEY_SYMCOSAUTHORITATIVE, "false", "", EXTN_SERVICE, SCfg, CFG_SERV_RESTART);
        _setSymCosAuth(pMxConf);
        _arrSymConfg.push_back(pMxConf);
 
        // deletedAttachmentNotice key
        pMxConf = new MxSConfig(KEY_SYMDELATTACHNOTICE, "", "", EXTN_SERVICE, SCfg, CFG_SERV_RESTART);
        _setSymDelAttachNotice(pMxConf);
        _arrSymConfg.push_back(pMxConf);

        // deletedPlainTextNotice key
        pMxConf = new MxSConfig(KEY_SYMDELPLAINTEXTNOTICE, "", "", EXTN_SERVICE, SCfg, CFG_SERV_RESTART);
        _setSymDelPlainTextNotice(pMxConf);
        _arrSymConfg.push_back(pMxConf);

        // virusNoticeFrom key
        pMxConf = new MxSConfig(KEY_SYMVIRUSNOTICEFRM, "", "", EXTN_SERVICE, SCfg, CFG_SERV_RESTART);
        _setSymVirusScanFrm(pMxConf);
        _arrSymConfg.push_back(pMxConf);

        // virusNotices key
        pMxConf = new MxSConfig(KEY_SYMVIRUSNOTICE, "false", "", EXTN_SERVICE, SCfg, CFG_SERV_RESTART);
        _setSymVirusNotice(pMxConf);
        _arrSymConfg.push_back(pMxConf);

        // virusNoticeBodyRecipients key
        pMxConf = new MxSConfig(KEY_SYMVIRUSNOTICEBODYRECIP, "", "", EXTN_SERVICE, SCfg, CFG_SERV_RESTART);
        _setSymVirusNoticeBodyRecip(pMxConf);
        _arrSymConfg.push_back(pMxConf);

        // virusNoticeHeaderRecipients key
        pMxConf = new MxSConfig(KEY_SYMVIRUSNOTICEHEADERRECIP, "", "", EXTN_SERVICE, SCfg, CFG_SERV_RESTART);
        _setSymVirusNoticeHeaderRecip(pMxConf);
        _arrSymConfg.push_back(pMxConf);

        // symnAVServerConnRetries key
        pMxConf = new MxSConfig(KEY_SYMSERVERCONNRETRIES, "0", "", EXTN_SERVICE, SCfg, CFG_SERV_RESTART);
        _setSymServerConnRetry(pMxConf);
        _arrSymConfg.push_back(pMxConf);

        // repairedAttachmentNotice key
        pMxConf = new MxSConfig(KEY_SYMREPAIREDATTACHNOTICE, "", "", EXTN_SERVICE, SCfg, CFG_SERV_RESTART);
        _setSymRepairedAttachNotice(pMxConf);
        _arrSymConfg.push_back(pMxConf);

        // repairedPlainTextNotice key
        pMxConf = new MxSConfig(KEY_SYMREPAIREDPLAINTEXNOTICE, "", "", EXTN_SERVICE, SCfg, CFG_SERV_RESTART);
        _setSymRepairedPlainTexNotice(pMxConf);
        _arrSymConfg.push_back(pMxConf);

        // disableBase64Decode key
        pMxConf = new MxSConfig(KEY_SYMDISBASE64DECODE, "false", "", EXTN_SERVICE, SCfg, CFG_SERV_RESTART);
        _setSymDisBase64Decode(pMxConf);
        _arrSymConfg.push_back(pMxConf);

        // rescanSuspectedAttachment key
        pMxConf = new MxSConfig(KEY_SYMRESCANSUSPECATTACH, "false", "", EXTN_SERVICE, SCfg, CFG_SERV_RESTART);
        _setSymRescanSuspectAttach(pMxConf);
        _arrSymConfg.push_back(pMxConf);

        // symnScanAndClean key
        pMxConf = new MxSConfig(KEY_SYMSCANANDCLEAN, "false", "", EXTN_SERVICE, SCfg, CFG_SERV_RESTART);
        _setSymnScanAndClean(pMxConf);
        _arrSymConfg.push_back(pMxConf);

        // removeAllMessageParts key
        pMxConf = new MxSConfig(KEY_SYMREMVALLMSGPARTS, "false", "", EXTN_SERVICE, SCfg, CFG_SERV_RESTART);
        _setSymnRemAllMsgParts(pMxConf);
        _arrSymConfg.push_back(pMxConf);

        // removedAllMessagePartsNotice key
        pMxConf = new MxSConfig(KEY_SYMREMALLMSGPARTSNOTICE, "", "", EXTN_SERVICE, SCfg, CFG_SERV_RESTART);
        _setSymnRemAllMsgPartsNotice(pMxConf);
        _arrSymConfg.push_back(pMxConf);

        // rescanUUEncode key
        pMxConf = new MxSConfig(KEY_SYMRESCANUUENCODE, "false", "", EXTN_SERVICE, SCfg, CFG_SERV_RESTART);
        _setSymnRescanUUEncode(pMxConf);
        _arrSymConfg.push_back(pMxConf); 

        //symnAVScanByMsgType key
        pMxConf = new MxSConfig(KEY_SYMSCANBYMSGTYPE, "AllMsg", "", EXTN_SERVICE, SCfg, CFG_SERV_RESTART);
        _setSymAVScanByMsgType(pMxConf);
        _arrSymConfg.push_back(pMxConf);

        //symnAVScanFailedAction key
        pMxConf = new MxSConfig(KEY_SYMSCANFAILEDACTION, "Discard", "", EXTN_SERVICE, SCfg, CFG_SERV_RESTART);
        _setSymAVScanFailedAction(pMxConf);
        _arrSymConfg.push_back(pMxConf);

        //symnAVScanFailNoticeBody key
        pMxConf = new MxSConfig(KEY_SYMSCANFAILNOTICEBODY, "", "", EXTN_SERVICE, SCfg, CFG_SERV_RESTART);
        _setSymAVScanFailNoticeBody(pMxConf);
        _arrSymConfg.push_back(pMxConf);

        //symnAVScanByMsgType key
        pMxConf = new MxSConfig(KEY_SYMSCANFAILNOTICEHEADER, "", "", EXTN_SERVICE, SCfg, CFG_SERV_RESTART);
        _setSymAVScanFailNoticeHeader(pMxConf);
        _arrSymConfg.push_back(pMxConf);

        //symnSpeRetryInterval key
        pMxConf = new MxSConfig(KEY_SYMSPERETRYINTERVAL, "30", "", EXTN_SERVICE, SCfg, CFG_SERV_RESTART);
        _setSymSpeRetryInterval(pMxConf);
        _arrSymConfg.push_back(pMxConf);

        //symnReportDefInfoEnabled key
        pMxConf = new MxSConfig(KEY_SYMREPORTDEFINFO, "false", "", EXTN_SERVICE, SCfg, CFG_SERV_RESTART);
        _setSymnReportDefInfoEnabled(pMxConf);
        _arrSymConfg.push_back(pMxConf);

        //symnICAPHttpClientIPEnable key
        pMxConf = new MxSConfig(KEY_SYMICAPHTTPCLIENTIP, "false", "", EXTN_SERVICE, SCfg, CFG_SERV_RESTART);
        _setSymnICAPHttpClientIPEnable(pMxConf);
        _arrSymConfg.push_back(pMxConf);

        //symnAVVersion
        pMxConf = new MxSConfig(KEY_SYMVERSION, "1.0.0.0", "", EXTN_SERVICE, SCfg, CFG_SERV_RESTART);
        _setSymnAVVersion(pMxConf);
        _arrSymConfg.push_back(pMxConf);

    } catch(exception& ex) {
        LOG(Notification, "Unable to populate config array ! ", "%s", ex.what());
    } 
}

//------------------------------------------------------------------------
//  
// Method: SymantecConfig::getValidatedString
//
// Synopsis: Validate string for long and get it converted back to in string format
//
//------------------------------------------------------------------------
string SymantecConfig::getValidatedLongStr(string& str)
{
    long lval = atol(str.c_str());
    char arr[20];
    sprintf(arr, "%lu", lval);
    string ret(arr);
    return ret;
}
